console.log('Loading paybyphone selection');

exports.handler = function (event, context) {

	qryObject = parseQuery(event.reqbody);
	var enteredNum = qryObject['Digits'];
	var ecitnumEncoded = event.ecitnum;
	var choices = [];
	var jwt = require('jsonwebtoken');
	
	jwt.verify(ecitnumEncoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671', function (err, decoded) {
		if (err) {
			//Need to send user to first screen
		} else {
			var authTokenWithStatus = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum, citationStatus: decoded.citationStatus },
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });

			var validOptions = ["*", "8", "1", "2"];
			var lang = decoded.selectedlang;
			
	if (lang == "en-US") {
		if (validOptions.indexOf(enteredNum) == -1) {
				var inputInvalid = "";
				if(decoded.invalidCount != undefined) {
					inputInvalid = decoded.invalidCount;
				}
				inputInvalid = inputInvalid + "1"; 
				
				var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum, citationStatus: decoded.citationStatus, invalidCount: inputInvalid },
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
				if(inputInvalid == "111") {
					choices = [{
						"digits":0,
						"lang": decoded.selectedlang,
						"action":"",
						"message": "",
						"options":[],
						"redirectAction":"YES",
						"timeOut":"0"
					}];
				}else {
					choices = [{
						"digits": 1,
						"lang": decoded.selectedlang,
						"action": "/dev/paybyphone?ecitnum=" + authTokenTryAgain,
						"message": "We are sorry, your entry was invalid. Please try again",
						"repeatMenu": "To repeat these options, press *",
						"mainMenu": "To return to the previous menu, press 8"
					}];
				}
				context.succeed(choices[0]);
			}

				switch (enteredNum) {
					case "*":
						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/paybyphone?ecitnum=" + authTokenWithStatus,
							"message": "",
							"options": [
								"To pay only the amount due on this ticket, </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.totalDue,
								"To pay the entire amount owed on this license plate, </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.allPenalties,
							],
							"repeatMenu": "To repeat these options, press *",
							"mainMenu": "To return to the previous menu, press 8"
						}]
						context.succeed(choices[0]);

						break;

					case "8":
						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/payoptions?ecitnum=" + authTokenWithStatus,
							"message": "",
							"options": [
								"To learn how to pay your violation on the internet",
								"To pay now with a Visa or Mastercard",
								"To learn about paying by mail",
								"To learn about paying in person"
							],
							"repeatMenu": "To repeat these options, press *",
							"mainMenu": "To return to the previous menu, press 8"
						}];
						context.succeed(choices[0]);
						break;

					case "1":

						var fullCitationNum = "";
						if (decoded.selectedOption == "1") {
							fullCitationNum = "T" + decoded.userCitNum;
						}
						if (decoded.selectedOption == "2") {
							fullCitationNum = "P" + decoded.userCitNum;
						}
						var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, ecitnum: fullCitationNum, userCitNum: decoded.userCitNum, citationStatus: decoded.citationStatus, amount: '$' + decoded.citationStatus.totalDue }, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });

						choices = [{
							"digits": 18,
							"lang": decoded.selectedlang,
							"action": "/dev/ccenter?ecitnum=" + authToken,
							"message": "Please enter your credit card number now, followed by the pound sign. ",
							"options": []
						}];
						context.succeed(choices[0]);
						break;

					case "2":
						var fullCitationNum = "";
						if (decoded.selectedOption == "1") {
							fullCitationNum = "T" + decoded.userCitNum;
						}
						if (decoded.selectedOption == "2") {
							fullCitationNum = "P" + decoded.userCitNum;
						}
						var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, ecitnum: fullCitationNum, userCitNum: decoded.userCitNum, citationStatus: decoded.citationStatus, amount: '$' + decoded.citationStatus.allPenalties }, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });

						choices = [{
							"digits": 18,
							"lang": decoded.selectedlang,
							"action": "/dev/ccenter?ecitnum=" + authToken,
							"message": "Please enter your credit card number now, followed by the pound sign. ",
							"options": []
						}];
						context.succeed(choices[0]);
						break;
				}
			}
	
	
	if (lang == "es-MX") {
				
		if (validOptions.indexOf(enteredNum) == -1) {
			
			var inputInvalid = "";
				if(decoded.invalidCount != undefined) {
					inputInvalid = decoded.invalidCount;
				}
				inputInvalid = inputInvalid + "1"; 
				
				var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum, citationStatus: decoded.citationStatus, invalidCount: inputInvalid },
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
				if(inputInvalid == "111") {
					choices = [{
						"digits":0,
						"lang": decoded.selectedlang,
						"action":"",
						"message": "",
						"options":[],
						"redirectAction":"YES",
						"timeOut":"0"
					}];
				}else {
					choices = [{
						"digits": 1,
						"lang": decoded.selectedlang,
						"action": "/dev/paybyphone?ecitnum=" + authTokenTryAgain,
						"message": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
						"repeatMenu": "Para repetir estas opciones, presione estrella",
						"mainMenu": "Para regresar al menú anterior, presione ocho"
					}];
				}
				context.succeed(choices[0]);
			}

				switch (enteredNum) {
					case "*":
						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/paybyphone?ecitnum=" + authTokenWithStatus,
							"message": "",
							"optionSpanish": [
								"Para pagar solamente el monto debido, </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.totalDue +"presione uno",
								"Para pagar la cantidad adeudada en esta placa, </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.allPenalties +"presione dos",
							],
							"repeatMenu": "Para repetir estas opciones, presione estrella",
							"mainMenu": "Para regresar al menú anterior, presione ocho"
						}]
						context.succeed(choices[0]);

						break;

					case "8":
						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/payoptions?ecitnum=" + authTokenWithStatus,
							"message": "",
							"optionSpanish": [
								"Para aprender como pagar su multa en Internet, presione uno",
								"Para realizar su pago con una tarjeta Visa o MasterCard… presione dos",
								"Para información acerca de cómo realizar un pago por correo… presione tres",
								"Para aprender sobre el pago en persona, presione cuatro"
							],
							"repeatMenu": "Para repetir estas opciones, presione estrella",
							"mainMenu": "Para regresar al menú anterior, presione ocho"
						}];
						context.succeed(choices[0]);
						break;

					case "1":

						var fullCitationNum = "";
						if (decoded.selectedOption == "1") {
							fullCitationNum = "T" + decoded.userCitNum;
						}
						if (decoded.selectedOption == "2") {
							fullCitationNum = "P" + decoded.userCitNum;
						}
						
						var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, ecitnum: fullCitationNum, userCitNum: decoded.userCitNum, citationStatus: decoded.citationStatus, amount: '$' + decoded.citationStatus.totalDue }, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });

						choices = [{
							"digits": 18,
							"lang": decoded.selectedlang,
							"action": "/dev/ccenter?ecitnum=" + authToken,
							"message": "Introduzca su número de tarjeta de crédito ahora, seguido por el signo de libra.",
							"options": []
						}];
						context.succeed(choices[0]);
						break;

					case "2":
						var fullCitationNum = "";
						if (decoded.selectedOption == "1") {
							fullCitationNum = "T" + decoded.userCitNum;
						}
						if (decoded.selectedOption == "2") {
							fullCitationNum = "P" + decoded.userCitNum;
						}
						var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, ecitnum: fullCitationNum, userCitNum: decoded.userCitNum, citationStatus: decoded.citationStatus, amount: '$' + decoded.citationStatus.allPenalties }, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });

						choices = [{
							"digits": 18,
							"lang": decoded.selectedlang,
							"action": "/dev/ccenter?ecitnum=" + authToken,
							"message": "Introduzca su número de tarjeta de crédito ahora, seguido por el signo de libra.. ",
							"options": []
						}];
						context.succeed(choices[0]);
						break;
				}
			

			}
		}
	});
};

function parseQuery(qstr) {
	var query = {};
	var a = qstr.substr(0).split('&');
	for (var i = 0; i < a.length; i++) {
		var b = a[i].split('=');
		query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
	}
	return query;
}