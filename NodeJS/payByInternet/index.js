exports.handler = (event, context) => {
   
	qryObject = parseQuery(event.reqbody);
    var enteredNum = qryObject['Digits'];
    var langObj = event.lang;
    var lang = "";
    var inputInvalid = "";
		
    if(langObj.indexOf("invalidCount") !== -1) {
        var langArr = langObj.split('00');
        lang = langArr[0];
        inputInvalidTemp = langArr[1];
        var inputInvalidArr = inputInvalidTemp.split('=');
        inputInvalid = inputInvalidArr[1];
    }else {
        lang = event.lang;
    }
    var validOptions = ["*", "9"];
	
if(lang == "en-US"){
    
    if(validOptions.indexOf(enteredNum) == -1) {
        inputInvalid = inputInvalid + "1"; 
		if(inputInvalid == "111") {
			choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/customerservicescall?lang="+lang,
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
			}];
			context.succeed(choices[0]);
		}else {
			 choices = [{
                "digits":1,
                "lang": lang,
                "action":"/dev/paybyinternet?lang="+lang+"00invalidCount="+inputInvalid,
                "message": "We are sorry, your entry was invalid. Please try again",
                "options":[],
                "repeatMenu":"To repeat this message,  press *",
                "mainMenu": "To return to the main menu,  press 9",
                }];
			context.succeed(choices[0]);
		}
    }
    
    switch(enteredNum) {
			case "*":
			choices = [{
                    "digits":1,
                    "lang": lang,
                    "action":"/dev/paybyinternet?lang="+lang,
                    "message": "Violations can be paid on the internet by accessing our web site at W-W-W dot Metro dot net, slash, Pay Violation. The web site offers easy to use and secure credit card payment transactions 24 hours a day, 7 days a week.",
                    "options":[],
                    "repeatMenu":"To repeat this message,  press *",
                    "mainMenu": "To return to the main menu,  press 9",
                }]
			context.succeed(choices[0]);
			break;
			case "9":
            choices = [{
                    "digits":1,
                    "lang": lang,
                    "action":"/dev/violationmenu?lang="+lang,
                    "message": "",
                    "options":[
                        "To check the amount due on a violation",
                        "To find out how to pay for a violation",
                        "For information on how to contest a violation, or schedule an administrative hearing"
                    ],
                    "repeatMenu":"To repeat this menu, press *"
                }];
            context.succeed(choices[0]);
            break;
    }
}
	
	
if(lang == "es-MX"){
	    
	if(validOptions.indexOf(enteredNum) == -1) {
        inputInvalid = inputInvalid + "1"; 
		if(inputInvalid == "111") {
			choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/customerservicescall?lang="+lang,
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
			}];
		}else {
		    choices = [{
		        "digits":1,
				"lang": lang,
				"action":"/dev/paybyinternet?lang="+lang+"00invalidCount="+inputInvalid,
				"message": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
				"repeatMenu":"Para repetir estas opciones, presione estrella",
                "mainMenu": "Para regresar al menú principal, presione nueve"
			}];
		}
		context.succeed(choices[0]);
	}
    
    
    
    switch(enteredNum) {
			case "*":
			choices = [{
                    "digits":1,
                    "lang": lang,
                    "action":"/dev/paybyinternet?lang="+lang,
                    "message": "Multas pueden ser pagadas en Internet accediendo a nuestro sitio web en W-W-W Metro punto net, barra, Pay Violation. The web site offers easy to use and secure credit card payment transactions 24 hours a day, 7 days a week.",
                    "options":[],
                    "repeatMenu":"Para repetir estas opciones, presione estrella",
                    "mainMenu": "Para regresar al menú principal, presione nueve"
                }]
			context.succeed(choices[0]);
			break;
			case "9":
            choices = [{
                    "digits":1,
                    "lang": lang,
                    "action":"/dev/violationmenu?lang="+lang,
                    "message": "",
                    "optionSpanish":[
                        "Para verificar el saldo de una multa ... presione uno",
                        "Información para pagar su multa ... presione dos",
                        "Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
                    ],
                    "repeatMenu":"Para repetir este menú, presione estrella"
                }];
            context.succeed(choices[0]);
            break;
    }	
}

};
function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}