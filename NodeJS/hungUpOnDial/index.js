console.log('Loading hungUpOnDial function');
    
exports.handler = function(event, context) {
        
        qryObject = parseQuery(event.reqbody);
        var dialCallStatusInBody = qryObject['DialCallStatus'];
        var dialCallStatus = event.DialCallStatus;
        
        console.log(dialCallStatusInBody+' '+dialCallStatus);
        
        var choices = {
           "digits":1,
           "lang": "en-US",
           "action":"hungup",
           "message": dialCallStatus,
           "options":[]
            };
        context.succeed(choices);

};
    
    
function parseQuery(qstr) {
    	var query = {};
    	var a = qstr.substr(0).split('&');
    	for (var i = 0; i < a.length; i++) {
    		var b = a[i].split('=');
    		query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
    	}
    	return query;
    }