exports.handler = function(event, context) {
	poptions = [
		"Pay through credit card",
		"Place a review request",
		"Go back to previous menu",
		"Go back to main menu"
	];
	npoptions = [
		"Go back to previous menu",
		"Go back to main menu"
	];

	qryObject = parseQuery(event.reqbody);
	
	var ecitnum = qryObject['Digits'];
	var request = require('request');
	
	request("http://54.153.51.173:8080/ecitation/getCitationAmount/T"+ecitnum, function(error, res, body) {
	    if( !error ){
    	    var penalty = JSON.parse(body);
    		var response = {};
    		if( penalty && penalty.code === 200){
    			response = {
    				"action":"/dev/penaltymenu?ecitnum="+ecitnum,
    				"message":"Your penalty amount is " + penalty.token,
    				"options":poptions
    			};
    		}else if( penalty && penalty.code === 404){
    			response = {
    				"digits":10,
                    "action":"/dev/citnument",
                    "message": "Invalid citation number. Please enter valid citation number followed by #",
                    "options":[] 
    			};
    		}else{
    			response = {
    				"action":"/dev/nopenalty",
    				"message":"No penalty for given citation number",
    				"options":npoptions
    			};
    		}    
	    }else{
	        response = {
				"action":"/dev/penaltymenu?ecitnum="+ecitnum,
				"message":"Internal error please try again",
				"options":poptions
			};
	    }
		
		context.succeed(response);
	});
};

function parseQuery(qstr) {
	var query = {};
	var a = qstr.substr(0).split('&');
	for (var i = 0; i < a.length; i++) {
		var b = a[i].split('=');
		query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
	}
	return query;
}