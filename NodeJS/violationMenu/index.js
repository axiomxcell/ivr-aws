    console.log('Loading violationmenu function');
    
    exports.handler = function(event, context) {
        
        qryObject = parseQuery(event.reqbody);
        var selOption = qryObject['Digits'];
        
        var langObj = event.lang;
        var lang = "";
        var inputInvalid = "";
		
        if(langObj.indexOf("invalidCount") !== -1) {
            var langArr = langObj.split('00');
            lang = langArr[0];
            inputInvalidTemp = langArr[1];
            var inputInvalidArr = inputInvalidTemp.split('=');
            inputInvalid = inputInvalidArr[1];
        }else {
            lang = event.lang;
        }
        
        
        var choices = [];
        var validOptions = ["*", "1", "2", "3"];
        
        

if(lang == "en-US"){
            
    if(validOptions.indexOf(selOption) == -1) {
        inputInvalid = inputInvalid + "1"; 
		if(inputInvalid == "111") {
			choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/customerservicescall?lang=en-US",
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
			}];
			context.succeed(choices[0]);
		}else {
			 choices = [{
                "digits":1,
                "lang": lang,
                "action":"/dev/violationmenu?lang="+lang+"00invalidCount="+inputInvalid,
                "message": "We are sorry, your entry was invalid. Please try again",
                "options":[
                    "To check the amount due on a violation",
                    "To find out how to pay for a violation",
                    "For information on how to contest a violation, or schedule an administrative hearing"
                    ],
                "repeatMenu":"To repeat this menu, press *"
                }];
			context.succeed(choices[0]);
		}
           
        }
        
        switch(selOption) {
            case "*": 
               choices = [{
            "digits":1,
            "lang": lang,
            "action":"/dev/violationmenu?lang="+lang,
            "message": "",
            "options":[
            "To check the amount due on a violation",
            "To find out how to pay for a violation",
            "For information on how to contest a violation, or schedule an administrative hearing"
            ],
            "repeatMenu":"To repeat this menu, press *"
          }];
          context.succeed(choices[0]);
          break;
    default: 
    
        choices = [{
           "digits":1,
           "lang": lang,
            "action":"/dev/readviolationprefix?lang="+lang,
            "message": "Please have your violation ready.  You will find the violation number located on the upper right hand corner. You may repeat this menu at any time by pressing star.",
            "options":[
            "If your violation starts with a T",
            "If it starts with a P"
            ],
            "repeatMenu":"To repeat this menu, press *",
            "mainMenu": "To return to the main menu,  press 9"
        },
        {
           "digits":1,
           "lang": lang,
            "action":"/dev/readviolationprefix?lang="+lang,
            "message": "Please have your violation ready.  You will find the violation number located on the upper right hand corner. You may repeat this menu at any time by pressing star.",
            "options":[
            "If your violation starts with a T",
            "If it starts with a P"
            ],
            "repeatMenu":"To repeat this menu, press *",
            "mainMenu": "To return to the main menu,  press 9"
        },
        {
           "digits":1,
           "lang": lang,
            "action":"/dev/contestticket?lang="+lang,
            "message": "",
            "options":[
            "If you were recently issued a violation, or received a notice of delinquency or wish to contest the violation…",
            "If you already had an initial review and you wish to schedule an administrative hearing…",
            "If you mailed in a request for an initial review or an administrative hearing and have not received a response…"
            ],
            "repeatMenu":"To repeat these options, press *",
            "mainMenu": "To return to the main menu,  press 9"
        }
       ];
       context.succeed(choices[qryObject['Digits']-1]);
       break;
    }

            
}
        
if(lang == "es-MX"){
        
    if(validOptions.indexOf(selOption) == -1) {
        inputInvalid = inputInvalid + "1"; 
		if(inputInvalid == "111") {
			choices = [{
					"digits":0,
					"lang": lang,
					"action":"/dev/customerservicescall?lang=en-US",
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
			}];
			context.succeed(choices[0]);
		}else {
			 choices = [{
                "digits":1,
                "lang": lang,
                "action":"/dev/violationmenu?lang="+lang+"00invalidCount="+inputInvalid,
                "message": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
                "optionSpanish":[
                    "Para verificar el saldo de una multa ... presione uno",
                    "Información para pagar su multa ... presione dos",
                    "Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
                    ],
                "repeatMenu":"Para repetir este menú, presione estrella"
                }];
			context.succeed(choices[0]);
		}
           
        }
        
        
        switch(selOption) {
            case "*": 
               choices = [{
            "digits":1,
            "lang": lang,
            "action":"/dev/violationmenu?lang="+lang,
            "message": "",
            "optionSpanish":[
            "Para verificar el saldo de una multa ... presione uno",
            "Información para pagar su multa ... presione dos",
            "Para información cómo puede disputar una multa o cómo citar una audiencia administrativa, presione tres"
            ],
            "repeatMenu":"Para repetir este menú, presione estrella"
          }];
          context.succeed(choices[0]);
          break;
    default: 
    
        choices = [{
           "digits":1,
           "lang": lang,
            "action":"/dev/readviolationprefix?lang="+lang,
            "message": "Por favor tenga su violación lista. Encontrará el número de violación en la esquina superior derecha. Puede repetir este menú en cualquier momento pulsando la tecla de la estrella.",
            "optionSpanish":[
            "Si su multa comienza con una T, presione uno",
            "Si comienza con una P, presione dos"
            ],
            "repeatMenu":"Para repetir este menú, presione estrella",
            "mainMenu": "Para regresar al menú principal, presione nueve"
        },
        {
           "digits":1,
           "lang": lang,
            "action":"/dev/readviolationprefix?lang="+lang,
            "message": "Por favor tenga su violación lista. Encontrará el número de violación en la esquina superior derecha. Puede repetir este menú en cualquier momento pulsando la tecla de la estrella.",
            "optionSpanish":[
            "Si su multa comienza con una T, presione uno",
            "Si comienza con una P, presione dos"
            ],
            "repeatMenu":"Para repetir este menú, presione estrella",
            "mainMenu": "Para regresar al menú principal, presione nueve"
        },
        {
           "digits":1,
           "lang": lang,
            "action":"/dev/contestticket?lang="+lang,
            "message": "",
            "optionSpanish":[
            "Si recientemente recibió una multa, o recibió una notificación de delincuencia o desea disputar su multa, presione uno",
            "Si ya ha pasado una revisión inicial y desea citar una audiencia administrativa, presione dos",
            "Si ha enviado por correo una solicitud de revisión inicial o de audiencia administrativa y no ha recibido respuesta, presione tres"
            ],
            "repeatMenu":"Para repetir este menú, presione estrella",
            "mainMenu": "Para regresar al menú principal, presione nueve"
        }
       ];
       context.succeed(choices[qryObject['Digits']-1]);
       break;
    }
}

};
    
    
    function parseQuery(qstr) {
    	var query = {};
    	var a = qstr.substr(0).split('&');
    	for (var i = 0; i < a.length; i++) {
    		var b = a[i].split('=');
    		query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
    	}
    	return query;
    }