console.log('Loading contest ticket function');

exports.handler = function (event, context) {

	qryObject = parseQuery(event.reqbody);
	var selOption = qryObject['Digits'];
	var langObj = event.lang;
    var lang = "";
    var inputInvalid = "";
		
    if(langObj.indexOf("invalidCount") !== -1) {
        var langArr = langObj.split('00');
        lang = langArr[0];
        inputInvalidTemp = langArr[1];
        var inputInvalidArr = inputInvalidTemp.split('=');
        inputInvalid = inputInvalidArr[1];
    }else {
        lang = event.lang;
    }
	
	var choices = [];
	var validOptions = ["*", "9", "1", "2", "3"];
	
	
	if (lang == "en-US") {
		if (validOptions.indexOf(selOption) == -1) {
			inputInvalid = inputInvalid + "1";
			if(inputInvalid == "111") {
				choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/customerservicescall?lang="+lang,
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
				}];
				context.succeed(choices[0]);
			}else {
				choices = [{
				"digits": 1,
				"lang": lang,
				"action": "/dev/contestticket?lang="+ lang +"00invalidCount="+ inputInvalid,
				"message": "We are sorry, your entry was invalid. Please try again",
				"options": [
						"If you were recently issued a violation, or received a notice of delinquency or wish to contest the violation…",
						"If you already had an initial review and you wish to schedule an administrative hearing…",
						"If you mailed in a request for an initial review or an administrative hearing and have not received a response…"
				],
				"repeatMenu": "To repeat these options, press *",
				"mainMenu": "To return to the main menu,  press 9"
				}];
			context.succeed(choices[0]);
			}
		}

		switch (selOption) {
			case "*":
				choices = [{
					"digits": 1,
					"lang": lang,
					"action": "/dev/contestticket?lang=" + lang,
					"message": "",
					"options": [
						"If you were recently issued a violation, or received a notice of delinquency or wish to contest the violation…",
						"If you already had an initial review and you wish to schedule an administrative hearing…",
						"If you mailed in a request for an initial review or an administrative hearing and have not received a response…"
					],
					"repeatMenu": "To repeat these options, press *",
					"mainMenu": "To return to the main menu,  press 9"
				}];
				context.succeed(choices[0]);
				break;

			case "9":
				choices = [{
					"digits": 1,
					"lang": lang,
					"action": "/dev/violationmenu?lang=" + lang,
					"message": "",
					"options": [
						"To check the amount due on a violation",
						"To find out how to pay for a violation",
						"For information on how to contest a violation, or schedule an administrative hearing"
					],
					"repeatMenu": "To repeat this menu, press *"
				}];
				context.succeed(choices[0]);
				break;

			default:
				choices = [{
					"digits": 1,
					"lang": lang,
					"action": "/dev/contestticketselection?lang=" + lang,
					"message": "",
					"options": [
						"If you are calling about a transit violation…",
						"If you are calling about a parking violation…"
					]
				},
				{
					"digits": 1,
					"lang": lang,
					"action": "/dev/heraingcitation?lang=" + lang,
					"message": "",
					"options": [
						"If you are calling about a transit violation…",
						"If you are calling about a parking violation…"
					]
				},
				{
					"digits": 1,
					"lang": lang,
					"action": "/dev/contestticwaitingres?lang=" + lang,
					"message": "",
					"options": [
						"If you have not received confirmation of your request for an initial review or administrative hearing and a request was made more than sixty days ago…"
					],
					"mainMenu": "To return to the main menu,  press 9"
				}
				];
				context.succeed(choices[qryObject['Digits'] - 1]);
				break;
		}
	}
	
	
	if (lang == "es-MX") {
		if (validOptions.indexOf(selOption) == -1) {
			inputInvalid = inputInvalid + "1";
			if(inputInvalid == "111") {
				choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/customerservicescall?lang="+lang,
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
				}];
			    context.succeed(choices[0]);
			}else {
				choices = [{
				"digits": 1,
				"lang": lang,
				"action": "/dev/contestticket?lang="+ lang +"00invalidCount="+ inputInvalid,
				"message": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
				"optionSpanish": [
						"Si recientemente recibió una multa, o recibió una notificación de delincuencia o desea disputar su multa ... presione uno",
						"Si ya ha pasado una revisión inicial y desea citar una audiencia administrativa… presione dos",
						"Si ha enviado por correo una solicitud de revisión inicial o de audiencia administrativa y no ha recibido respuesta…presione tres"
				],
				"repeatMenu": "Para repetir este menú, presione estrella",
				"mainMenu": "Para regresar al menú principal, presione nueve"
				}];
			context.succeed(choices[0]);
			}
			
		}



		switch (selOption) {
			case "*":
				choices = [{
					"digits": 1,
					"lang": lang,
					"action": "/dev/contestticket?lang=" + lang,
					"message": "",
					"optionSpanish": [
						"Si recientemente recibió una multa, o recibió una notificación de delincuencia o desea disputar su multa ... presione uno",
						"Si ya ha pasado una revisión inicial y desea citar una audiencia administrativa… presione dos",
						"Si ha enviado por correo una solicitud de revisión inicial o de audiencia administrativa y no ha recibido respuesta…presione tres"
					],
					"repeatMenu": "Para repetir este menú, presione estrella",
					"mainMenu": "Para regresar al menú principal, presione nueve"
				}];
				context.succeed(choices[0]);
				break;

			case "9":
				choices = [{
					"digits": 1,
					"lang": lang,
					"action": "/dev/violationmenu?lang=" + lang,
					"message": "",
					"optionSpanish": [
						"Para verificar el saldo de una multa ... presione uno",
						"Información para pagar su multa ... presione dos",
						"Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
					],
					"repeatMenu": "Para repetir este menú, presione estrella"
				}];
				context.succeed(choices[0]);
				break;

			default:
				choices = [{
					"digits": 1,
					"lang": lang,
					"action": "/dev/contestticketselection?lang=" + lang,
					"message": "",
					"optionSpanish": [
						"Si llama acerca de una multa de transito…presione uno",
						"Si llama acerca de una multa de estacionamiento…presione dos"
					]
				},
				{
					"digits": 1,
					"lang": lang,
					"action": "/dev/heraingcitation?lang=" + lang,
					"message": "",
					"optionSpanish": [
						"Si llama acerca de una multa de transito…presione uno",
						"Si llama acerca de una multa de estacionamiento…presione dos"
					]
				},
				{
					"digits": 1,
					"lang": lang,
					"action": "/dev/contestticwaitingres?lang=" + lang,
					"message": "",
					"optionSpanish": [
						"Si no ha recibido confirmación de su solicitud de revisión inicial o de audiencia administrativa y presentó su solicitud hace más de 60 días… presione uno"
					],
					"mainMenu": "Para regresar al menú principal, presione nueve"
				}
				];
				context.succeed(choices[qryObject['Digits'] - 1]);
				break;
		}

	}
};


function parseQuery(qstr) {
	var query = {};
	var a = qstr.substr(0).split('&');
	for (var i = 0; i < a.length; i++) {
		var b = a[i].split('=');
		query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
	}
	return query;
}