exports.handler = function(event, context) {
	console.log('Loading holidays/business hours check');
	qryObject = parseQuery(event.reqbody);
	var hearingselection = qryObject['Digits'];
	var jwt = require('jsonwebtoken');
	 
	var langObj = event.lang;
    var lang = "";
    var inputInvalid = "";
		
    if(langObj.indexOf("invalidCount") !== -1) {
        var langArr = langObj.split('00');
        lang = langArr[0];
        inputInvalidTemp = langArr[1];
        var inputInvalidArr = inputInvalidTemp.split('=');
        inputInvalid = inputInvalidArr[1];
    }else {
        lang = event.lang;
    }

	var authToken = jwt.sign({ selectedlang: lang, selectedOption: hearingselection }, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });

	var validOptions = ["1", "2"];
	
if(lang == "en-US") {
	if(validOptions.indexOf(hearingselection) == -1) {
		inputInvalid = inputInvalid + "1"; 
		if(inputInvalid == "111") {
			choices = [{
					"digits":1,
					"lang": lang,
					"action":"",
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
			}];
		}else {
			choices = [{
					"digits":1,
					"lang": lang,
					"action": "/dev/heraingcitation?lang=" + lang+"00invalidCount="+inputInvalid,
					"message1": "We are sorry, your entry was invalid. Please try again",
					"options": [
						"If you are calling about a transit violation…",
						"If you are calling about a parking violation…"
					]
			}];
		}
		context.succeed(choices[0]);
	}
	
	choices = [
        {
            "digits":1,
            "lang":lang,
            "action":"/dev/hearingrepeat?ecitnum="+authToken,
            "message1": "If you disagree with the results of the initial review, you may request an administrative hearing by mail, in person, or by phone.  You must make the request within twenty-one calendar days from the date of the initial review letter that was mailed to you.  A hearing officer will conduct the administrative hearing.  If you do not request an administrative hearing within the twenty-one day period, you lose your right to further contest the ticket.",
            "message2": "All fines and penalties must be paid in full before an administrative hearing is scheduled.  If the hearing officer dismisses your violation, the money paid will be refunded.  If you wish to request an administrative hearing, please mail in your hearing request form with full payment of all fines and penalties to ",
            "message3": "Metro Transit Court, PO Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6",
            "submessage1": "",
            "submessage2": "Metro may allow a waiver of the advance payment for persons unable to pay. You should have received instructions for obtaining a waiver with the letter of decision for your initial review.",
            "repeatMenu": "To repeat this message, press *",
            "mainMenu": "To return to the main menu, press 9",
            "options":[
                "If you have already paid the violation, and want to schedule an administrative hearing",
                "To find out the outstanding fine amount on the violation being contested",
                "To repeat the address"
                ]
        },
        {
           "digits":1,
            "lang":lang,
            "action":"/dev/hearingrepeat?ecitnum="+authToken,
            "message1": "If you disagree with the results of the initial review, you may request an administrative hearing by mail, in person, or by phone.  You must make the request within twenty-one calendar days from the date of the initial review letter that was mailed to you.  A hearing officer will conduct the administrative hearing.  If you do not request an administrative hearing within the twenty-one day period, you lose your right to further contest the ticket.",
            "message2": "All fines and penalties must be paid in full before an administrative hearing is scheduled.  If the hearing officer dismisses your violation, the money paid will be refunded.  If you wish to request an administrative hearing, please mail in your hearing request form with full payment of all fines and penalties to ",
            "message3": "Metro Transit Court, PO Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. ",
            "submessage1": "",
            "submessage2": "Metro may allow a waiver of the advance payment for persons unable to pay. You should have received instructions for obtaining a waiver with the letter of decision for your initial review.",
            "repeatMenu": "To repeat this message, press *",
            "mainMenu": "To return to the main menu, press 9",
            "options":[
                "If you have already paid the violation, and want to schedule an administrative hearing",
                "To find out the outstanding fine amount on the violation being contested",
                "To repeat the address"
                ]
        }
      ]
        
   context.succeed(choices[qryObject['Digits']-1]);

}

if(lang == "es-MX") {
	
	if(validOptions.indexOf(hearingselection) == -1) {
		inputInvalid = inputInvalid + "1"; 
		if(inputInvalid == "111") {
			choices = [{
					"digits":1,
					"lang": lang,
					"action":"",
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
			}];
		}else {
			choices = [{
					"digits":1,
					"lang": lang,
					"action": "/dev/heraingcitation?lang=" + lang+"00invalidCount="+inputInvalid,
					"message1": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
					"optionSpanish": [
						"Si llama acerca de una multa de transito…presione uno",
						"Si llama acerca de una multa de estacionamiento…presione dos"
					]
			}];
		}
		context.succeed(choices[0]);
	}
	
	choices = [{
            "digits":1,
            "lang":lang,
            "action":"/dev/hearingrepeat?ecitnum="+authToken,
            "message1": "Si no está de acuerdo con los resultados de la revisión inicial, puede solicitar una audiencia administrativa por correo, en persona o por teléfono. Debe hacer esa solicitud dentro de los veintiún días calendario a partir de la fecha de la carta de revisión inicial que se le envió por correo. Un oficial de audiencia llevará a cabo la audiencia administrativa. Si no solicita una audiencia administrativa dentro del período de veintiún días,  pierde su derecho a impugnar el boleto. ",
            "message2": "Todas las multas deben ser pagadas en su totalidad antes de que se cite una audiencia administrativa. Si el oficial de la audiencia despide su multa, el dinero pagado será devuelto. Si desea solicitar una audiencia administrativa, envíe por correo su formulario de solicitud de audiencia con el pago completo de todas las multas y sanciones a ",
            "message3": "Metro Transit Court, PO Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6.",
            "submessage1": "",
            "submessage2": "Metro puede permitir una exención del pago anticipado de las personas que no pueden pagar. Usted debe haber recibido instrucciones para obtener una exención con la carta de decisión para su revisión inicial.",
            "repeatMenu": "Para repetir este mensaje, presione estrella",
            "mainMenu": "Para regresar al menú principal, presione nueve",
            "optionSpanish":[
                "Si ya ha pagado la multa y desea citar una audiencia administrativa… presione uno",
                "Para averiguar el monto de la multa pendiente sobre la violación que está siendo disputada, presione dos",
                "Para repetir la dirección, pulse tres"
                ]
        },
        {
           "digits":1,
            "lang":lang,
            "action":"/dev/hearingrepeat?ecitnum="+authToken,
            "message1": "Si no está de acuerdo con los resultados de la revisión inicial, puede solicitar una audiencia administrativa por correo, en persona o por teléfono. Debe hacer esa solicitud dentro de los veintiún días calendario a partir de la fecha de la carta de revisión inicial que se le envió por correo. Un oficial de audiencia llevará a cabo la audiencia administrativa. Si no solicita una audiencia administrativa dentro del período de veintiún días,  pierde su derecho a impugnar el boleto. ",
            "message2": "Todas las multas deben ser pagadas en su totalidad antes de que se cite una audiencia administrativa. Si el oficial de la audiencia despide su multa, el dinero pagado será devuelto. Si desea solicitar una audiencia administrativa, envíe por correo su formulario de solicitud de audiencia con el pago completo de todas las multas y sanciones a ",
            "message3": "Metro Transit Court, PO Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. ",
            "submessage1": "",
            "submessage2": "Metro puede permitir una exención del pago anticipado de las personas que no pueden pagar. Usted debe haber recibido instrucciones para obtener una exención con la carta de decisión para su revisión inicial.",
            "repeatMenu": "Para repetir este mensaje, presione estrella",
            "mainMenu": "Para regresar al menú principal, presione nueve",
            "optionSpanish":[
                "Si ya ha pagado la multa y desea citar una audiencia administrativa… presione uno",
                "Para averiguar el monto de la multa pendiente sobre la violación que está siendo disputada, presione dos",
                "Para repetir la dirección, pulse tres"
                ]
        }];
    
   context.succeed(choices[qryObject['Digits']-1]);	
}


};

function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}