exports.handler = function (event, context) {
	var EXPENTER = 0;
	var REPEAT = 1;

	qryObject = parseQuery(event.reqbody);
	var ccnum = qryObject['Digits'];
	var token = event.ecitnum;
	var jwt = require('jsonwebtoken');

	jwt.verify(token, 'e3b65b3a-2b52-11e7-93ae-92361f002671', function (err, decoded) {
		if (err) {
			//Need to send user to first screen
		} else {
			var lang = decoded.selectedlang;
			if (lang == "en-US") {
				var resObj = {};
				if (!decoded.expa || decoded.expa === EXPENTER) {
					decoded.expiry = ccnum;
					//user entered the expiry date...  do some validation
					var validationError = false;
					if (!decoded.expiry) {
						validationError = true;
					} else if (decoded.expiry.length != 4) {
						validationError = true;
					} else {
						var month = parseInt(decoded.expiry.substring(0, 2));
						if (month < 1 || month > 12) {
							validationError = true;
						} else {
							var year = parseInt(decoded.expiry.substring(2));
							var today, someday;
							today = new Date();
							someday = new Date();
							someday.setFullYear(2000 + year, month, 1);

							if (someday < today) {
								validationError = true;
							}
						}
					}

					if (validationError) {
						delete decoded.expa;
						delete decoded.expiry;
						if (!decoded.expt) {
							decoded.expt = 0;
						}
						decoded.expt = decoded.expt + 1;
						if (decoded.expt > 2) {
							//Reached max tries
							resObj = {
								"digits": 1,
								"timeout": 0,
								"action": "/dev/customerservicescall?lang=" + lang,
								"message": "",
								"options": [],
								"lang": lang,
								"redirectAction": '<Redirect method="GET">/dev/customerservicescallget?lang=' + lang + '&amp;Digits=1</Redirect>'
							};
						} else {
							//Invalid expiry date
							var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
							if (decoded.expt == 1) {
								resObj = {
									"digits": 5,
									"timeout": 20,
									"action": "/dev/expenter?ecitnum=" + authToken,
									"message": "The expiration date is a 4-digit number.",
									"options": [
										"For example, if the expiration date is May 2019, you will enter 0-5, 1-9.",
										"Please enter the expiration date, followed by the pound sign."
									],
									"lang": lang
								}
							} else {
								resObj = {
									"digits": 5,
									"timeout": 20,
									"action": "/dev/expenter?ecitnum=" + authToken,
									"message": "That number was not recognized.",
									"options": [
										"You must enter a 4-digit number only.",
										"Please enter the expiration date, followed by the pound sign."
									],
									"lang": lang
								}
							}
						}
					} else {
						decoded.expa = REPEAT;
						var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
						resObj = {
							"digits": 1,
							"timeout": 20,
							"action": "/dev/expenter?ecitnum=" + authToken,
							"message": "You entered... " + expProunce(ccnum),
							"options": [
								"If this is correct, press 1",
								"To reenter the number, press 2"
							],
							"lang": lang
						}
					}
				} else if (decoded.expa === REPEAT) {
					ccnum = parseInt(ccnum);
					if (ccnum === 1) {
						delete decoded.expa;
						//user says it is correct... continue...
						var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
						resObj = {
							"digits": 4,
							"timeout": 20,
							"action": "/dev/cvventer?ecitnum=" + authToken,
							"message": "Please locate the security code on the back of your credit card.",
							"options": [
								"The security code is a 3-digit number often located in the signature box.",
								"Please enter the security code, followed by the pound sign."
							],
							"lang": lang
						}
						//By passing zipcode
						/*
						resObj = {
							"digits":5,
							"action":"/dev/zipenter?ecitnum="+authToken,
							"message":"Please enter the 5 digit zip code for the credit card billing address.",
							"options":[],
							"lang": lang
						}*/
					} else if (ccnum === 2) {
						delete decoded.expa;
						delete decoded.expiry;
						//Chooses to reenter the expiry date
						var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
						resObj = {
							"digits": 5,
							"timeout": 20,
							"action": "/dev/expenter?ecitnum=" + authToken,
							"message": "Please enter the four-digit expiration date on your credit card using two digits for the month and two digits for the year, followed by the pound sign.",
							"options": [
								"For example, if the expiration date is May 2019, you will enter 0-5, 1-9.",
								"If you have a six digit date, enter only the month and year."
							],
							"lang": lang
						}
					} else {
						//Invalid option selected.. so repeat the step one
						decoded.expa = REPEAT;
						var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
						resObj = {
							"digits": 1,
							"timeout": 20,
							"action": "/dev/expenter?ecitnum=" + authToken,
							"message": "I didn't get your entry.",
							"options": [
								"Please try again"
							],
							"lang": lang
						}
					}
				}
				context.succeed([resObj]);
			}
			if (lang == "es-MX") {
					var resObj = {};
				if (!decoded.expa || decoded.expa === EXPENTER) {
					decoded.expiry = ccnum;
					//user entered the expiry date...  do some validation
					var validationError = false;
					if (!decoded.expiry) {
						validationError = true;
					} else if (decoded.expiry.length != 4) {
						validationError = true;
					} else {
						var month = parseInt(decoded.expiry.substring(0, 2));
						if (month < 1 || month > 12) {
							validationError = true;
						} else {
							var year = parseInt(decoded.expiry.substring(2));
							var today, someday;
							today = new Date();
							someday = new Date();
							someday.setFullYear(2000 + year, month, 1);

							if (someday < today) {
								validationError = true;
							}
						}
					}

					if (validationError) {
						delete decoded.expa;
						delete decoded.expiry;
						if (!decoded.expt) {
							decoded.expt = 0;
						}
						decoded.expt = decoded.expt + 1;
						if (decoded.expt > 2) {
							//Reached max tries
							resObj = {
								"digits": 1,
								"timeout": 0,
								"action": "/dev/customerservicescall?lang=" + lang,
								"message": "",
								"options": [],
								"lang": lang,
								"redirectAction": '<Redirect method="GET">/dev/customerservicescallget?lang=' + lang + '&amp;Digits=1</Redirect>'
							};
						} else {
							//Invalid expiry date
							var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
							if (decoded.expt == 1) {
								resObj = {
									"digits": 5,
									"timeout": 20,
									"action": "/dev/expenter?ecitnum=" + authToken,
									"message": "La fecha de caducidad es un número de 4 dígitos.",
									"options": [
										"Por ejemplo, si la fecha de caducidad es mayo de 2019, ingresará 0-5, 1-9.",
										"Por favor introduzca la fecha de caducidad, seguida del signo de libra."
									],
									"lang": lang
								}
							} else {
								resObj = {
									"digits": 5,
									"timeout": 20,
									"action": "/dev/expenter?ecitnum=" + authToken,
									"message": "Ese número no fue reconocido.",
									"options": [
										"Sólo debe introducir un número de 4 dígitos.",
										"Introduzca la fecha de caducidad, seguida del signo de libra."
									],
									"lang": lang
								}
							}
						}
					} else {
						decoded.expa = REPEAT;
						var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
						resObj = {
							"digits": 1,
							"timeout": 20,
							"action": "/dev/expenter?ecitnum=" + authToken,
							"message": "oprimio... " + expProunce(ccnum),
							"options": [
								"Si esto es correcto, presione uno",
								"Para volver a ingresar el número, presione dos"
							],
							"lang": lang
						}
					}
				} else if (decoded.expa === REPEAT) {
					ccnum = parseInt(ccnum);
					if (ccnum === 1) {
						delete decoded.expa;
						//user says it is correct... continue...
						var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
						resObj = {
							"digits": 4,
							"timeout": 20,
							"action": "/dev/cvventer?ecitnum=" + authToken,
							"message": "Por favor localice el código de seguridad en la parte posterior de su tarjeta de crédito.",
							"options": [
								"El código de seguridad es un número de 3 dígitos que a menudo esta situado en el cuadro de firma.",
								"Introduzca el código de seguridad, seguido del signo de libra."
							],
							"lang": lang
						}
						//By passing zipcode
						/*
						resObj = {
							"digits":5,
							"action":"/dev/zipenter?ecitnum="+authToken,
							"message":"Please enter the 5 digit zip code for the credit card billing address.",
							"options":[],
							"lang": lang
						}*/
					} else if (ccnum === 2) {
						delete decoded.expa;
						delete decoded.expiry;
						//Chooses to reenter the expiry date
						var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
						resObj = {
							"digits": 5,
							"timeout": 20,
							"action": "/dev/expenter?ecitnum=" + authToken,
							"message": "Ingrese la fecha de vencimiento de cuatro dígitos en su tarjeta de crédito usando dos dígitos para el mes y dos dígitos para el año, seguido por el signo de libra.",
							"options": [
								"Por ejemplo, si la fecha de caducidad es mayo de 2019, ingresará 0-5, 1-9.",
								"Si tiene una fecha de seis dígitos, introduzca sólo el mes y el año."
							],
							"lang": lang
						}
					} else {
						//Invalid option selected.. so repeat the step one
						decoded.expa = REPEAT;
						var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
						resObj = {
							"digits": 1,
							"timeout": 20,
							"action": "/dev/expenter?ecitnum=" + authToken,
							"message": "No entendí lo que entraste.",
							"options": [
								"Por favor, inténtelo de nuevo"
							],
							"lang": lang
						}
					}
				}
				context.succeed([resObj]);
			

			}
		}
	});
};
function parseQuery(qstr) {
	var query = {};
	if (qstr) {
		var a = qstr.substr(0).split('&');
		for (var i = 0; i < a.length; i++) {
			var b = a[i].split('=');
			query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
		}
	}
	return query;
}

function expProunce(cc) {
	var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
	var ccn = "";
	var month = 0;
	if (cc.length >= 2) {
		month = parseInt(cc.substring(0, 2));
	}
	var year = 0;
	if (cc.length > 2) {
		year = 2000 + parseInt(cc.substring(2));
	}

	return months[month - 1] + " " + year;
}