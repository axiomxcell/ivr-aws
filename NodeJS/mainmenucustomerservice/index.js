console.log('Loading main menu customer service function');

exports.handler = function(event, context) {
    qryObject = parseQuery(event.reqbody);
    var selOption = qryObject['Digits'];
    var lang = event.lang;
    var choices = {};
    
    var date = new Date();
	console.log('In select Language:: '+date);
	var checkHoliday = require('custom-metro-holidaylist')(date);
	console.log("checkHoliday:: "+checkHoliday);	
	
	var businessHours = require('business-hours')(7, 16);
	var isInBusinessHours = businessHours.isBusinessHours();
	console.log("isInBusinessHours:: "+isInBusinessHours);

if(!checkHoliday && isInBusinessHours) { 

if(lang == "en-US") {

    if(selOption == "0") {
        choices = 
        {
           "digits":1,
           "lang": lang,
            "action":"dialcustomer",
            "message": "Please hold, a Transit Court representative will be with you shortly. This call may be monitored for quality assurance purposes.",
            "options":[] 
        }

    }else {
        
		if(selOption == undefined) {
			choices = {
            	"digits":1,
            	"lang": lang,
            	"action":"/dev/mainmenucustomerservice?lang="+lang,
            	"message": "To speak to a Metro Transit Court representative,  press 0",
            	"options":[] 
            }
        }else {
        
            choices = {
            "digits":1,
            "lang": lang,
            "action":"/dev/mainMenuCustomerService?lang="+lang,
            "message": "We’re sorry, your entry was invalid. Please try again",
            "options":[] 
            }
        }
    }
	
 }

  
 if(lang == "es-MX") {
	 
	if(selOption == "0") {
        choices = 
        {
           "digits":1,
           "lang": lang,
            "action":"dialcustomer",
            "message": "Por favor, mantenga, un representante de la corte de transito de metro estará con usted en breve. Esta llamada puede ser monitoreada para fines de garantía de calidad.",
            "options":[] 
        }

    }else {
        
		if(selOption == undefined) {
			choices = {
            	"digits":1,
            	"lang": lang,
            	"action":"/dev/mainmenucustomerservice?lang="+lang,
            	"message": "Para hablar con un representante de la corte de transito de metro, presione cero",
            	"options":[] 
            }
        }else {
        
            choices = {
            "digits":1,
            "lang": lang,
            "action":"/dev/mainMenuCustomerService?lang="+lang,
            "message": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
            "options":[] 
            }
        }
    } 
 }

	
}else {
	 choices = {
            "digits":1,
            "lang": lang,
            "action":"hangup",
            "message": "",
            "options":[] 
      }
}
    
context.succeed(choices);
};


function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}