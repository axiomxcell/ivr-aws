exports.handler = function(event, context) {
    
    qryObject = parseQuery(event.reqbody);
    var enteredNum = qryObject['Digits'];
    
	var ecitnumEncoded = event.ecitnum;
    var choices = [];
	var jwt = require('jsonwebtoken');
	
	
	
	jwt.verify(ecitnumEncoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671', function(err, decoded) {    
		if (err) {
			//Need to send user to first screen
		}else{
			
		var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption }, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });	
			
		var validOptions = ["*", "9", "1", "2", "3"];
		var lang = decoded.selectedlang;
		
		if (lang == "en-US") {
		
			if(validOptions.indexOf(enteredNum) == -1) {
				var inputInvalid = "";
				if(decoded.invalidCount != undefined) {
					inputInvalid = decoded.invalidCount;
				}
				inputInvalid = inputInvalid + "1"; 
				
				var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption,invalidCount: inputInvalid },
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
				if(inputInvalid == "111") {
					choices = [{
						"digits":0,
						"lang": decoded.selectedlang,
						"action":"",
						"message": "",
						"options":[],
						"redirectAction":"YES",
						"timeOut":"0"
					}];
				}else {
					choices = [{
						"digits":1,
						"lang": decoded.selectedlang,
						"action":"/dev/hearingrepeat?ecitnum="+authTokenTryAgain,
						"message1": "We are sorry, your entry was invalid. Please try again",
						"repeatMenu": "To repeat this message, press *",
            			"mainMenu": "To return to the main menu, press 9",
            			"options":[
                			"If you have already paid the violation, and want to schedule an administrative hearing",
                			"To find out the outstanding fine amount on the violation being contested",
                			"To repeat the address"
                		]
					}];
				}
			context.succeed(choices[0]);
		}
	
		
	switch(enteredNum) {
	case "1":
			
	var date = new Date();
	console.log('In select Language:: '+date);
	var checkHoliday = require('custom-metro-holidaylist')(date);
	console.log("checkHoliday:: "+checkHoliday);	
	
    var businessHours = require('business-hours')(7, 16);
	var isInBusinessHours = businessHours.isBusinessHours();
	console.log("isInBusinessHours:: "+isInBusinessHours);

    if(checkHoliday || !isInBusinessHours) {
	console.log('holiday or not business hours');
    choices = [
        {
           "digits":1,
           "lang": decoded.selectedlang,
            "action":"",
            "message1": "Your call must be handled by a Metro Transit Court representative. Our hours are 8:00 am-4pm Monday through Friday, except holidays. Please call back during our regular business hours.",
            "options":[],
			"timeOut":"0"
        }
      ]
    }else {
        choices = [
        {
           "digits":1,
           "lang": decoded.selectedlang,
            "action":"dialcustomer",
            "message1": "Please hold, a Transit Court representative will be with you shortly. This call may be monitored for quality assurance purposes.",
            "options":[] 
        }
      ]
    }
	
	context.succeed(choices[0]);
				
	break;
	
	
	case "2":
			choices = [{
					"digits":1,
					"lang": decoded.selectedlang,
					"action": "/dev/readviolationprefix?lang="+decoded.selectedlang,
					"message1": "Please have your violation ready.  You will find the violation number located on the upper right hand corner. You may repeat this menu at any time by pressing star.",
					"options":[
					"If your violation starts with a T",
					"If it starts with a P"
					],
					"repeatMenu":"To repeat this menu, press *",
					"mainMenu": "To return to the main menu,  press 9"
				}];
			context.succeed(choices[0]);
			break;
			
			
	case "3":
	
			    
			if(decoded.selectedOption == "1") {
			 choices = [{
            "digits":1,
            "lang":decoded.selectedlang,
            "action":"/dev/hearingrepeat?ecitnum="+authToken,
            "message1": "With full payment of all fines and penalties to ",
            "message2": "Metro Transit Court, P-O Box 866015, Los Angeles, California, 90086. ",
            "repeatMenu": "To repeat this message, press *",
            "mainMenu": "To return to the main menu, press 9",
            "options":[
                "If you have already paid the violation, and want to schedule an administrative hearing",
                "To find out the outstanding fine amount on the violation being contested",
                "To repeat the address"
                ]
            }];
		 } 
		 
		 if(decoded.selectedOption == "2"){
		    choices = [{
           "digits":1,
            "lang":decoded.selectedlang,
            "action":"/dev/hearingrepeat?ecitnum="+authToken,
            "message1": "With full payment of all fines and penalties to ",
            "message2": "Metro Transit Court, PO Box 866015, Los Angeles, California, 90086. ",
            "repeatMenu": "To repeat this message, press *",
            "mainMenu": "To return to the main menu, press 9",
            "options":[
                "If you have already paid the violation, and want to schedule an administrative hearing",
                "To find out the outstanding fine amount on the violation being contested",
                "To repeat the address"
                ]
		    }];
		 }
		    
			context.succeed(choices[0]);
			break;
			
			
			case "*":
			    var outt = {
				digits : 1,
                lang: decoded.selectedlang,
                action: "/dev/heraingcitation?lang="+decoded.selectedlang,
                message: "",
                options: [
                 "If you are calling about a transit violation",
                 "If you are calling about a parking violation"
                  ]
                };
			context.succeed(outt);
			break;
			
			
			case "9":
                choices = [{
                    "digits":1,
                    "lang": decoded.selectedlang,
                    "action":"/dev/violationmenu?lang="+decoded.selectedlang,
                    "message": "",
                    "options":[
                        "To check the amount due on a violation",
                        "To find out how to pay for a violation",
                        "For information on how to contest a violation, or schedule an administrative hearing"
                    ],
                    "repeatMenu":"To repeat this menu, press *"
                }];
            context.succeed(choices[0]);
            break;
		 }
		 
		}
		
		
		if(lang == "es-MX") {
		
			if(validOptions.indexOf(enteredNum) == -1) {
				var inputInvalid = "";
				if(decoded.invalidCount != undefined) {
					inputInvalid = decoded.invalidCount;
				}
				inputInvalid = inputInvalid + "1"; 
				
				var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption,invalidCount: inputInvalid },
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
				if(inputInvalid == "111") {
					choices = [{
						"digits":0,
						"lang": decoded.selectedlang,
						"action":"",
						"message": "",
						"options":[],
						"redirectAction":"YES",
						"timeOut":"0"
					}];
				}else {
					choices = [{
						"digits":1,
						"lang": decoded.selectedlang,
						"action":"/dev/hearingrepeat?ecitnum="+authTokenTryAgain,
						"message1": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
						"repeatMenu": "Para repetir este mensaje, presione estrella",
            			"mainMenu": "Para regresar al menú principal, presione nueve",
            			"optionSpanish":[
                			"Si ya ha pagado la multa y desea citar una audiencia administrativa....presione uno",
                			"Para averiguar el monto de la multa pendiente sobre la violación que está siendo disputada, presione dos",
                			"Para repetir la dirección, pulse tres"
                		]
					}];
				}
			context.succeed(choices[0]);
		}
	
		
	switch(enteredNum) {
	case "1":
			
	var date = new Date();
	console.log('In select Language:: '+date);
	var checkHoliday = require('custom-metro-holidaylist')(date);
	console.log("checkHoliday:: "+checkHoliday);	
	
    var businessHours = require('business-hours')(7, 16);
	var isInBusinessHours = businessHours.isBusinessHours();
	console.log("isInBusinessHours:: "+isInBusinessHours);

    if(checkHoliday || !isInBusinessHours) {
	console.log('holiday or not business hours');
    choices = [
        {
           "digits":1,
           "lang": decoded.selectedlang,
            "action":"",
            "message1": "Su llamada debe ser manejada por un representante de la corte de transito de metro. Nuestro horario es de 8:00 am-4pm de lunes a viernes, excepto días feriados. por favor llamar de vuelta durante nuestro horario comercial regular.",
            "options":[],
			"timeOut":"0"
        }
      ]
    }else {
        choices = [
        {
           "digits":1,
           "lang": decoded.selectedlang,
            "action":"dialcustomer",
            "message1": "Por favor, mantenga, un representante de la corte de transito de metro estará con usted en breve. Esta llamada puede ser monitoreada para fines de garantía de calidad.",
            "options":[] 
        }
      ]
    }
	
	context.succeed(choices[0]);
				
	break;
	
	
	case "2":
			choices = [{
					"digits":1,
					"lang": decoded.selectedlang,
					"action": "/dev/readviolationprefix?lang="+decoded.selectedlang,
					"message1": "Por favor tenga su violación lista. Encontrará el número de violación en la esquina superior derecha. Puede repetir este menú en cualquier momento pulsando la tecla de la estrella.",
					"optionSpanish":[
					"Si su multa comienza con una T presione uno",
					"Si comienza con una P presione dos"
					],
					"repeatMenu": "Para repetir este menú, presione estrella",
					"mainMenu": "Para regresar al menú principal, presione nueve"
				}];
			context.succeed(choices[0]);
			break;
			
			
	case "3":
			    
			if(decoded.selectedOption == "1") {
			 choices = [{
            "digits":1,
            "lang":decoded.selectedlang,
            "action":"/dev/hearingrepeat?ecitnum="+authToken,
            "message1": "Con el pago completo de todas las multas y multas a ",
            "message2": "Metro Transit Court, P-O Box 8 6 6 0 1 5, Los Ángeles, California, 9 0 0 8 6. Asegúrese de incluir su número de multa en el cheque o giro postal.",
            "repeatMenu": "Para repetir este mensaje, presione estrella",
            "mainMenu": "Para regresar al menú principal, presione nueve",
            "optionSpanish":[
                			"Si ya ha pagado la multa y desea citar una audiencia administrativa....presione uno",
                			"Para averiguar el monto de la multa pendiente sobre la violación que está siendo disputada, presione dos",
                			"Para repetir la dirección, pulse tres"
                		]
            }];
		 } 
		 
		 if(decoded.selectedOption == "2"){
		    choices = [{
           "digits":1,
            "lang":decoded.selectedlang,
            "action":"/dev/hearingrepeat?ecitnum="+authToken,
            "message1": "Con el pago completo de todas las multas y multas a",
            "message2": "Metro Transit Court, P-O Box 8 6 6 0 1 5, Los Ángeles, California, 9 0 0 8 6. Asegúrese de incluir su número de multa en el cheque o giro postal. ",
            "repeatMenu": "Para repetir este mensaje, presione estrella",
            "mainMenu": "Para regresar al menú principal, presione nueve",
            "optionSpanish":[
                			"Si ya ha pagado la multa y desea citar una audiencia administrativa....presione uno",
                			"Para averiguar el monto de la multa pendiente sobre la violación que está siendo disputada, presione dos",
                			"Para repetir la dirección, pulse tres"
                ]
		    }];
		 }
		    
			context.succeed(choices[0]);
			break;
			
			
			case "*":
			    var outt = {
				digits : 1,
                lang: decoded.selectedlang,
                action: "/dev/heraingcitation?lang="+decoded.selectedlang,
                message: "",
                optionSpanish: [
                 	"Si llama acerca de una multa de transito… presione uno",
                 	"Si llama acerca de una multa de estacionamiento... presione dos"
                  ]
                };
			context.succeed(outt);
			break;
			
			
			case "9":
                choices = [{
                    "digits":1,
                    "lang": decoded.selectedlang,
                    "action":"/dev/violationmenu?lang="+decoded.selectedlang,
                    "message": "",
                    "optionSpanish":[
                        "Para verificar el saldo de una multa ... presione uno",
                        "Información para pagar su multa ... presione dos",
                        "Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
                    ],
                    "repeatMenu":"Para repetir este menú, presione estrella"
                }];
            context.succeed(choices[0]);
            break;
		 }
		}		
	}
  });
};

function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}