console.log('Loading paymentstatusrepeat selection');

exports.handler = function (event, context) {

	qryObject = parseQuery(event.reqbody);
	var enteredNum = qryObject['Digits'];
	var ecitnumEncoded = event.ecitnum;
	var choices = [];
	var jwt = require('jsonwebtoken');
	
	jwt.verify(ecitnumEncoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671', function (err, decoded) {
		if (err) {
			//Need to send user to first screen
		} else {

			var licPlateNumArray = decoded.citationStatus.licencePlateNumber.split('');
			
			var authTokenWithStatus = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum, citationStatus: decoded.citationStatus }, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });

			var validOptions = ["1", "*", "9"];
			var lang = decoded.selectedlang;
			
			
	if (lang == "en-US") {
		
		var licPlateNumToSay = '';
		for (var i = 0; i < licPlateNumArray.length; i++) {
			licPlateNumToSay = licPlateNumToSay + licPlateNumArray[i] + ',';
		}
		
		if(validOptions.indexOf(enteredNum) == -1) {
				var inputInvalid = "";
				if(decoded.invalidCount != undefined) {
					inputInvalid = decoded.invalidCount;
				}
				inputInvalid = inputInvalid + "1"; 
				
				var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption , userCitNum : decoded.userCitNum, citationStatus: decoded.citationStatus, invalidCount: inputInvalid }, 
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
				if(inputInvalid == "111") {
					choices = [{
						"digits":0,
						"lang": decoded.selectedlang,
						"action":"",
						"message": "",
						"options":[],
						"redirectAction":"YES",
						"timeOut":"0"
					}];
				}else {
					choices = [{
						"digits":1,
						"lang": decoded.selectedlang,
						"action":"/dev/paymentstatusrepeat?ecitnum="+authTokenTryAgain,
						"message": "We are sorry, your entry was invalid. Please try again.",
						"repeatMenu": "To repeat this information,  press *",
						"payOptn": "For payment options, press 1",
						"mainMenu": "To return to the main menu,  press 9"
					}];
				}
			context.succeed(choices[0]);
		}
		
				switch (enteredNum) {
					case "*":

						//for transit
						if (decoded.selectedOption == "1") {

							if (decoded.citationStatus.paymentStatus == "PARTIALPAYMENTDUE") {


								if (decoded.citationStatus.licencePlateNumber != "" && decoded.citationStatus.allPenalties > decoded.citationStatus.totalDue) {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "A partial payment of </Say><Pause length='1'></Pause><Say language='en-US'>$" + decoded.citationStatus.amountPaid + " was received. The unpaid balance is </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.totalDue,
										"submessage1": "To close this ticket, this amount must be paid in full.",
										"submessage2": "For license plate " + licPlateNumToSay + " the total amount due with all penalties is $" + decoded.citationStatus.allPenalties,
										"repeatMenu": "To repeat this information,  press *",
										"payOptn": "For payment options, press 1",
										"mainMenu": "To return to the main menu,  press 9",
										"options": []
									}]
								} else {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "A partial payment of </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.amountPaid + " was received. The unpaid balance is </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.totalDue,
										"submessage1": "To close this ticket, this amount must be paid in full.",
										"submessage2": "",
										"repeatMenu": "To repeat this information,  press *",
										"payOptn": "For payment options, press 1",
										"mainMenu": "To return to the main menu,  press 9",
										"options": []
									}]

								}


							} else if (decoded.citationStatus.paymentStatus == "NOPAYMENTMADE") {


								if (decoded.citationStatus.licencePlateNumber != "" && decoded.citationStatus.allPenalties > decoded.citationStatus.totalDue) {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "No payments have been received as of today. The current amount due is  </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.totalDue,
										"submessage1": "To close this ticket, this amount must be paid in full.",
										"submessage2": "For license plate " + licPlateNumToSay + " , the total amount due with all penalties is </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.allPenalties,
										"repeatMenu": "To repeat this information,  press *",
										"payOptn": "For payment options, press 1",
										"mainMenu": "To return to the main menu,  press 9",
										"options": []
									}]
								} else {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "No payments have been received as of today. The current amount due is </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.totalDue,
										"submessage1": "To close this ticket, this amount must be paid in full.",
										"submessage2": "",
										"repeatMenu": "To repeat this information,  press *",
										"payOptn": "For payment options, press 1",
										"mainMenu": "To return to the main menu,  press 9",
										"options": []
									}]
								}

							} else {
								//error
							}

						}

						//for parking
						if (decoded.selectedOption == "2") {
							if (decoded.citationStatus.paymentStatus == "PARTIALPAYMENTDUE") {
								if (decoded.citationStatus.licencePlateNumber != "" && decoded.citationStatus.allPenalties > decoded.citationStatus.totalDue) {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "A partial payment of </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.amountPaid + " was received. The unpaid balance is </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.totalDue,
										"submessage1": "To close this ticket, this amount must be paid in full.",
										"submessage2": "For license plate " + licPlateNumToSay + " , the total amount due with all penalties is </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.allPenalties,
										"repeatMenu": "To repeat this information,  press *",
										"payOptn": "For payment options, press 1",
										"mainMenu": "To return to the main menu,  press 9",
										"options": []
									}]

								} else {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "A partial payment of </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.amountPaid + " was received. The unpaid balance is </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.totalDue,
										"submessage1": "To close this ticket, this amount must be paid in full.",
										"submessage2": "",
										"repeatMenu": "To repeat this information,  press *",
										"payOptn": "For payment options, press 1",
										"mainMenu": "To return to the main menu,  press 9",
										"options": []
									}]
								}

							} else if (decoded.citationStatus.paymentStatus == "NOPAYMENTMADE") {

								if (decoded.citationStatus.licencePlateNumber != "" && decoded.citationStatus.allPenalties > decoded.citationStatus.totalDue) {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "No payments have been received as of today. The current amount due is  </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.totalDue,
										"submessage1": "To close this ticket, this amount must be paid in full.",
										"submessage2": "For license plate " + licPlateNumToSay + " , the total amount due with all penalties is </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.allPenalties,
										"repeatMenu": "To repeat this information,  press *",
										"payOptn": "For payment options, press 1",
										"mainMenu": "To return to the main menu,  press 9",
										"options": []
									}]
								} else {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "No payments have been received as of today. The current amount due is </Say><Pause length='1'></Pause><Say language='en-US'> $" + decoded.citationStatus.totalDue,
										"submessage1": "To close this ticket, this amount must be paid in full.",
										"submessage2": "",
										"repeatMenu": "To repeat this information,  press *",
										"payOptn": "For payment options, press 1",
										"mainMenu": "To return to the main menu,  press 9",
										"options": []
									}]
								}
							} else {
								//error
							}

						}//end if

						context.succeed(choices[0]);

						break;
					case "9":
						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/violationmenu?lang=" + decoded.selectedlang,
							"message": "",
							"options": [
								"To check the amount due on a violation",
								"To find out how to pay for a violation",
								"For information on how to contest a violation, or schedule an administrative hearing"
							],
							"repeatMenu": "To repeat this menu, press *"
						}];
						context.succeed(choices[0]);
						break;

					case "1":

						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/payoptions?ecitnum=" + authTokenWithStatus,
							"message": "",
							"options": [
								"To learn how to pay your violation on the internet",
								"To pay now with a Visa or Mastercard",
								"To learn about paying by mail",
								"To learn about paying in person"
							],
							"repeatMenu": "To repeat these options, press *",
							"mainMenu": "To return to the main menu,  press 9",
						}];
						context.succeed(choices[0]);
						break;


				}
			} 
			
	if (lang == "es-MX") {
		
		var licPlateNumToSaySpanish = '';
		for (var i = 0; i < licPlateNumArray.length; i++) {
			licPlateNumToSaySpanish = licPlateNumToSaySpanish + licPlateNumArray[i] + "</Say><Say language='es-MX'><Pause length='1'></Pause>";
		}
				
			if(validOptions.indexOf(enteredNum) == -1) {
				var inputInvalid = "";
				if(decoded.invalidCount != undefined) {
					inputInvalid = decoded.invalidCount;
				}
				inputInvalid = inputInvalid + "1"; 
				
				var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption , userCitNum : decoded.userCitNum, citationStatus: decoded.citationStatus, invalidCount: inputInvalid }, 
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
				if(inputInvalid == "111") {
					choices = [{
						"digits":0,
						"lang": decoded.selectedlang,
						"action":"",
						"message": "",
						"options":[],
						"redirectAction":"YES",
						"timeOut":"0"
					}];
				}else {
					choices = [{
						"digits":1,
						"lang": decoded.selectedlang,
						"action":"/dev/paymentstatusrepeat?ecitnum="+authTokenTryAgain,
						"message": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
						"repeatMenu": "Para repetir esta información, presione Estrella",
						"payOptn": "Para opciones de pago, presione uno",
						"mainMenu": "Para regresar al menú principal, presione nueve",
						"options":[]
					}];
				}
			context.succeed(choices[0]);
		}		
				
				
			
				switch (enteredNum) {
					case "*":

						//for transit
						if (decoded.selectedOption == "1") {

							if (decoded.citationStatus.paymentStatus == "PARTIALPAYMENTDUE") {


								if (decoded.citationStatus.licencePlateNumber != "" && decoded.citationStatus.allPenalties > decoded.citationStatus.totalDue) {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "Un pago parcial de </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.amountPaid + " fue recibido. El saldo es $" + decoded.citationStatus.totalDue,
										"submessage1": "Para cerrar esta multa, la cantidad debe ser pagado en su totalidad.",
										"submessage2": "Para placa de carro" + licPlateNumToSaySpanish + " La cantidad debida con todas las penalidades es </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.allPenalties,
										"repeatMenu": "Para repetir esta información, presione Estrella",
										"payOptn": "Para opciones de pago, presione uno",
										"mainMenu": "Para regresar al menú principal, presione nueve",
										"options": []
									}]
								} else {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "Un pago parcial de </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.amountPaid + " fue recibido. El saldo es </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.totalDue,
										"submessage1": "Para cerrar esta multa, la cantidad debe ser pagado en su totalidad.",
										"submessage2": "",
										"repeatMenu": "Para repetir esta información, presione Estrella",
										"payOptn": "Para opciones de pago, presione uno",
										"mainMenu": "Para regresar al menú principal, presione nueve",
										"options": []
									}]

								}


							} else if (decoded.citationStatus.paymentStatus == "NOPAYMENTMADE") {


								if (decoded.citationStatus.licencePlateNumber != "" && decoded.citationStatus.allPenalties > decoded.citationStatus.totalDue) {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "No se han recibido pagos a partir de la fecha de hoy. El monto actual debido es </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.totalDue,
										"submessage1": "Para cerrar esta multa, la cantidad debe ser pagado en su totalidad.",
										"submessage2": "For license plate " + licPlateNumToSaySpanish + " , La cantidad debida con todas las penalidades es </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.allPenalties,
										"repeatMenu": "Para repetir esta información, presione Estrella",
										"payOptn": "Para opciones de pago, presione uno",
										"mainMenu": "Para regresar al menú principal, presione nueve",
										"options": []
									}]
								} else {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "No se han recibido pagos a partir de la fecha de hoy. El monto actual debido es </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.totalDue,
										"submessage1": "Para cerrar esta multa, la cantidad debe ser pagado en su totalidad.",
										"submessage2": "",
										"repeatMenu": "Para repetir esta información, presione Estrella",
										"payOptn": "Para opciones de pago, presione uno",
										"mainMenu": "Para regresar al menú principal, presione nueve",
										"options": []
									}]
								}

							} else {
								//error
							}

						}

						//for parking
						if (decoded.selectedOption == "2") {
							if (decoded.citationStatus.paymentStatus == "PARTIALPAYMENTDUE") {
								if (decoded.citationStatus.licencePlateNumber != "" && decoded.citationStatus.allPenalties > decoded.citationStatus.totalDue) {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "Un pago parcial de </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.amountPaid + " fue recibido. El saldo es $" + decoded.citationStatus.totalDue,
										"submessage1": "Para cerrar esta multa, la cantidad debe ser pagado en su totalidad.",
										"submessage2": "Para placa de carro " + licPlateNumToSaySpanish + " , La cantidad debida con todas las penalidades es </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.allPenalties,
										"repeatMenu": "Para repetir esta información, presione Estrella",
										"payOptn": "Para opciones de pago, presione uno",
										"mainMenu": "Para regresar al menú principal, presione nueve",
										"options": []
									}]

								} else {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "Un pago parcial de  </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.amountPaid + " was received. The unpaid balance is </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.totalDue,
										"submessage1": "Para cerrar esta multa, la cantidad debe ser pagado en su totalidad.",
										"submessage2": "",
										"repeatMenu": "Para repetir esta información, presione Estrella",
										"payOptn": "Para opciones de pago, presione uno",
										"mainMenu": "Para regresar al menú principal, presione nueve",
										"options": []
									}]
								}

							} else if (decoded.citationStatus.paymentStatus == "NOPAYMENTMADE") {

								if (decoded.citationStatus.licencePlateNumber != "" && decoded.citationStatus.allPenalties > decoded.citationStatus.totalDue) {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "No se han recibido pagos a partir de la fecha de hoy. El monto actual debido es </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.totalDue,
										"submessage1": "Para cerrar esta multa, la cantidad debe ser pagado en su totalidad.",
										"submessage2": "Para placa de carro" + licPlateNumToSaySpanish + " , La cantidad debida con todas las penalidades es </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.allPenalties,
										"repeatMenu": "Para repetir esta información, presione Estrella",
										"payOptn": "Para opciones de pago, presione uno",
										"mainMenu": "Para regresar al menú principal, presione nueve",
										"options": []
									}]
								} else {
									choices = [{
										"digits": 1,
										"lang": decoded.selectedlang,
										"action": "/dev/paymentstatusrepeat?ecitnum=" + authTokenWithStatus,
										"message": "No se han recibido pagos a partir de la fecha de hoy. El monto actual debido es  </Say><Pause length='1'></Pause><Say language='es-MX'> $" + decoded.citationStatus.totalDue,
										"submessage1": "Para cerrar esta multa, la cantidad debe ser pagado en su totalidad.",
										"submessage2": "",
										"repeatMenu": "Para repetir esta información, presione Estrella",
										"payOptn": "Para opciones de pago, presione uno",
										"mainMenu": "Para regresar al menú principal, presione nueve",
										"options": []
									}]
								}
							} else {
								//error
							}

						}//end if

						context.succeed(choices[0]);

						break;
					case "9":
						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/violationmenu?lang=" + decoded.selectedlang,
							"message": "",
							"optionSpanish": [
								"Para verificar el saldo de una multa ... presione uno",
								"Información para pagar su multa ... presione dos",
								"Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
							],
							"repeatMenu": "To repeat this menu, press *"
						}];
						context.succeed(choices[0]);
						break;

					case "1":

						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/payoptions?ecitnum=" + authTokenWithStatus,
							"message": "",
							"optionSpanish": [
								"Para aprender como pagar su multa en Internet, presione uno",
								"Para realizar su pago con una tarjeta Visa o MasterCard...presione dos",
								"Para información acerca de cómo realizar un pago por correo…presione tres",
								"Para aprender sobre el pago en persona, presione cuatro"
							],
							"repeatMenu": "Para repetir estas opciones, presione estrella",
							"mainMenu": "Para regresar al menú principal, presione nueve",
						}];
						context.succeed(choices[0]);
						break;


				}

			}
		}
	});
};

function parseQuery(qstr) {
	var query = {};
	var a = qstr.substr(0).split('&');
	for (var i = 0; i < a.length; i++) {
		var b = a[i].split('=');
		query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
	}
	return query;
}