exports.handler = function (event, context) {

	qryObject = parseQuery(event.reqbody);
	var enteredNum = qryObject['Digits'];
	var ecitnumEncoded = event.ecitnum;
	var choices = [];
	var jwt = require('jsonwebtoken');
	
	jwt.verify(ecitnumEncoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671', function (err, decoded) {
		if (err) {
			//Need to send user to first screen
		} else {

			var authTokenWithStatus = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum, citationStatus: decoded.citationStatus }, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });

			var validOptions = ["1", "2", "3", "4", "*", "9"];
			var lang = decoded.selectedlang;
			
		if (lang == "en-US") {
			if (validOptions.indexOf(enteredNum) == -1) {
					
				var inputInvalid = "";
				if(decoded.invalidCount != undefined) {
					inputInvalid = decoded.invalidCount;
				}
				inputInvalid = inputInvalid + "1"; 
				
				var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum, citationStatus: decoded.citationStatus, invalidCount: inputInvalid },
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
				if(inputInvalid == "111") {
					choices = [{
						"digits":0,
						"lang": decoded.selectedlang,
						"action":"/dev/customerservicescall?lang="+decoded.selectedlang,
						"message": "",
						"options":[],
						"redirectAction":"YES",
						"timeOut":"0"
					}];
				}else {
					choices = [{
						"digits": 1,
						"lang": decoded.selectedlang,
						"action": "/dev/payoptions?ecitnum="+authTokenTryAgain,
						"message": "We are sorry, your entry was invalid. Please try again",
						"options": [
								"To learn how to pay your violation on the internet",
								"To pay now with a Visa or Mastercard",
								"To learn about paying by mail",
								"To learn about paying in person"
							],
						"repeatMenu": "To repeat these options, press *",
						"mainMenu": "To return to the main menu,  press 9"
					}];
				}
				context.succeed(choices[0]);
			}

				switch (enteredNum) {
					case "*":

						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/payoptions?ecitnum=" + authTokenWithStatus,
							"message": "",
							"options": [
								"To learn how to pay your violation on the internet",
								"To pay now with a Visa or Mastercard",
								"To learn about paying by mail",
								"To learn about paying in person"
							],
							"repeatMenu": "To repeat these options, press *",
							"mainMenu": "To return to the main menu,  press 9"
						}];

						context.succeed(choices[0]);

						break;
					case "9":
						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/violationmenu?lang=" + decoded.selectedlang,
							"message": "",
							"options": [
								"To check the amount due on a violation",
								"To find out how to pay for a violation",
								"For information on how to contest a violation, or schedule an administrative hearing"
							],
							"repeatMenu": "To repeat this menu, press *"
						}];
						context.succeed(choices[0]);
						break;

					default:

						if (decoded.citationStatus.licencePlateNumber != "" && decoded.citationStatus.allPenalties > decoded.citationStatus.totalDue) {

							choices = [{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/paybyinternet?lang=" + decoded.selectedlang,
								"message": "Violations can be paid on the internet by accessing our web site at W-W-W dot Metro dot net, slash, Pay Violation. The web site offers easy to use and secure credit card payment transactions 24 hours a day, 7 days a week.",
								"options": [],
								"repeatMenu": "To repeat this message,  press *",
								"mainMenu": "To return to the main menu,  press 9"
							},
							{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/paybyphone?ecitnum=" + authTokenWithStatus,
								"message": "",
								"options": [
									"To pay only the amount due on this ticket, $" + decoded.citationStatus.totalDue,
									"To pay the entire amount owed on this license plate, $" + decoded.citationStatus.allPenalties
								],
								"repeatMenu": "To repeat these options, press *",
								"mainMenu": "To return to the previous menu, press 8"
							},
							{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/payusingmail?lang=" + decoded.selectedlang,
								"message": "Checks or money orders are accepted; please do not send cash.   Write your violation number on your payment.",
								"options": [
									"To pay your transit violation",
									"To pay your parking violation"
								]
							},
							{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/paybyperson?lang=" + decoded.selectedlang,
								"message": "To pay in person please go to Metro Transit Court located on the plaza level at One Gateway Plaza in Los Angeles, California, 9 0 0 1 2, open Monday through Thursday  from 9 A-M to 3 P-M. ",
								"options": [],
								"repeatMenu": "To repeat this information,  press *",
								"mainMenu": "To select another menu option, press 9"
							}];



						} else {

							var fullCitationNum = "";
							if (decoded.selectedOption == "1") {
								fullCitationNum = "T" + decoded.userCitNum;
							}
							if (decoded.selectedOption == "2") {
								fullCitationNum = "P" + decoded.userCitNum;
							}

							var authTokenWithStatusAmount = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum, ecitnum: fullCitationNum, citationStatus: decoded.citationStatus, amount: '$' + decoded.citationStatus.totalDue }, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });

							choices = [{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/paybyinternet?lang=" + decoded.selectedlang,
								"message": "Violations can be paid on the internet by accessing our web site at W-W-W dot Metro dot net, slash, Pay Violation. The web site offers easy to use and secure credit card payment transactions 24 hours a day, 7 days a week.",
								"options": [],
								"repeatMenu": "To repeat this message,  press *",
								"mainMenu": "To return to the main menu,  press 9"
							},
							{
								"digits": 18,
								"lang": decoded.selectedlang,
								"action": "/dev/ccenter?ecitnum=" + authTokenWithStatusAmount,
								"message": "",
								"options": [],
								"repeatMenu": "Please enter your credit card number now, followed by the pound sign. "
							},
							{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/payusingmail?lang=" + decoded.selectedlang,
								"message": "Checks or money orders are accepted; please do not send cash.   Write your violation number on your payment.",
								"options": [
									"To pay your transit violation",
									"To pay your parking violation"
								]
							},
							{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/paybyperson?lang=" + decoded.selectedlang,
								"message": "To pay in person please go to Metro Transit Court located on the plaza level at One Gateway Plaza in Los Angeles, California, 9 0 0 1 2, open Monday through Thursday  from 9 A-M to 3 P-M. ",
								"options": [],
								"repeatMenu": "To repeat this information,  press *",
								"mainMenu": "To select another menu option, press 9"
							}];

						}

						context.succeed(choices[qryObject['Digits'] - 1]);
						break;
				}
			}
			
	
	if (lang == "es-MX") {
			if (validOptions.indexOf(enteredNum) == -1) {
				
				var inputInvalid = "";
				if(decoded.invalidCount != undefined) {
					inputInvalid = decoded.invalidCount;
				}
				inputInvalid = inputInvalid + "1"; 
				
				var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum, citationStatus: decoded.citationStatus, invalidCount: inputInvalid },
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
				if(inputInvalid == "111") {
					choices = [{
						"digits":0,
						"lang": decoded.selectedlang,
						"action":"/dev/customerservicescall?lang="+decoded.selectedlang,
						"message": "",
						"options":[],
						"redirectAction":"YES",
						"timeOut":"0"
					}];
				}else {
					choices = [{
						"digits": 1,
						"lang": decoded.selectedlang,
						"action": "/dev/payoptions?ecitnum="+authTokenTryAgain,
						"message": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
						"optionSpanish": [
								"Para aprender como pagar su multa en Internet, presione uno",
								"Para realizar su pago con una tarjeta Visa o MasterCard…presione dos",
								"Para información acerca de cómo realizar un pago por correo… pulse tres",
								"Para aprender sobre el pago en persona, presione cuatro"
							],
						"repeatMenu": "Para repetir estas opciones, presione estrella",
						"mainMenu": "Para regresar al menú principal, presione nueve",
					}];
				}
				context.succeed(choices[0]);
			}

				switch (enteredNum) {
					case "*":

						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/payoptions?ecitnum=" + authTokenWithStatus,
							"message": "",
							"optionSpanish": [
								"Para aprender como pagar su multa en Internet, presione uno",
								"Para realizar su pago con una tarjeta Visa o MasterCard…presione dos",
								"Para información acerca de cómo realizar un pago por correo… pulse tres",
								"Para aprender sobre el pago en persona, presione cuatro"
							],
							"repeatMenu": "Para repetir estas opciones, presione estrella",
							"mainMenu": "Para regresar al menú principal, presione nueve"
						}];

						context.succeed(choices[0]);

						break;
					case "9":
						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/violationmenu?lang=" + decoded.selectedlang,
							"message": "",
							"optionSpanish": [
								"Para verificar el saldo de una multa ... presione uno",
								"Información para pagar su multa ... presione dos",
								"Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
							],
							"repeatMenu": "Para repetir este menú, presione estrella"
						}];
						context.succeed(choices[0]);
						break;

					default:

						if (decoded.citationStatus.licencePlateNumber != "" && decoded.citationStatus.allPenalties > decoded.citationStatus.totalDue) {

							choices = [{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/paybyinternet?lang=" + decoded.selectedlang,
								"message": "Multas pueden ser pagadas en Internet accediendo a nuestro sitio web en W-W-W Metro punto net, barra, PayViolation. El sitio web ofrece un sistema de pago con tarjeta de crédito seguro y fácil de usar las 24 horas del día, los 7 días de la semana.",
								"options": [],
								"repeatMenu": "Para repetir estas opciones, presione estrella",
								"mainMenu": "Para regresar al menú principal, presione nueve"
							},
							{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/paybyphone?ecitnum=" + authTokenWithStatus,
								"message": "",
								"optionSpanish": [
									"Para pagar solamente el monto debido, $" + decoded.citationStatus.totalDue +" presione uno ",
									"Para pagar la cantidad adeudada en esta placa, $" + decoded.citationStatus.allPenalties+" presione dos "
								],
								"repeatMenu": "Para repetir estas opciones, presione estrella",
								"mainMenu": "Para regresar al menú anterior, presione ocho"
							},
							{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/payusingmail?lang=" + decoded.selectedlang,
								"message": "Se aceptan cheques o giros postales, por favor, no envíe dinero en efectivo.  Indique en el pago su número de multa.",
								"optionSpanish": [
									"Para realizar el pago de su multa de tansito… presione uno",
									"Para realizar el pago de su multa de estacionamiento… presione dos"
								]
							},
							{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/paybyperson?lang=" + decoded.selectedlang,
								"message": "Para pagar en persona, por favor vaya a la corte de transito de metro ubicado en el nivel plaza en One Gateway Plaza en Los Angeles, California, 9 0 0 1 2, abierto de lunes a jueves de 9 A-M a 3 P-M. ",
								"options": [],
								"repeatMenu": "Para repetir esta información, presione Estrella",
								"mainMenu": "Para seleccionar otra opción de menú, presione nueve"
							}];



						} else {

							var fullCitationNum = "";
							if (decoded.selectedOption == "1") {
								fullCitationNum = "T" + decoded.userCitNum;
							}
							if (decoded.selectedOption == "2") {
								fullCitationNum = "P" + decoded.userCitNum;
							}

							var authTokenWithStatusAmount = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum, ecitnum: fullCitationNum, citationStatus: decoded.citationStatus, amount: '$' + decoded.citationStatus.totalDue }, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });

							choices = [{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/paybyinternet?lang=" + decoded.selectedlang,
								"message": "Multas pueden ser pagadas en Internet accediendo a nuestro sitio web en W-W-W Metro punto net, barra, PayViolation. El sitio web ofrece un sistema de pago con tarjeta de crédito seguro y fácil de usar las 24 horas del día, los 7 días de la semana",
								"options": [],
								"repeatMenu": "Para repetir estas opciones, presione estrella",
								"mainMenu": "Para regresar al menú principal, presione nueve"
							},
							{
								"digits": 18,
								"lang": decoded.selectedlang,
								"action": "/dev/ccenter?ecitnum=" + authTokenWithStatusAmount,
								"message": "",
								"options": [],
								"repeatMenu": "Introduzca su número de tarjeta de crédito ahora, seguido por el signo de libra. "
							},
							{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/payusingmail?lang=" + decoded.selectedlang,
								"message": "Se aceptan cheques o giros postales, por favor, no envíe dinero en efectivo.  Indique en el pago su número de multa.",
								"optionSpanish": [
									"Para realizar el pago de su multa de tansito…presione uno",
									"Para realizar el pago de su multa de estacionamiento…presione dos"
								]
							},
							{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/paybyperson?lang=" + decoded.selectedlang,
								"message": "Para pagar en persona, por favor vaya a la corte de transito de metro ubicado en el nivel plaza en One Gateway Plaza en Los Angeles, California, 9 0 0 1 2, abierto de lunes a jueves de 9 A-M a 3 P-M.",
								"options": [],
								"repeatMenu": "Para repetir esta información, presione Estrella",
								"mainMenu": "Para seleccionar otra opción de menú, presione nueve"
							}];

						}

						context.succeed(choices[qryObject['Digits'] - 1]);
						break;
				}
			

			}
		}
	});
};

function parseQuery(qstr) {
	var query = {};
	var a = qstr.substr(0).split('&');
	for (var i = 0; i < a.length; i++) {
		var b = a[i].split('=');
		query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
	}
	return query;
}