console.log('Loading contest tic waiting res check');
exports.handler = function(event, context) {
    
    qryObject = parseQuery(event.reqbody);
    var enteredNum = qryObject['Digits'];
    var langObj = event.lang;
    var lang = "";
    var inputInvalid = "";
		
    if(langObj.indexOf("invalidCount") !== -1) {
        var langArr = langObj.split('00');
        lang = langArr[0];
        inputInvalidTemp = langArr[1];
        var inputInvalidArr = inputInvalidTemp.split('=');
        inputInvalid = inputInvalidArr[1];
    }else {
        lang = event.lang;
    }
	var choices = [];
	var validOptions = ["1", "9"];
	
	
if (lang == "en-US") {	
	
	if(validOptions.indexOf(enteredNum) == -1) {
		inputInvalid = inputInvalid + "1"; 
		if(inputInvalid == "111") {
			choices = [{
					"digits":1,
					"lang": lang,
					"action":"",
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
			}];
			context.succeed(choices[0]);
		}else {
			choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/contestticwaitingres?lang=" + lang+"00invalidCount="+inputInvalid,
					"message": "We are sorry, your entry was invalid. Please try again",
					"options": [
						"If you have not received confirmation of your request for an initial review or administrative hearing and a request was made more than sixty days ago…"
					],
					"repeatMenu": "To return to the main menu,  press 9"
			}];
		}
		context.succeed(choices[0]);
	}
	
	
	switch(enteredNum) {
	
	case "1":
			
	var date = new Date();
	console.log('In select Language:: '+date);
	var checkHoliday = require('custom-metro-holidaylist')(date);
	console.log("checkHoliday:: "+checkHoliday);
								
    var businessHours = require('business-hours')(7, 16);
	var isInBusinessHours = businessHours.isBusinessHours();
	console.log("isInBusinessHours:: "+isInBusinessHours);

    if(checkHoliday || !isInBusinessHours) {
	console.log('holiday or not business hours');
    choices = [{
           "digits":1,
           "lang": lang,
            "action":"",
            "message": "Your call must be handled by a Metro Transit Court representative. Our hours are 8:00 am-4pm Monday through Friday, except holidays. Please call back during our regular business hours.",
            "options":[],
			"timeOut":"0"
        }];

    }else {
        choices = [{
           "digits":1,
           "lang": lang,
            "action":"dialcustomer",
            "message": "Please hold, a Transit Court representative will be with you shortly. This call may be monitored for quality assurance purposes.",
            "options":[] 
        }];
    }
	
	context.succeed(choices[0]);
				
	break;
	
	case "9":
            choices = [{
                    "digits":1,
                    "lang": lang,
                    "action":"/dev/violationmenu?lang="+lang,
                    "message": "",
                    "options":[
                        "To check the amount due on a violation",
                        "To find out how to pay for a violation",
                        "For information on how to contest a violation, or schedule an administrative hearing"
                    ],
                    "repeatMenu":"To repeat this menu, press *"
                }];
            context.succeed(choices[0]);
            break;
	}
	
}


if (lang == "es-MX") {
	
	if(validOptions.indexOf(enteredNum) == -1) {
		inputInvalid = inputInvalid + "1"; 
		if(inputInvalid == "111") {
			choices = [{
					"digits":1,
					"lang": lang,
					"action":"",
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
			}];
			context.succeed(choices[0]);
		}else {
			choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/contestticwaitingres?lang=" + lang+"00invalidCount="+inputInvalid,
					"message": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
					"optionSpanish": [
						"Si no ha recibido confirmación de su solicitud de revisión inicial o de audiencia administrativa y presentó su solicitud hace más de 60 días...presione uno"
					],
					"repeatMenu": "Para regresar al menú principal, presione nueve"
			}];
		}
		context.succeed(choices[0]);
	}
	
	
	switch(enteredNum) {
	
	case "1":
			
	var date = new Date();
	console.log('In select Language:: '+date);
	var checkHoliday = require('custom-metro-holidaylist')(date);
	console.log("checkHoliday:: "+checkHoliday);
								
    var businessHours = require('business-hours')(7, 16);
	var isInBusinessHours = businessHours.isBusinessHours();
	console.log("isInBusinessHours:: "+isInBusinessHours);

    if(checkHoliday || !isInBusinessHours) {
	console.log('holiday or not business hours');
    choices = [{
           "digits":1,
           "lang": lang,
            "action":"",
            "message": "Su llamada debe ser manejada por un representante de la corte de transito de metro. Nuestro horario es de 8:00 am-4pm de lunes a viernes, excepto días feriados. por favor llamar de vuelta durante nuestro horario comercial regular.",
            "options":[],
			"timeOut":"0"
        }];

    }else {
        choices = [{
           "digits":1,
           "lang": lang,
            "action":"dialcustomer",
            "message": "Por favor, mantenga, un representante de la corte de transito de metro estará con usted en breve. Esta llamada puede ser monitoreada para fines de garantía de calidad.",
            "options":[] 
        }];

    }
	context.succeed(choices[0]);
				
	break;
	
	case "9":
            choices = [{
					"digits": 1,
					"lang": lang,
					"action": "/dev/violationmenu?lang=" + lang,
					"message": "",
					"optionSpanish": [
						"Para verificar el saldo de una multa ... presione uno",
						"Información para pagar su multa ... presione dos",
						"Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
					],
					"repeatMenu": "Para repetir este menú, presione estrella"
				}];
            context.succeed(choices[0]);
            break;
	}	
}
	
};

function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}