console.log('Loading repeatEntNum selection');

exports.handler = function(event, context) {
    
    qryObject = parseQuery(event.reqbody);
    var enteredNum = qryObject['Digits'];
    var ecitnumEncoded = event.ecitnum;
    var choices = [];
    var jwt = require('jsonwebtoken');
    			
	var enteredNumArray = enteredNum.split('');
    var convertedNum = '';
	var convertedNumSpanish = '';
	
    for(var i = 0;i<enteredNumArray.length;i++) {
        convertedNum = convertedNum + enteredNumArray[i]+',';
    }
	
	for(var i = 0;i<enteredNumArray.length;i++) {
        convertedNumSpanish = convertedNumSpanish + enteredNumArray[i]+"</Say><Say language='es-MX'><Pause length='1'></Pause>";
    }
    
	jwt.verify(ecitnumEncoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671', function(err, decoded) {    
		if (err) {
			//Need to send user to first screen
		}else{
			var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption , userCitNum : enteredNum}, 
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				var lang = decoded.selectedlang;
			if(lang == "en-US"){
			choices = [{
				"digits":1,
				"lang": decoded.selectedlang,
				"action":"/dev/confirmenterednum?ecitnum="+authToken,
				"message": "You entered "+convertedNum,
				"options":[
					"If this is correct, ",
					"To reenter the number, ",
				]
			}];
			context.succeed(choices[0]);
			}
			if(lang == "es-MX"){
				choices = [{
				"digits":1,
				"lang": decoded.selectedlang,
				"action":"/dev/confirmenterednum?ecitnum="+authToken,
				"message": "oprimio "+convertedNumSpanish,
				"optionSpanish":[
					"Si esto es correcto, presione uno, ",
					"Para volver a ingresar el número, presione dos, ",
				]
			}];
			context.succeed(choices[0]);
			}
		}
	});
};

function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}