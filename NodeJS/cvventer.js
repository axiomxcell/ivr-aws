exports.handler = function(event, context) {
	qryObject = parseQuery(event.reqbody);
	var ccnum = qryObject['Digits'];
	var token = event.ecitnum;
	var jwt = require('jsonwebtoken');
	var response = [];
	jwt.verify(token, 'e3b65b3a-2b52-11e7-93ae-92361f002671', function(err, decoded) {      
		if (err) {
			response.push({
				"digits":17,
				"action":"/dev/ccenter?ecitnum="+token,
				"message":err,
				"options":[]
			});
			context.succeed(response);
			//Need to send user to first screen
		}else{
			var ccData = { 
				ecitnum: decoded.ecitnum, 
				amount: decoded.amount, 
				cc: decoded.cc, 
				expiry: decoded.expiry,
				cvv: ccnum};
			var stripe = require("stripe")(
			  "sk_test_BoyVv6wl0ejvrnqDVW7hXN0t"
			);
			stripe.tokens.create({
			  card: {
				"number": ccData.cc,
				"exp_month": ccData.expiry.length >= 2? ccData.expiry.substring(0,2):ccData.expiry,
				"exp_year": ccData.expiry.length >= 2? ccData.expiry.substring(2):ccData.expiry,
				"cvc": ccData.cvv
			  }
			}, function(err, token) {
				if( err || !token || !token.id){
					var message = err;
					if( !err ){
					 message = "Invalid card details";
					}
					var authToken = jwt.sign({ ecitnum: ccData.ecitnum, amount: ccData.amount}, 
						'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
					response.push({
					"digits":17,
					"action":"/dev/ccenter?ecitnum="+authToken,
					"message":message + " Enter your credit card number followed by #",
					"options":[]
					});
					context.succeed(response);
				}
				else{
					var paymentInfo = {};
					var request = require('request');
					var reqData = {
						citationId : ccData.ecitnum,
						amount : ccData.amount,
						cardToken : token.id
					};
					request.post({
					  headers: {'content-type' : 'application/x-www-form-urlencoded'},
					  url:     'http://54.183.19.66:8080/ecitation/payViolation',
					  body:    "paymentDetails="+JSON.stringify(reqData)
					}, function(error, res, body){
						if( error ){
							var authToken = jwt.sign({ ecitnum: ccData.ecitnum, amount: ccData.amount}, 
								'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
							response.push({
								"digits":17,
								"action":"/dev/ccenter?ecitnum="+authToken,
								"message":error + " Enter your credit card number followed by #",
								"options":[]
							});
						}else{
							var payRes = JSON.parse(body);
							if( payRes && payRes.code === 200){
								response.push({ 
								"digits":5,
								"action":"/dev/paysuc",
								"message":payRes.token,
								"options":[]
								});
							}else{
								var authToken = jwt.sign({ ecitnum: ccData.ecitnum, amount: ccData.amount}, 
									'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
								response.push({
								"digits":17,
								"action":"/dev/ccenter?ecitnum="+authToken,
								"message":payRes.token + " Enter your credit card number followed by #",
								"options":[]
								});
							}
						}
						context.succeed(response);
					});
				}
			});
		}
	});
};
function parseQuery(qstr) {
	var query = {};
	var a = qstr.substr(0).split('&');
	for (var i = 0; i < a.length; i++) {
		var b = a[i].split('=');
		query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
	}
	return query;
}