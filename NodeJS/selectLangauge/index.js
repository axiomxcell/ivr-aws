console.log('Loading select langauge function');

exports.handler = function(event, context) {
	console.log('Loading holidays/business hours check');
	qryObject = parseQuery(event.reqbody);
	var enteredNum = qryObject['Digits'];
	
	var date = new Date();
	console.log('In select Language:: '+date);
	var checkHoliday = require('custom-metro-holidaylist')(date);
	console.log("checkHoliday:: "+checkHoliday);
	
	var businessHours = require('business-hours')(7, 17);
	var isInBusinessHours = businessHours.isBusinessHours();//return true in business hours
	console.log("isInBusinessHours:: "+isInBusinessHours);
	
	var validOptions = ["1", "2"];
	console.log(validOptions.indexOf(enteredNum));
	if(validOptions.indexOf(enteredNum) == -1) {
		var inputInvalid = event.invalidCount;
		inputInvalid = inputInvalid + "1"; 
		if(inputInvalid == "111") {
			choices = [{
					"digits":1,
					"lang": "en-US",
					"action":"/dev/customerservicescall?lang=en-US",
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
			}];
			context.succeed(choices[0]);
		}else {
			choices = [{
					"digits":1,
					"lang": "en-US",
					"action":"/dev/selectlanguage?invalidCount="+inputInvalid,
					"message": "We are sorry, your entry was invalid. Please try again. For english , press 1",
					"options":[],
					"repeatMenu":"Para hablar en espanol  marque el numero dos"
			}];
			context.succeed(choices[0]);
		}
	}
	
	//not business-hours and not holiday
	if(!isInBusinessHours && !checkHoliday) {
		choices = [
        {
            "digits":1,
            "lang":"en-US",
            "action":"/dev/violationmenu?lang=en-US",
            "message": "This is an automated system that can handle most inquiries regarding your violation.  If after using this system, your question is not answered, Transit Court representatives are available Monday through Friday, 8 A-M to 5 P-M except holidays. ",
            "submessage": "Did you know that you can also pay your violations online?  Visit us at Metro dot net, slash, Pay Violation.",
            "options":[
            "To check the amount due on a violation",
            "To find out how to pay for a violation",
            "For information on how to contest a violation, or schedule an administrative hearing"
            ],
			"repeatMenu" : "To repeat this menu, press *"
        },
        {
					"digits":1,
					"lang": "es-MX",
					"action":"/dev/customerservicescall?lang=en-US",
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
		}
      ];
	 context.succeed(choices[qryObject['Digits']-1]);
	}
	
	
	//not business-hours and holiday
	if(!isInBusinessHours && checkHoliday) {
		choices = [
        {
            "digits":1,
            "lang":"en-US",
            "action":"/dev/violationmenu?lang=en-US",
            "message": "This is an automated system that can handle most inquiries regarding your violation.  If after using this system, your question is not answered, Transit Court representatives are available Monday through Friday, 8 A-M to 5 P-M except holidays. ",
            "submessage": "Transit Court is closed.  You may use the automated system to obtain general information, or pay your violation by credit card. </Say><Pause length='2'></Pause><Say>Did you know that you can also pay your violations online?  Visit us at Metro dot net, slash, Pay Violation.",
            "options":[
            "To check the amount due on a violation",
            "To find out how to pay for a violation",
            "For information on how to contest a violation, or schedule an administrative hearing"
            ],
			"repeatMenu" : "To repeat this menu, press *"
        },
        {
					"digits":1,
					"lang": "es-MX",
					"action":"/dev/customerservicescall?lang=en-US",
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
		}
      ];
	context.succeed(choices[qryObject['Digits']-1]);
	}
	
	
	//business-hours and holiday
    if(isInBusinessHours && checkHoliday) {
	console.log('holiday or not business hours');
    choices = [
        {
            "digits":1,
            "lang":"en-US",
            "action":"/dev/violationmenu?lang=en-US",
            "message": "Transit Court is closed.",
            "submessage": "You may use the automated system to obtain general information, or pay your violation by credit card.",
            "options":[
            "To check the amount due on a violation",
            "To find out how to pay for a violation",
            "For information on how to contest a violation, or schedule an administrative hearing"
            ],
			"repeatMenu" : "To repeat this menu, press *"
        },
        {
					"digits":1,
					"lang": "es-MX",
					"action":"/dev/customerservicescall?lang=en-US",
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
		}
      ];
	context.succeed(choices[qryObject['Digits']-1]);
    }
	
	//business-hours && not holiday
	if(isInBusinessHours && !checkHoliday) {
        choices = [
        {
            "digits":1,
            "lang":"en-US",
            "action":"/dev/violationmenu?lang=en-US",
            "message": "",
            "submessage": "Did you know that you can also pay your violations online?  Visit us at Metro dot net, slash, Pay Violation.",
            "options":[
            "To check the amount due on a violation",
            "To find out how to pay for a violation",
            "For information on how to contest a violation, or schedule an administrative hearing"
            ],
			"repeatMenu" : "To repeat this menu, press *"
        },
        {
					"digits":1,
					"lang": "es-MX",
					"action":"/dev/customerservicescall?lang=en-US",
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
		}
      ];
	context.succeed(choices[qryObject['Digits']-1]);
    }
        
   
  
};

function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}