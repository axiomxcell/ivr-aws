console.log('Loading select langauge function');

exports.handler = function(event, context) {
	console.log('Loading holidays/business hours check');
	qryObject = parseQuery(event.reqbody);
	var enteredNum = qryObject['Digits'];
	var request = require('request');
	var etimesMaintenance = false;
	
	console.log("Before request:: etimesMaintenance:: "+etimesMaintenance);
	
	request("http://54.183.19.66:8080/ecitation/getServerStatus", function(error, res, body) {
		if(error){
			etimesMaintenance = true;
			console.log('In error'+error+' '+body+' '+res);			
		}else {
			console.log('not error'+error+' '+body+' '+res);
			var statusRes = JSON.parse(body);
			if( statusRes && statusRes.code === 500){
				etimesMaintenance = true;
				console.log('SERVER ERROR');	
			}
			if( statusRes && (statusRes.code === 200 && statusRes.token == "APPLICATION")){
				etimesMaintenance = true;
				console.log('ONLY APLLICATION SERVER WORKING');
			}
		}
	console.log("After request:: "+etimesMaintenance);
	
	var date = new Date();
	console.log('In select Language:: '+date);
	var checkHoliday = require('custom-metro-holidaylist')(date);
	console.log("checkHoliday:: "+checkHoliday);
	
	var businessHours = require('business-hours')(7, 16);
	var isInBusinessHours = businessHours.isBusinessHours();//return true in business hours
	console.log("isInBusinessHours:: "+isInBusinessHours);
	
	var validOptions = ["1", "2"];
	console.log(validOptions.indexOf(enteredNum));
	if(validOptions.indexOf(enteredNum) == -1) {
		var inputInvalid = event.invalidCount;
		inputInvalid = inputInvalid + "1"; 
		if(inputInvalid == "111") {
			choices = [{
					"digits":1,
					"lang": "en-US",
					"action":"/dev/customerservicescall?lang=en-US",
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
			}];
			context.succeed(choices[0]);
		}else {
			choices = [{
					"digits":1,
					"lang": "en-US",
					"action":"/dev/selectlanguage?invalidCount="+inputInvalid,
					"message": "We are sorry, your entry was invalid. Please try again. For english , press 1",
					"repeatSpanish": "Para hablar en espanol  marque el numero dos"		
			}];
			context.succeed(choices[0]);
		}
	}
	
	//not business-hours and not holiday
	if(!isInBusinessHours && !checkHoliday) {
		if(etimesMaintenance) {	
			choices = [
        	{
            	"digits":1,
            	"lang":"en-US",
            	"action":"/dev/violationmenu?lang=en-US",
            	"message": "This is an automated system that can handle most inquiries regarding your violation.  If after using this system, your question is not answered, Transit Court representatives are available Monday through Friday, 8 A-M to 4 P-M except holidays. ",
				"message1": "Our inquiry and payment services are temporarily unavailable. If you wish to pay a violation, please call back at a later time. For other information services, please stay on the line",
            	"submessage": "Did you know that you can also pay your violations online?  Visit us at Metro dot net, slash, Pay Violation.",
            	"options":[
            	"To check the amount due on a violation",
            	"To find out how to pay for a violation",
            	"For information on how to contest a violation, or schedule an administrative hearing"
            	],
				"repeatMenu" : "To repeat this menu, press *"
        	},
        	{
           		"digits":1,
           		"lang":"es-MX",
            	"action":"/dev/violationmenu?lang=es-MX",
            	"message": "Este es un sistema automatizado que puede solucionar la mayoría de las consultas relacionadas con su multa. Si después de usar este sistema, su pregunta no es resuelta, los representantes de la corte de transito de metro están disponibles de lunes a viernes, de 8 A-M a 4 P-M, excepto los días festivos.",
				"message1" : "Nuestros servicios de información y pago no están disponibles temporalmente. Si quiere pagar una multa, por favor llame de nuevo más tarde. Para otros servicios de información, por favor, permanezca en la línea",
            	"submessage": "¿Sabía que puede pagar su multa por internet? Visítenos en Metro punto net, barra, Pay Violation.",
            	"optionSpanish":[
            	"Para verificar el saldo de una multa ... presione uno",
            	"Información para pagar su multa ... presione dos",
            	"Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
            	],
				"repeatMenu" : "Para repetir este menú, presione estrella"
        	}
      		];
		} else {
			choices = [
        	{
            	"digits":1,
            	"lang":"en-US",
            	"action":"/dev/violationmenu?lang=en-US",
            	"message": "This is an automated system that can handle most inquiries regarding your violation.  If after using this system, your question is not answered, Transit Court representatives are available Monday through Friday, 8 A-M to 4 P-M except holidays. ",
            	"submessage": "Did you know that you can also pay your violations online?  Visit us at Metro dot net, slash, Pay Violation.",
            	"options":[
            	"To check the amount due on a violation",
            	"To find out how to pay for a violation",
            	"For information on how to contest a violation, or schedule an administrative hearing"
            	],
				"repeatMenu" : "To repeat this menu, press *"
        	},
        	{
           		"digits":1,
           		"lang":"es-MX",
            	"action":"/dev/violationmenu?lang=es-MX",
            	"message": "Este es un sistema automatizado que puede solucionar la mayoría de las consultas relacionadas con su multa. Si después de usar este sistema, su pregunta no es resuelta, los representantes de la corte de transito de metro están disponibles de lunes a viernes, de 8 A-M a 4 P-M, excepto los días festivos.",
            	"submessage": "¿Sabía que puede pagar su multa por internet? Visítenos en Metro punto net, barra, Pay Violation.",
            	"optionSpanish":[
            	"Para verificar el saldo de una multa ... presione uno",
            	"Información para pagar su multa ... presione dos",
            	"Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
            	],
				"repeatMenu" : "Para repetir este menú, presione estrella"
        	}
      		];
		}
	 	context.succeed(choices[qryObject['Digits']-1]);
	}
	
	
	//not business-hours and holiday
	if(!isInBusinessHours && checkHoliday) {
		if(etimesMaintenance) {
			choices = [
        	{
            	"digits":1,
            	"lang":"en-US",
            	"action":"/dev/violationmenu?lang=en-US",
            	"message": "This is an automated system that can handle most inquiries regarding your violation.  If after using this system, your question is not answered, Transit Court representatives are available Monday through Friday, 8 A-M to 4 P-M except holidays. ",
				"submessage": "Transit Court is closed.  You may use the automated system to obtain general information, or pay your violation by credit card. </Say><Pause length='2'></Pause><Say>    Our inquiry and payment services are temporarily unavailable. If you wish to pay a violation, please call back at a later time. For other information services, please stay on the line    </Say><Pause length='2'></Pause><Say>       Did you know that you can also pay your violations online?  Visit us at Metro dot net, slash, Pay Violation.",
				"options":[
            	"To check the amount due on a violation",
            	"To find out how to pay for a violation",
           	 	"For information on how to contest a violation, or schedule an administrative hearing"
            	],
				"repeatMenu" : "To repeat this menu, press *"
        	},
        	{
           		"digits":1,
           		"lang":"es-MX",
            	"action":"/dev/violationmenu?lang=es-MX",
            	"message": "Este es un sistema automatizado que puede solucionar la mayoría de las consultas relacionadas con su multa. Si después de usar este sistema, su pregunta no es resuelta, los representantes de la corte de transito de metro están disponibles de lunes a viernes, de 8 A-M a 4 P-M, excepto los días festivos.",
            	"submessage": "La corte de transito de metro está cerrado. Usted puede usar el sistema automatizado para obtener información general, o pagar su multa con tarjeta de crédito. </Say><Pause length='2'></Pause><Say language='es-MX'> Nuestros servicios de información y pago no están disponibles temporalmente. Si quiere pagar una multa, por favor llame de nuevo más tarde. Para otros servicios de información, por favor, permanezca en la línea </Say><Pause length='2'></Pause><Say language='es-MX'>¿Sabía que puede pagar su multa por internet? Visítenos en Metro punto net, barra, Pay Violation.",
            	"optionSpanish":[
            	"Para verificar el saldo de una multa ... presione uno",
            	"Información para pagar su multa ... presione dos",
            	"Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
            	],
				"repeatMenu" : "Para repetir este menú, presione estrella"
        	}
      		];
		}else {
			choices = [
        	{
            	"digits":1,
            	"lang":"en-US",
            	"action":"/dev/violationmenu?lang=en-US",
            	"message": "This is an automated system that can handle most inquiries regarding your violation.  If after using this system, your question is not answered, Transit Court representatives are available Monday through Friday, 8 A-M to 4 P-M except holidays. ",
            	"submessage": "Transit Court is closed.  You may use the automated system to obtain general information, or pay your violation by credit card. </Say><Pause length='2'></Pause><Say>Did you know that you can also pay your violations online?  Visit us at Metro dot net, slash, Pay Violation.",
            	"options":[
            	"To check the amount due on a violation",
            	"To find out how to pay for a violation",
           	 	"For information on how to contest a violation, or schedule an administrative hearing"
            	],
				"repeatMenu" : "To repeat this menu, press *"
        	},
        	{
           		"digits":1,
           		"lang":"es-MX",
            	"action":"/dev/violationmenu?lang=es-MX",
            	"message": "Este es un sistema automatizado que puede solucionar la mayoría de las consultas relacionadas con su multa. Si después de usar este sistema, su pregunta no es resuelta, los representantes de la corte de transito de metro están disponibles de lunes a viernes, de 8 A-M a 4 P-M, excepto los días festivos.",
            	"submessage": "La corte de transito de metro está cerrado. Usted puede usar el sistema automatizado para obtener información general, o pagar su multa con tarjeta de crédito. </Say><Pause length='2'></Pause><Say language='es-MX'> ¿Sabía que puede pagar su multa por internet? Visítenos en Metro punto net, barra, Pay Violation.",
            	"optionSpanish":[
            	"Para verificar el saldo de una multa ... presione uno",
            	"Información para pagar su multa ... presione dos",
            	"Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
            	],
				"repeatMenu" : "Para repetir este menú, presione estrella"
        	}
      		];
		}
	context.succeed(choices[qryObject['Digits']-1]);
	}
	
	
	//business-hours and holiday
    if(isInBusinessHours && checkHoliday) {
	console.log('holiday or not business hours');
	if(etimesMaintenance) {
		choices = [
        	{
            	"digits":1,
            	"lang":"en-US",
            	"action":"/dev/violationmenu?lang=en-US",
            	"message": "Transit Court is closed. You may use the automated system to obtain general information, or pay your violation by credit card.",
            	"submessage": "Our inquiry and payment services are temporarily unavailable. If you wish to pay a violation, please call back at a later time. For other information services, please stay on the line",
				"submessage1" : "Did you know that you can also pay your violations online?  Visit us at Metro dot net, slash, Pay Violation.",	
            	"options":[
            	"To check the amount due on a violation",
            	"To find out how to pay for a violation",
            	"For information on how to contest a violation, or schedule an administrative hearing"
            	],
				"repeatMenu" : "To repeat this menu, press *"
        	},
        	{
           		"digits":1,
           		"lang":"es-MX",
            	"action":"/dev/violationmenu?lang=es-MX",
            	"message": "La corte de transito de metro está cerrado. Usted puede usar el sistema automatizado para obtener información general, o pagar su multa con tarjeta de crédito.",
            	"submessage": "Nuestros servicios de información y pago no están disponibles temporalmente. Si quiere pagar una multa, por favor llame de nuevo más tarde. Para otros servicios de información, por favor, permanezca en la línea",
				"submessage1" : "¿Sabía que puede pagar su multa por internet? Visítenos en Metro punto net, barra, Pay Violation.",
            	"optionSpanish":[
            	"Para verificar el saldo de una multa ... presione uno",
            	"Información para pagar su multa ... presione dos",
            	"Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
            	],
				"repeatMenu" : "Para repetir este menú, presione estrella"
        	}
      		];
		}else {
			choices = [
        	{
            	"digits":1,
            	"lang":"en-US",
            	"action":"/dev/violationmenu?lang=en-US",
            	"message": "Transit Court is closed.",
            	"submessage": "You may use the automated system to obtain general information, or pay your violation by credit card.",
				"submessage1" : "Did you know that you can also pay your violations online?  Visit us at Metro dot net, slash, Pay Violation.",
            	"options":[
            	"To check the amount due on a violation",
            	"To find out how to pay for a violation",
            	"For information on how to contest a violation, or schedule an administrative hearing"
            	],
				"repeatMenu" : "To repeat this menu, press *"
        	},
        	{
           		"digits":1,
           		"lang":"es-MX",
            	"action":"/dev/violationmenu?lang=es-MX",
            	"message": "La corte de transito de metro está cerrado.",
            	"submessage": "Usted puede usar el sistema automatizado para obtener información general, o pagar su multa con tarjeta de crédito.",
				"submessage1" : "¿Sabía que puede pagar su multa por internet? Visítenos en Metro punto net, barra, Pay Violation.",
            	"optionSpanish":[
            	"Para verificar el saldo de una multa ... presione uno",
            	"Información para pagar su multa ... presione dos",
            	"Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
            	],
				"repeatMenu" : "Para repetir este menú, presione estrella"
        	}
      		];
		}	
		context.succeed(choices[qryObject['Digits']-1]);
    }
	
	
	
	//business-hours && not holiday
	if(isInBusinessHours && !checkHoliday) {
		if(etimesMaintenance) {
			choices = [
        	{
            	"digits":1,
            	"lang":"en-US",
            	"action":"/dev/violationmenu?lang=en-US",
            	"message": "Our inquiry and payment services are temporarily unavailable. If you wish to pay a violation, please call back at a later time. For other information services, please stay on the line",
            	"submessage": "Did you know that you can also pay your violations online?  Visit us at Metro dot net, slash, Pay Violation.",
            	"options":[
            	"To check the amount due on a violation",
            	"To find out how to pay for a violation",
            	"For information on how to contest a violation, or schedule an administrative hearing"
            	],
				"repeatMenu" : "To repeat this menu, press *"
        	},
        	{
           		"digits":1,
           		"lang":"es-MX",
            	"action":"/dev/violationmenu?lang=es-MX",
            	"message": "Nuestros servicios de información y pago no están disponibles temporalmente. Si quiere pagar una multa, por favor llame de nuevo más tarde. Para otros servicios de información, por favor, permanezca en la línea",
            	"submessage": "¿Sabía que puede pagar su multa por internet? Visítenos en Metro punto net, barra, Pay Violation.",
            	"optionSpanish":[
            	"Para verificar el saldo de una multa ... presione uno",
            	"Información para pagar su multa ... presione dos",
            	"Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
            	],
				"repeatMenu" : "Para repetir este menú, presione estrella"
        	}
      		];
		}else {
			choices = [
        	{
            	"digits":1,
            	"lang":"en-US",
            	"action":"/dev/violationmenu?lang=en-US",
            	"message": "",
            	"submessage": "Did you know that you can also pay your violations online?  Visit us at Metro dot net, slash, Pay Violation.",
            	"options":[
            	"To check the amount due on a violation",
            	"To find out how to pay for a violation",
            	"For information on how to contest a violation, or schedule an administrative hearing"
            	],
				"repeatMenu" : "To repeat this menu, press *"
        	},
        	{
           		"digits":1,
           		"lang":"es-MX",
            	"action":"/dev/violationmenu?lang=es-MX",
            	"message": "",
            	"submessage": "¿Sabía que puede pagar su multa por internet? Visítenos en Metro punto net, barra, Pay Violation.",
            	"optionSpanish":[
            	"Para verificar el saldo de una multa ... presione uno",
            	"Información para pagar su multa ... presione dos",
            	"Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
            	],
				"repeatMenu" : "Para repetir este menú, presione estrella"
        	}
      		];
		}
	context.succeed(choices[qryObject['Digits']-1]);
    }
	
});	
  
};

function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}