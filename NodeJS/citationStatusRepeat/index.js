console.log('Loading readViolationNumPrefix selection');

exports.handler = function (event, context) {

	qryObject = parseQuery(event.reqbody);
	var enteredNum = qryObject['Digits'];
	var ecitnumEncoded = event.ecitnum;
	var choices = [];
	var jwt = require('jsonwebtoken');
	
	jwt.verify(ecitnumEncoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671', function (err, decoded) {
		if (err) {
			//Need to send user to first screen
		} else {
			var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum, citationStatus: decoded.citationStatus },
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });

			var validOptions = ["*", "9"];
			var lang = decoded.selectedlang;
			
		if (lang == "en-US") {
				
			if(validOptions.indexOf(enteredNum) == -1) {
				var inputInvalid = "";
				if(decoded.invalidCount != undefined) {
					inputInvalid = decoded.invalidCount;
				}
				inputInvalid = inputInvalid + "1"; 
				
				var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum, citationStatus: decoded.citationStatus, invalidCount: inputInvalid },
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
				if(inputInvalid == "111") {
					choices = [{
						"digits":0,
						"lang": decoded.selectedlang,
						"action":"",
						"message": "",
						"options":[],
						"redirectAction":"YES",
						"timeOut":"0"
					}];
				}else {
					choices = [{
						"digits":1,
						"lang": decoded.selectedlang,
						"action":"/dev/citationstatusrepeat?ecitnum="+authTokenTryAgain,
						"message": "We are sorry, your entry was invalid. Please try again.",
						"repeatMenu": "To repeat this information,  press *",
						"mainMenu": "To select another menu option, press 9"
					}];
				}
			context.succeed(choices[0]);
			}
				
			

				switch (enteredNum) {
					case "*":

						if (decoded.citationStatus.status == "PREPROCESSED") {
							choices = [{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/citationstatusrepeat?ecitnum=" + authToken,
								"message": "We are unable to retrieve the status of your violation at this time. Please call back in 5 days",
								"repeatMenu": "To repeat this information,  press *",
								"mainMenu": "To select another menu option, press 9",
								"options": []
							}]
						} else if (decoded.citationStatus.status == "CLOSED" || decoded.citationStatus.status == "PAID") {
							choices = [{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/citationstatusrepeat?ecitnum=" + authToken,
								"message": "Your violation is closed.  There is no payment due and no further action is required.",
								"repeatMenu": "To repeat this information,  press *",
								"mainMenu": "To select another menu option, press 9",
								"options": []
							}]
						}
						context.succeed(choices[0]);

						break;
					case "9":
						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/violationmenu?lang=" + decoded.selectedlang,
							"message": "",
							"options": [
								"To check the amount due on a violation",
								"To find out how to pay for a violation",
								"For information on how to contest a violation, or schedule an administrative hearing"
							],
							"repeatMenu": "To repeat this menu, press *"
						}];
						context.succeed(choices[0]);
						break;
				}
			}
			
			
			
			
		if (lang == "es-MX") {
				
			if(validOptions.indexOf(enteredNum) == -1) {
				var inputInvalid = "";
				if(decoded.invalidCount != undefined) {
					inputInvalid = decoded.invalidCount;
				}
				inputInvalid = inputInvalid + "1"; 
				
				var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum, citationStatus: decoded.citationStatus, invalidCount: inputInvalid },
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
				if(inputInvalid == "111") {
					choices = [{
						"digits":0,
						"lang": decoded.selectedlang,
						"action":"",
						"message": "",
						"options":[],
						"redirectAction":"YES",
						"timeOut":"0"
					}];
				}else {
					choices = [{
						"digits":1,
						"lang": decoded.selectedlang,
						"action":"/dev/citationstatusrepeat?ecitnum="+authTokenTryAgain,
						"message": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
						"repeatMenu": "Para repetir esta información, presione Estrella",
						"mainMenu": "Para seleccionar otra opción de menú, presione nueve"
					}];
				}
				context.succeed(choices[0]);
			}
				
			
			switch (enteredNum) {
					case "*":

						if (decoded.citationStatus.status == "PREPROCESSED") {
							choices = [{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/citationstatusrepeat?ecitnum=" + authToken,
								"message": "No podemos recuperar el estado de su multa en este momento. Por favor llame en 5 días",
								"repeatMenu": "Para repetir esta información, presione Estrella",
								"mainMenu": "Para seleccionar otra opción de menú, presione nueve",
								"options": []
							}]
						} else if (decoded.citationStatus.status == "CLOSED" || decoded.citationStatus.status == "PAID") {
							choices = [{
								"digits": 1,
								"lang": decoded.selectedlang,
								"action": "/dev/citationstatusrepeat?ecitnum=" + authToken,
								"message": "Su multa está cerrada.  No hay pago pendiente y no es necesario realizar ninguna otra acción.",
								"repeatMenu": "Para repetir esta información, presione Estrella",
								"mainMenu": "Para seleccionar otra opción de menú, presione nueve",
								"options": []
							}]
						}
						context.succeed(choices[0]);

						break;
					case "9":
						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/violationmenu?lang=" + decoded.selectedlang,
							"message": "",
							"optionSpanish": [
								"Para verificar el saldo de una multa ... presione uno",
								"Información para pagar su multa ... presione dos",
								"Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
							],
							"repeatMenu": "Para repetir este menú, presione estrella"
						}];
						context.succeed(choices[0]);
						break;
				}
			}
		}
	});
};

function parseQuery(qstr) {
	var query = {};
	var a = qstr.substr(0).split('&');
	for (var i = 0; i < a.length; i++) {
		var b = a[i].split('=');
		query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
	}
	return query;
}