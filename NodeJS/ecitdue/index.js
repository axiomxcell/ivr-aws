exports.handler = function(event, context) {
	poptions = [
		"Pay through credit card",
		"Place a review request",
		"Go back to previous menu",
		"Go back to main menu"
	];
	npoptions = [
		"Go back to previous menu",
		"Go back to main menu"
	];
	qryObject = parseQuery(event.reqbody);
	var ecitnum = "P" + qryObject['Digits'];
	var request = require('request');
	var jwt = require('jsonwebtoken');
	request("http://:8080/ecitation/getCitationAmount/"+ecitnum, function(error, res, body) {
		if( !error ){
			var penalty = JSON.parse(body);
			var response = {};
			var lang = "en-US";
			if( penalty && penalty.code === 200){
				var authToken = jwt.sign({ ecitnum: ecitnum, amount: penalty.token, selectedlang: lang}, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				response = {
					"digits":1,
					"action":"/dev/penaltymenu?ecitnum="+authToken,
					"message":"Your penalty amount is " + penalty.token,
					"options":poptions,
					"lang": lang
				};
			}else if( penalty && penalty.code === 404){
				response = {
					"digits":10,
					"action":"/dev/citnument",
					"message": "Invalid citation number. Please enter valid citation number followed by #",
					"options":[],
					"lang": lang 
				};
			}else{
				response = {
					"digits":1,
					"action":"/dev/nopenalty",
					"message":"No penalty for given citation number",
					"options":npoptions,
					"lang": lang
				};
			}    
		}else{
			response = {
				"digits":10,
				"action":"/dev/citnument",
				"message": "Unable to get the citation number details. Please enter citation number followed by # ",
				"options":[],
				"lang": lang
			};
		}
		context.succeed(response);
	});
};
function parseQuery(qstr) {
	var query = {};
	var a = qstr.substr(0).split('&');
	for (var i = 0; i < a.length; i++) {
		var b = a[i].split('=');
		query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
	}
	return query;
}