console.log('Loading readViolationNumPrefix selection');

exports.handler = function(event, context) {
    
    qryObject = parseQuery(event.reqbody);
    var selOption = qryObject['Digits'];
    var lang = event.lang;
    var choices = [];
    
    switch(selOption) {
        case "*":	
        choices = [
        {
           "digits":1,
           "lang": lang,
            "action":"/dev/readviolationprefix?lang="+lang,
            "message": "Please have your violation ready.  You will find the violation number located on the upper right hand corner. You may repeat this menu at any time by pressing star.",
            "options":[
            "If your violation starts with a T",
            "If it starts with a P",
            ],
            "repeatMenu":"To repeat this menu, press *",
            "mainMenu": "To return to the main menu,  press 9"
        }];
        context.succeed(choices[0]);
        break;
        
        case "9":	
        choices = [
        {
            "digits":1,
            "lang": lang,
            "action":"/dev/violationmenu?lang="+lang,
            "message": "This is an automated system that can handle most inquiries regarding your violation.  If after using this system, your question is not answered, Transit Court representatives are available Monday through Friday, 8 A-M to 5 P-M except holidays.",
            "submessage": "Did you know that you can also pay your violations online?  Visit us at Metro dot net, slash, Pay Violation.",
            "options":[
            "To check the amount due on a violation",
            "To find out how to pay for a violation",
            "For information on how to contest a violation, or schedule an administrative hearing"
            ],
            "repeatMenu":"To repeat this menu, press *"
        }];
        context.succeed(choices[0]);
        break;
        
        
        default:
        choices = [
        {
            "digits":7,
            "lang": lang,
            "action":"/dev/",
            "message": "Please enter the 7 numbers listed after the letter T.",
            "options":[]
        },
        {
           "digits":7,
           "lang": lang,
            "action":"/dev/",
            "message": "Please enter the 7 numbers listed after the letter P.",
            "options":[] 
        },
        {
           "digits":1,
           "lang": lang,
            "action":"/dev/",
            "message": "You have selected star.",
            "options":[] 
        }
        ];
        context.succeed(choices[qryObject['Digits']-1]);
        break;
    }
    console.log(choices[0]);
};

function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}