console.log('Loading pay using mail');

exports.handler = function(event, context) {
    
    qryObject = parseQuery(event.reqbody);
    var enteredNum = qryObject['Digits'];
	var choices = [];
    var jwt = require('jsonwebtoken');
    var langObj = event.lang;
    var lang = "";
    var inputInvalid = "";
		
    if(langObj.indexOf("invalidCount") !== -1) {
        var langArr = langObj.split('00');
        lang = langArr[0];
        inputInvalidTemp = langArr[1];
        var inputInvalidArr = inputInvalidTemp.split('=');
        inputInvalid = inputInvalidArr[1];
    }else {
        lang = event.lang;
    }
	
	var authToken = jwt.sign({ selectedlang: lang, selectedOption: enteredNum }, 
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
	var validOptions = ["1", "2"];
	
if(lang == "en-US"){
	if(validOptions.indexOf(enteredNum) == -1) {
		inputInvalid = inputInvalid + "1"; 
		if(inputInvalid == "111") {
			choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/customerservicescall?lang="+lang,
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
			}];
		}else {
			choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/payusingmail?lang="+lang+"00invalidCount="+inputInvalid,
					"message": "We are sorry, your entry was invalid. Please try again",
					"options": [
						"To pay your transit violation",
						"To pay your parking violation"
					]
			}];
		}
		context.succeed(choices[0]);
	}
	
	choices = [{
		"digits":1,
		"lang": lang,
		"action":"/dev/paybymailrepeat?ecitnum="+authToken,
		"message": "Checks should be made payable to Metro Transit Court.  Mail to: P-O Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. Be sure to include your violation number on the check or money order. ",
		"repeatMenu": "To repeat this message,  press *",
		"mainMenu": "To return to the main menu,  press 9",
		"options":[]
	},
	{
		"digits":1,
		"lang": lang,
		"action":"/dev/paybymailrepeat?ecitnum="+authToken,
		"message": "Checks should be made payable to Metro Transit Court.  Mail to: PO Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. Be sure to include your violation and license plate number on the check or money order.  ",
		"repeatMenu": "To repeat this message,  press *",
		"mainMenu": "To return to the main menu,  press 9",
		"options":[]
	}]
	context.succeed(choices[qryObject['Digits']-1]);
	}
	if(lang == "es-MX"){
		if(validOptions.indexOf(enteredNum) == -1) {
			inputInvalid = inputInvalid + "1"; 
			if(inputInvalid == "111") {
				choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/customerservicescall?lang="+lang,
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
				}];
			}else {
				choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/payusingmail?lang="+lang,
					"message": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
					"optionSpanish": [
						"Para realizar el pago de su multa de tansito…presione uno",
						"Para realizar el pago de su multa de estacionamiento…presione dos"
					]
				}];
			}
		context.succeed(choices[0]);
	}
	
	choices = [{
		"digits":1,
		"lang": lang,
		"action":"/dev/paybymailrepeat?ecitnum="+authToken,
		"message": "Los cheques deben hacerse a nombre de pagadero Metro Transit Court. Mail a: P-O Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. Asegúrese de incluir su número de multa en el cheque o giro postal.",
		"repeatMenu": "Para repetir esta información, presione Estrella",
		"mainMenu": "Para seleccionar otra opción de menú, presione nueve",
		"options":[]
	},
	{
		"digits":1,
		"lang": lang,
		"action":"/dev/paybymailrepeat?ecitnum="+authToken,
		"message": "Los cheques deben hacerse a nombre de pagadero Metro Transit Court. Mail a: P-O Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. Asegúrese de incluir su número de multa en el cheque o giro postal.",
		"repeatMenu": "Para repetir esta información, presione Estrella",
		"mainMenu": "Para seleccionar otra opción de menú, presione nueve",
		"options":[]
	}]
	context.succeed(choices[qryObject['Digits']-1]);
	

	}
};

function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}