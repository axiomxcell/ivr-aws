console.log('Loading contest tic selection repeat check');

exports.handler = function(event, context) {
    
    qryObject = parseQuery(event.reqbody);
    var enteredNum = qryObject['Digits'];
    var ecitnumEncoded = event.ecitnum;
    var choices = [];
	var jwt = require('jsonwebtoken');
	
	
	jwt.verify(ecitnumEncoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671', function(err, decoded) {    
		if (err) {
			//Need to send user to first screen
		}else{
    
    
	var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption }, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
	
	var validOptions = ["1", "*", "9"];
	var lang =decoded.selectedlang;

	if(lang == "en-US"){
		if(validOptions.indexOf(enteredNum) == -1) {
			var inputInvalid = "";
			if(decoded.invalidCount != undefined) {
				inputInvalid = decoded.invalidCount;
			}
			inputInvalid = inputInvalid + "1"; 
				
			var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, invalidCount: inputInvalid },
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
			if(inputInvalid == "111") {
				choices = [{
						"digits":0,
						"lang": decoded.selectedlang,
						"action":"",
						"message": "",
						"options":[],
						"redirectAction":"YES",
						"timeOut":"0"
					}];
			}else {
				choices = [{
					"digits":1,
					"lang": decoded.selectedlang,
					"action":"/dev/contestticselectionrepeat?ecitnum="+authTokenTryAgain,
					"message": "We are sorry, your entry was invalid. Please try again",
					"repeatMenu": "To repeat this message, press *",
            		"mainMenu": "To return to the main menu, press 9",
            		"options":[
                		"To repeat the address,"
                	]
				}];
			}
		context.succeed(choices[0]);
	}
	
	switch(enteredNum) {
	case "1":
		
	if(decoded.selectedOption == "1") {
	choices = [
        {
            "digits":1,
            "lang":decoded.selectedlang,
            "action":"/dev/contestticselectionrepeat?ecitnum="+authToken,
			"message":"Please send all correspondence to: 	With full payment of all fines and penalties to: 	Metro Transit Court, P-O Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. Be sure to include your violation number on the check or money order.",
			"submessage2":"",
            "repeatMenu": "To repeat this message, press *",
            "mainMenu": "To return to the main menu, press 9",
            "options":[
                "To repeat the address,"
                ]
        }
      ]
    context.succeed(choices[0]);
	}
	
	if(decoded.selectedOption == "2") {
			choices = [{
            "digits":1,
            "lang":decoded.selectedlang,
            "action":"/dev/contestticselectionrepeat?ecitnum="+authToken,
			"message":"Please send all correspondence to: 	With full payment of all fines and penalties to: Metro Transit Court, PO Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. Be sure to include your violation and license plate number on the check or money order.",
			"submessage2":"",
            "repeatMenu": "To repeat this message, press *",
            "mainMenu": "To return to the main menu, press 9",
            "options":[
                "To repeat the address,"
                ]
        }]
    context.succeed(choices[0]);		
	}

	break;
	
	case "*":
	
	if(decoded.selectedOption == "1") {
		
		var output = {
            "digits":1,
            "lang":decoded.selectedlang,
            "action":"/dev/contestticselectionrepeat?ecitnum="+authToken,
			"message": "The request for an initial review must be received within twenty-one calendar days of the violation being issued.  If you do not request an initial review within this time frame, you lose your right to contest.  You may request the initial review in writing, in person, or by phone.",
			"submessage1":"Please send all correspondence to: 	With full payment of all fines and penalties to: 	Metro Transit Court, P-O Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. Be sure to include your violation number on the check or money order.",
			"submessage2":"Please state the reason for your initial review request in your letter and submit any supporting documents.  Metro Transit Court will determine whether or not the violation was properly issued.  The results of the review will be mailed to you.",
            "repeatMenu": "To repeat this message, press *",
            "mainMenu": "To return to the main menu, press 9",
            "options":[
                "To repeat the address,"
                ]
        };
		context.succeed(output);
	}
	
	if(decoded.selectedOption == "2") {
		
		var output = {
            "digits":1,
            "lang":decoded.selectedlang,
            "action":"/dev/contestticselectionrepeat?ecitnum="+authToken,
			"message": "The request for an initial review must be received within twenty-one days of the violation being issued, or within fourteen calendar days after the mailing of ‘the notice of delinquent parking violation’.  If you do not request an initial review within this time frame, you lose your right to contest.  You may request the initial review in writing, in person, or by phone.",
			"submessage1":"Please send all correspondence to: 	With full payment of all fines and penalties to:   Metro Transit Court, PO Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. Be sure to include your violation and license plate number on the check or money order.",
			"submessage2":"Please state the reason for your initial review request in your letter and submit any supporting documents.  Metro Transit Court will determine whether or not the violation was properly issued.  The results of the review will be mailed to you.",
            "repeatMenu": "To repeat this message, press *",
            "mainMenu": "To return to the main menu, press 9",
            "options":[
                "To repeat the address,"
                ]
        };
		context.succeed(output);
	}		    
			
	break;
			
			
	case "9":
            choices = [{
                    "digits":1,
                    "lang": decoded.selectedlang,
                    "action":"/dev/violationmenu?lang="+decoded.selectedlang,
                    "message": "",
                    "options":[
                        "To check the amount due on a violation",
                        "To find out how to pay for a violation",
                        "For information on how to contest a violation, or schedule an administrative hearing"
                    ],
                    "repeatMenu":"To repeat this menu, press *"
            }];
            context.succeed(choices[0]);
            break;
		 }
	 }
	
	
	 
	 
if(lang == "es-MX"){
		if(validOptions.indexOf(enteredNum) == -1) {
			var inputInvalid = "";
			if(decoded.invalidCount != undefined) {
				inputInvalid = decoded.invalidCount;
			}
			inputInvalid = inputInvalid + "1"; 	
			var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, invalidCount: inputInvalid },
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
			if(inputInvalid == "111") {
				choices = [{
						"digits":0,
						"lang": decoded.selectedlang,
						"action":"",
						"message": "",
						"options":[],
						"redirectAction":"YES",
						"timeOut":"0"
				}];
			}else {
				choices = [{
					"digits":1,
					"lang": decoded.selectedlang,
					"action":"/dev/contestticselectionrepeat?ecitnum="+authTokenTryAgain,
					"message": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
					"repeatMenu": "Para repetir este mensaje, presione estrella",
            		"mainMenu": "Para regresar al menú principal, presione nueve",
            		"optionSpanish":[
                		"Para repetir la dirección, pulse uno"
                	]
				}];
			}
		context.succeed(choices[0]);
	}
	
	switch(enteredNum) {
	case "1":
		
	if(decoded.selectedOption == "1") {
	choices = [
        {
            "digits":1,
            "lang":decoded.selectedlang,
            "action":"/dev/contestticselectionrepeat?ecitnum="+authToken,
			"message":"Por favor envíe toda la correspondencia a: 	With full payment of all fines and penalties to:…Metro Transit Court, P-O Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6.Be sure to include your violation number on the check or money order.",
			"submessage2":"",
            "repeatMenu": "Para repetir este mensaje, presione estrella",
            "mainMenu": "Para regresar al menú principal, presione nueve",
            "optionSpanish":[
                "Para repetir la dirección, pulse uno"
                ]
        }
      ]
    context.succeed(choices[0]);
	}
	
	if(decoded.selectedOption == "2") {
			choices = [{
            "digits":1,
            "lang":decoded.selectedlang,
            "action":"/dev/contestticselectionrepeat?ecitnum="+authToken,
			"message":"Por favor envíe toda la correspondencia a:  	With full payment of all fines and penalties to:…Metro Transit Court, PO Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6.Be sure to include your violation and license plate number on the check or money order.",
			"submessage2":"",
            "repeatMenu": "Para repetir este mensaje, presione estrella",
            "mainMenu": "Para regresar al menú principal, presione nueve",
            "optionSpanish":[
                "Para repetir la dirección, pulse uno"
                ]
        }]
    context.succeed(choices[0]);		
	}

	break;
	
	case "*":
	
	if(decoded.selectedOption == "1") {
		
		var output = {
            "digits":1,
            "lang":decoded.selectedlang,
            "action":"/dev/contestticselectionrepeat?ecitnum="+authToken,
			"message": "La solicitud de una revisión inicial debe ser recibida dentro de los veintiún días calendario de la multa en que fue emitida. Si no solicita una revisión inicial dentro de este marco de tiempo, pierde su derecho a participar. Usted puede solicitar la revisión inicial por escrito, en persona o por teléfono.",
			"submessage1":"Por favor envíe toda la correspondencia a: 	Con el pago completo de todas las multas y multas a:Metro Transit Court, P-O Box 8 6 6 0 1 5, Los Ángeles, California, 9 0 0 8 6.Asegúrese de incluir su número de multa en el cheque o giro postal.",
			"submessage2":"Por favor, en la carta, indique el motivo por el que solicita la revisión inicial y envíe toda la documentación pertinente.  La corte de transito de Metro determinará si fue multado correctamente o no.   Los resultados de la revisión se le enviarán por correo.",
            "repeatMenu": "Para repetir este mensaje, presione estrella",
            "mainMenu": "Para regresar al menú principal, presione nueve",
            "optionSpanish":[
                "Para repetir la dirección, pulse uno"
                ]
        };
		context.succeed(output);
	}
	
	if(decoded.selectedOption == "2") {
		
		var output = {
            "digits":1,
            "lang":decoded.selectedlang,
            "action":"/dev/contestticselectionrepeat?ecitnum="+authToken,
			"message": "La solicitud de una revisión inicial debe ser recibida dentro de los veintiún días de la multa en que fue emitida, o dentro de catorce días calendario después del envío de 'la notificación de violación de estacionamiento delincuente'. Si no solicita una revisión inicial dentro de este marco de tiempo, pierde su derecho a disputar. Usted puede solicitar la revisión inicial por escrito, en persona o por teléfono.",
			"submessage1":"Por favor envíe toda la correspondencia a: 	With full payment of all fines and penalties to:…Metro Transit Court, PO Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6.Be sure to include your violation and license plate number on the check or money order.",
			"submessage2":"Por favor, en la carta, indique el motivo por el que solicita la revisión inicial y envíe toda la documentación pertinente.  La corte de transito de Metro determinará si fue multado correctamente o no.   Los resultados de la revisión se le enviarán por correo.",
            "repeatMenu": "Para repetir este mensaje, presione estrella",
            "mainMenu": "Para regresar al menú principal, presione nueve",
            "optionSpanish":[
                "Para repetir la dirección, pulse uno"
                ]
        };
		context.succeed(output);
	}		    
			
	break;
			
			
	case "9":
            choices = [{
                    "digits":1,
                    "lang": decoded.selectedlang,
                    "action":"/dev/violationmenu?lang="+decoded.selectedlang,
                    "message": "",
                    "optionSpanish":[
                        "Para verificar el saldo de una multa ... presione uno",
                        "Información para pagar su multa ... presione dos",
                        "Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
                    ],
                    "repeatMenu":"Para repetir este menú, presione estrella"
            }];
            context.succeed(choices[0]);
            break;
		 }
	 }
		}
		
	});
};

function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}