console.log('Loading pay using mail');

exports.handler = function (event, context) {

    qryObject = parseQuery(event.reqbody);
    var enteredNum = qryObject['Digits'];
    var choices = [];
    var jwt = require('jsonwebtoken');
    var ecitnumEncoded = event.ecitnum;


    jwt.verify(ecitnumEncoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671', function (err, decoded) {

        if (err) {

            //Need to send user to first screen

        } else {

            var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption },
                'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });

            var validOptions = ["*", "9"];
            var lang = decoded.selectedlang;
            if (lang == "en-US") {
				
                if (validOptions.indexOf(enteredNum) == -1) {
					var inputInvalid = "";
					if(decoded.invalidCount != undefined) {
						inputInvalid = decoded.invalidCount;
					}
					inputInvalid = inputInvalid + "1"; 
				
					var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, invalidCount: inputInvalid },'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
					if(inputInvalid == "111") {
						choices = [{
							"digits":0,
							"lang": decoded.selectedlang,
							"action":"",
							"message": "",
							"options":[],
							"redirectAction":"YES",
							"timeOut":"0"
						}];
					}else {
						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/paybymailrepeat?ecitnum=" + authTokenTryAgain,
							"message": "We are sorry, your entry was invalid. Please try again",
							"options": [],
							"repeatMenu": "To repeat this message,  press *",
                            "mainMenu": "To return to the main menu,  press 9"
						}];
					}
                    context.succeed(choices[0]);
                }


                switch (enteredNum) {
                    case "*":

                        if (decoded.selectedOption == "1") {
                            choices = [{
                                "digits": 1,
                                "lang": decoded.selectedlang,
                                "action": "/dev/paybymailrepeat?ecitnum=" + authToken,
                                "message": "Checks should be made payable to Metro Transit Court.  Mail to: P-O Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. Be sure to include your violation number on the check or money order. ",
                                "options": [],
                                "repeatMenu": "To repeat this message,  press *",
                                "mainMenu": "To return to the main menu,  press 9"
                            }];
                        }

                        if (decoded.selectedOption == "2") {
                            choices = [{
                                "digits": 1,
                                "lang": decoded.selectedlang,
                                "action": "/dev/paybymailrepeat?ecitnum=" + authToken,
                                "message": "Checks should be made payable to Metro Transit Court.  Mail to: PO Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. Be sure to include your violation and license plate number on the check or money order. ",
                                "options": [],
                                "repeatMenu": "To repeat this message,  press *",
                                "mainMenu": "To return to the main menu,  press 9"
                            }];
                        }

                        context.succeed(choices[0]);
                        break;
                    case "9":
                        choices = [{
                            "digits": 1,
                            "lang": decoded.selectedlang,
                            "action": "/dev/violationmenu?lang=" + decoded.selectedlang,
                            "message": "",
                            "options": [
                                "To check the amount due on a violation",
                                "To find out how to pay for a violation",
                                "For information on how to contest a violation, or schedule an administrative hearing"
                            ],
                            "repeatMenu": "To repeat this menu, press *"
                        }];
                        context.succeed(choices[0]);
                        break;
                }
            }
			
			
    if (lang == "es-MX") {

                if (validOptions.indexOf(enteredNum) == -1) {
					var inputInvalid = "";
					if(decoded.invalidCount != undefined) {
						inputInvalid = decoded.invalidCount;
					}
					inputInvalid = inputInvalid + "1"; 
				
					var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, invalidCount: inputInvalid },'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
					if(inputInvalid == "111") {
						choices = [{
							"digits":0,
							"lang": decoded.selectedlang,
							"action":"",
							"message": "",
							"options":[],
							"redirectAction":"YES",
							"timeOut":"0"
						}];
					}else {
						choices = [{
							"digits": 1,
							"lang": decoded.selectedlang,
							"action": "/dev/paybymailrepeat?ecitnum=" + authToken,
							"message": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
							"options": [],
							"repeatMenu": "Para repetir estas opciones, presione estrella",
                            "mainMenu": "Para regresar al menú principal, presione nueve"
						}];
					}
                    context.succeed(choices[0]);
                }


                switch (enteredNum) {
                    case "*":

                        if (decoded.selectedOption == "1") {
                            choices = [{
                                "digits": 1,
                                "lang": decoded.selectedlang,
                                "action": "/dev/paybymailrepeat?ecitnum=" + authToken,
                                "message": "Los cheques deben hacerse a nombre de pagadero Metro Transit Court. Mail a: P-O Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. Asegúrese de incluir su número de multa en el cheque o giro postal. ",
                                "options": [],
                                "repeatMenu": "Para repetir estas opciones, presione estrella",
                                "mainMenu": "Para regresar al menú principal, presione nueve"
                            }];
                        }

                        if (decoded.selectedOption == "2") {
                            choices = [{
                                "digits": 1,
                                "lang": decoded.selectedlang,
                                "action": "/dev/paybymailrepeat?ecitnum=" + authToken,
                                "message": "Los cheques deben hacerse a nombre de pagadero Metro Transit Court. Mail a: P-O Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. Asegúrese de incluir su número de multa en el cheque o giro postal. ",
                                "options": [],
                                "repeatMenu": "Para repetir estas opciones, presione estrella",
                                "mainMenu": "Para regresar al menú principal, presione nueve"
                            }];
                        }

                        context.succeed(choices[0]);
                        break;
                    case "9":
                        choices = [{
                            "digits": 1,
                            "lang": decoded.selectedlang,
                            "action": "/dev/violationmenu?lang=" + decoded.selectedlang,
                            "message": "",
                            "optionSpanish": [
                                "Para verificar el saldo de una multa ... presione uno",
                                "Información para pagar su multa ... presione dos",
                                "Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
                            ],
                            "repeatMenu": "Para repetir este menú, presione estrella"
                        }];
                        context.succeed(choices[0]);
                        break;
                }

            }
        }
    });
};

function parseQuery(qstr) {
    var query = {};
    var a = qstr.substr(0).split('&');
    for (var i = 0; i < a.length; i++) {
        var b = a[i].split('=');
        query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
    }
    return query;
}