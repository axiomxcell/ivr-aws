exports.handler = function(event, context) {
    var CCENTER = 0;
	var REPEAT = 1;
	
	qryObject = parseQuery(event.reqbody);
	var ccnum = qryObject['Digits'];
	var token = event.ecitnum;
	var jwt = require('jsonwebtoken');
	jwt.verify(token, 'e3b65b3a-2b52-11e7-93ae-92361f002671', function(err, decoded) {      
		if (err) {
			//Need to send user to first screen
		}else{
			var lang = decoded.selectedlang;
			if(lang == "en-US"){
			var resObj = {};
			if( !decoded.cca || decoded.cca === CCENTER ){
				//user entered the credit card number... Now send him to repeat menu
				decoded.cc = ccnum;
				decoded.cca = REPEAT;
				var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
				resObj = {
					"digits":1,
					"timeout":20,
					"action":"/dev/ccenter?ecitnum="+authToken,
					"message":"You entered... " + ccnumProunce(ccnum),
					"options":[
						"If this is correct, press 1",
						"To reenter the number, press 2"
					],
					"lang": lang
				}
			}else if(decoded.cca === REPEAT){
				ccnum = parseInt(ccnum);
				if( ccnum === 1 ){
					delete decoded.cca;
					//user says it is correct... continue... do some validation
					var validationError = false;
					if( decoded.cc.startsWith("4") && !(decoded.cc.length == 16 || decoded.cc.length == 13) ){
						validationError = true;
					}else if( decoded.cc.startsWith("5") && decoded.cc.length != 16){
						validationError = true;
					}
					//Do mod 10 check
					if( !validationError ){
						var luhn = require('cc-luhn');
						validationError	= !luhn(true, decoded.cc);
					}
					if( validationError ){
						delete decoded.ccnum;
						if( !decoded.cct ){
							decoded.cct = 0;
						}
						decoded.cct = decoded.cct + 1;
						if( decoded.cct > 2 ){
							//Reached max tries
							//var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
							resObj = {
								"digits":1,
								"timeout":0,
								"action":"/dev/customerservicescall?lang="+lang,
								"message":"I'm sorry but we don't show that as a valid card number.",
								"options":[],
								"lang": lang,
								"redirectAction":'<Redirect method="GET">/dev/customerservicescallget?lang='+lang+'&amp;Digits=1</Redirect>'
							};
							//Transfer call to CSR
						}else{
							decoded.cca = CCENTER;
							//Invalid card number
							var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
							resObj = {
								"digits":17,
								"timeout":45,
								"action":"/dev/ccenter?ecitnum="+authToken,
								"message":"I'm sorry but we don't show the cardnumber entered " + ccnumProunce(decoded.cc),
								"options":[
									"as a valid credit or debit card number. Please check your card and reenter the number.",
									"Please enter your credit card number now, followed by the pound sign."
								],
								"lang": lang
							}
						}
					}else{
						var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
						resObj = {
							"digits":5,
							"timeout":20,
							"action":"/dev/expenter?ecitnum="+authToken,
							"message":"Please enter the four-digit expiration date on your credit card using two digits for the month and two digits for the year, followed by the pound sign." ,
							"options":[
								"For example, if the expiration date is May 2019, you will enter 0-5, 1-9." ,
								"If you have a six digit date, enter only the month and year."
							],
							"lang": lang
						}
					}
				} else if( ccnum === 2 ){
					delete decoded.cca;
					delete decoded.ccnum;
					//Chooses to reenter the credit card number
					var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
					resObj = {
						"digits":17,
						"action":"/dev/ccenter?ecitnum="+authToken,
						"message":"Please enter your credit card number now, followed by the pound sign.",
						"options":[],
						"lang": lang
					}
				}else{
					//Invalid option selected.. so repeat the step one
					decoded.cca = REPEAT;
					var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
					resObj = {
						"digits":1,
						"action":"/dev/ccenter?ecitnum="+authToken,
						"message":"You entered... " + ccnumProunce(decoded.cc),
						"options":[
							"If this is correct, press 1",
							"To reenter the number, press 2"
						],
						"lang": lang
					}
				}
			}
			context.succeed([resObj]);
	}
	
			
			
			
			
			
	if(lang == "es-MX"){
				var resObj = {};
			if( !decoded.cca || decoded.cca === CCENTER ){
				//user entered the credit card number... Now send him to repeat menu
				decoded.cc = ccnum;
				decoded.cca = REPEAT;
				var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
				resObj = {
					"digits":1,
					"timeout":20,
					"action":"/dev/ccenter?ecitnum="+authToken,
					"message":"oprimio... " + ccnumProunceSpanish(ccnum),
					"options":[
						"Si esto es correcto, presione uno",
						"Para volver a ingresar el número, presione dos"
					],
					"lang": lang
				}
			}else if(decoded.cca === REPEAT){
				ccnum = parseInt(ccnum);
				if( ccnum === 1 ){
					delete decoded.cca;
					//user says it is correct... continue... do some validation
					var validationError = false;
					if( decoded.cc.startsWith("4") && !(decoded.cc.length == 16 || decoded.cc.length == 13) ){
						validationError = true;
					}else if( decoded.cc.startsWith("5") && decoded.cc.length != 16){
						validationError = true;
					}
					//Do mod 10 check
					if( !validationError ){
						var luhn = require('cc-luhn');
						validationError	= !luhn(true, decoded.cc);
					}
					if( validationError ){
						delete decoded.ccnum;
						if( !decoded.cct ){
							decoded.cct = 0;
						}
						decoded.cct = decoded.cct + 1;
						if( decoded.cct > 2 ){
							//Reached max tries
							//var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
							resObj = {
								"digits":1,
								"timeout":0,
								"action":"/dev/customerservicescall?lang="+lang,
								"message":"Lo siento, pero no lo mostramos como un número de tarjeta válido.",
								"options":[],
								"lang": lang,
								"redirectAction":'<Redirect method="GET">/dev/customerservicescallget?lang='+lang+'&amp;Digits=1</Redirect>'
							};
							//Transfer call to CSR
						}else{
							decoded.cca = CCENTER;
							//Invalid card number
							var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
							resObj = {
								"digits":17,
								"timeout":45,
								"action":"/dev/ccenter?ecitnum="+authToken,
								"message":"Lo siento pero no mostramos el número de tarjeta ingresado " + ccnumProunceSpanish(decoded.cc),
								"options":[
									"Como un número válido de tarjeta de crédito o débito. Compruebe su tarjeta y vuelva a ingresar el número.",
									"Introduzca su número de tarjeta de crédito ahora, seguido por el signo de libra."
								],
								"lang": lang
							}
						}
					}else{
						var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
						resObj = {
							"digits":5,
							"timeout":20,
							"action":"/dev/expenter?ecitnum="+authToken,
							"message":"Ingrese la fecha de vencimiento de cuatro dígitos en su tarjeta de crédito usando dos dígitos para el mes y dos dígitos para el año, seguido por el signo de libra." ,
							"options":[
								"Por ejemplo, si la fecha de caducidad es mayo de 2019, ingresará 0-5, 1-9." ,
								"Si tiene una fecha de seis dígitos, introduzca sólo el mes y el año."
							],
							"lang": lang
						}
					}
				} else if( ccnum === 2 ){
					delete decoded.cca;
					delete decoded.ccnum;
					//Chooses to reenter the credit card number
					var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
					resObj = {
						"digits":17,
						"action":"/dev/ccenter?ecitnum="+authToken,
						"message":"Introduzca su número de tarjeta de crédito ahora, seguido por el signo de libra.",
						"options":[],
						"lang": lang
					}
				}else{
					//Invalid option selected.. so repeat the step one
					decoded.cca = REPEAT;
					var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
					resObj = {
						"digits":1,
						"action":"/dev/ccenter?ecitnum="+authToken,
						"message":"oprimio... " + ccnumProunceSpanish(decoded.cc),
						"options":[
							"Si esto es correcto, presione uno",
							"Para volver a ingresar el número, presione dos"
						],
						"lang": lang
					}
				}
			}
			context.succeed([resObj]);
			

			}
		}
	});
};
function parseQuery(qstr) {
	var query = {};
	var a = qstr.substr(0).split('&');
	for (var i = 0; i < a.length; i++) {
		var b = a[i].split('=');
		query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
	}
	return query;
}

function ccnumProunce(cc) {
	var ccn = "";
	for (var i = 0; i < cc.length; i++) {
		ccn = ccn + cc.charAt(i) + ",";
	}
	return ccn;
}


function ccnumProunceSpanish(cc) {
	var ccn = "";
	for (var i = 0; i < cc.length; i++) {
		ccn = ccn + cc.charAt(i) + "</Say><Say language='es-MX'><Pause length='1'></Pause>";
	}
	return ccn;
}