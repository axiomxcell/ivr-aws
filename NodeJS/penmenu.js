console.log('Loading function');

exports.handler = function(event, context) {
   qryObject = parseQuery(event.reqbody);

   var selOption = qryObject['Digits'];
   var ecitnum = event.ecitnum;
   var response = {};
   switch(selOption){
     case "1":
         response = [{
            "digits":17,
            "action":"/dev/ccenter?ecitnum="+ecitnum,
            "message":"Enter your credit card number followed by #",
            "options":[]
         }];
         break;
     case "2":
         response = [{
            "digits":1,
            "action":"/dev/menusel",
            "message":"Your review request placed successfully. Thanks for using this service.",
            "options":[
                "Go back to previous menu",
                "Go back to main menu"]
        }];
         break;
     case "3":
         response = [{
            "digits":10,
            "action":"/dev/citnument",
            "message":"Please enter your citation number followed by #",
            "options":[]
        }];
         break;
     case "4":
        response = [{
            "digits":1,
            "action":"/dev/action1",
            "message":"Welcome Metro Transit court",
            "options":[
                "Language selection",
                "Pay a citation"
                ]
        }];
         break;
   }
   
   context.succeed(response);
};

function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}