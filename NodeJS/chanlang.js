console.log('Loading num selection');

exports.handler = function(event, context) {
    choices = [
        {
            "digits":1,
            "action":"",
            "message": "We are not supporting this language for now. Thanks for using the service.",
            "options":[]
        },
        {
           "digits":10,
            "action":"/dev/citnument",
            "message": "Please enter your citation number followed by #",
            "options":[] 
        }
        ]
        
   qryObject = parseQuery(event.reqbody);
   
   context.succeed(choices[qryObject['Digits']-1]);
};

function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}