exports.handler = (event, context) => {
    qryObject = parseQuery(event.reqbody);
    var enteredNum = qryObject['Digits'];
    var lang = event.lang;
	
	var langObj = event.lang;
    var lang = "";
    var inputInvalid = "";
		
    if(langObj.indexOf("invalidCount") !== -1) {
        var langArr = langObj.split('00');
        lang = langArr[0];
        inputInvalidTemp = langArr[1];
        var inputInvalidArr = inputInvalidTemp.split('=');
        inputInvalid = inputInvalidArr[1];
    }else {
        lang = event.lang;
    }
    var validOptions = ["*", "9"];
	
	
 if(lang == "en-US"){
	 
	if(validOptions.indexOf(enteredNum) == -1) {
        inputInvalid = inputInvalid + "1"; 
		if(inputInvalid == "111") {
			choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/customerservicescall?lang="+lang,
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
			}];
			context.succeed(choices[0]);
		}else {
			 choices = [{
                "digits":1,
                "lang": lang,
                "action":"/dev/paybyperson?lang="+lang+"00invalidCount="+inputInvalid,
                "message": "We are sorry, your entry was invalid. Please try again",
                "options":[],
               	"repeatMenu":"To repeat this information,  press *",
				"mainMenu": "To select another menu option, press 9"
                }];
			context.succeed(choices[0]);
		}
    }
	
	
	
    switch(enteredNum) {
			case "*":
			choices = [{
                    "digits":1,
                    "lang": lang,
                    "action":"/dev/paybyperson?lang="+lang,
                    "message": "To pay in person please go to Metro Transit Court located on the plaza level at One Gateway Plaza in Los Angeles, California, 9 0 0 1 2, open Monday through Thursday  from 9 A-M to 3 P-M.",
                    "options":[],
                    "repeatMenu":"To repeat this information,  press *",
                    "mainMenu": "To select another menu option, press 9"
                }]
			context.succeed(choices[0]);
			break;
			case "9":
            choices = [{
                    "digits":1,
                    "lang": lang,
                    "action":"/dev/violationmenu?lang="+lang,
                    "message": "",
                    "options":[
                        "To check the amount due on a violation",
                        "To find out how to pay for a violation",
                        "For information on how to contest a violation, or schedule an administrative hearing"
                    ],
                    "repeatMenu":"To repeat this menu, press *"
                }];
            context.succeed(choices[0]);
            break;
    }
}
	
	
  if(lang == "es-MX"){
	  
	  
	if(validOptions.indexOf(enteredNum) == -1) {
        inputInvalid = inputInvalid + "1"; 
		if(inputInvalid == "111") {
			choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/customerservicescall?lang="+lang,
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
			}];
		}else {
		    choices = [{
		        "digits":1,
				"lang": lang,
				"action":"/dev/paybyperson?lang="+lang+"00invalidCount="+inputInvalid,
				"message": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
				"repeatMenu":"Para repetir estas opciones, presione estrella",
                "mainMenu": "Para regresar al menú principal, presione nueve"
			}];
		}
		context.succeed(choices[0]);
	}
		
	  
    switch(enteredNum) {
			case "*":
			choices = [{
                    "digits":1,
                    "lang": lang,
                    "action":"/dev/paybyperson?lang="+lang,
                    "message": "Para pagar en persona, por favor vaya a la corte de transito de metro ubicado en el nivel plaza en One Gateway Plaza en Los Angeles, California, 9 0 0 1 2, abierto de lunes a jueves de 9 A-M a 3 P-M.",
                    "options":[],
                    "repeatMenu":"Para repetir estas opciones, presione estrella",
                    "mainMenu": "Para regresar al menú principal, presione nueve",
                }]
			context.succeed(choices[0]);
			break;
			case "9":
            choices = [{
                    "digits":1,
                    "lang": lang,
                    "action":"/dev/violationmenu?lang="+lang,
                    "message": "",
                    "optionSpanish":[
                        "Para verificar el saldo de una multa ... presione uno",
                        "Información para pagar su multa ... presione dos",
                        "Para información cómo puede disputar una multa o cómo citar una audiencia administrativa…presione tres"
                    ],
                    "repeatMenu":"Para repetir este menú, presione estrella"
                }];
            context.succeed(choices[0]);
            break;
    }
       
   }
};
function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}