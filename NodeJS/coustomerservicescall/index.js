console.log('Loading custamerServiceCall function');

exports.handler = function(event, context) {
	console.log('Loading holidays/business hours check in custamerServiceCall');

	var lang = event.lang;
	
	var date = new Date();
	console.log('In select Language:: '+date);
	var checkHoliday = require('custom-metro-holidaylist')(date);
	console.log("checkHoliday:: "+checkHoliday);
		
	var businessHours = require('business-hours')(7, 16);
	var isInBusinessHours = businessHours.isBusinessHours();
	console.log("isInBusinessHours:: "+isInBusinessHours);
if(lang == "en-US"){
    if(checkHoliday || !isInBusinessHours) {
	console.log('holiday or not business hours');
    choices = [
        {
           "digits":1,
           "lang": lang,
            "action":"",
            "message": "Your call must be handled by a Metro Transit Court representative. Our hours are 8:00 am-4pm Monday through Friday, except holidays. Please call back during our regular business hours.",
            "options":[],
			"timeOut":"0"
        }
      ]
    }else {
        choices = [
        {
           "digits":1,
           "lang": lang,
            "action":"dialcustomer",
            "message": "Please hold, a Transit Court representative will be with you shortly. This call may be monitored for quality assurance purposes.",
            "options":[] 
        }
      ]
    }
        
   qryObject = parseQuery(event.reqbody);
   
   context.succeed(choices[0]);
}
if(lang == "es-MX"){
    if(checkHoliday || !isInBusinessHours) {
	console.log('holiday or not business hours');
    choices = [
        {
           "digits":1,
           "lang": lang,
            "action":"",
            "message": "Su llamada debe ser manejada por un representante de la corte de transito de metro. Nuestro horario es de 8:00 am-4pm de lunes a viernes, excepto días feriados. por favor llamar de vuelta durante nuestro horario comercial regular.",
            "options":[],
			"timeOut":"0"
        }
      ]
    }else {
        choices = [
        {
           "digits":1,
           "lang": lang,
            "action":"dialcustomer",
            "message": "Por favor, mantenga, un representante de la corte de transito de metro estará con usted en breve. Esta llamada puede ser monitoreada para fines de garantía de calidad.",
            "options":[] 
        }
      ]
    }
        
   qryObject = parseQuery(event.reqbody);
   
   context.succeed(choices[0]);

}
};

function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}