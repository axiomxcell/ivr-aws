exports.handler = function(event, context) {
	var ZIPENTER = 0;
	var REPEAT = 1;
	
	qryObject = parseQuery(event.reqbody);
	var ccnum = qryObject['Digits'];
	var token = event.ecitnum;
	var jwt = require('jsonwebtoken');
	
	jwt.verify(token, 'e3b65b3a-2b52-11e7-93ae-92361f002671', function(err, decoded) {      
		if (err) {
			//Need to send user to first screen
		}else{
			var lang = decoded.selectedlang;
			var resObj = {};
			if( !decoded.zipa || decoded.zipa === ZIPENTER ){
				decoded.zipcode = ccnum;
				decoded.zipa = REPEAT;
				var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
				resObj = {
					"digits":1,
					"action":"/dev/zipenter?ecitnum="+authToken,
					"message":"You entered... " + zipProunce(ccnum),
					"options":[
						"If this is correct, press 1",
						"To reenter the number, press 2"
					],
					"lang": lang
				}
			}else if(decoded.zipa === REPEAT){
				ccnum = parseInt(ccnum);
				if( ccnum === 1 ){
					delete decoded.zipa;
					//user says it is correct... continue...
					var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
					resObj = {
						"digits":4,
						"action":"/dev/cvventer?ecitnum="+authToken,
						"message":"Please locate the security code on the back of your credit card.",
						"options":[
							"The security code is a 3-digit number often located in the signature box.",
							"Please enter the security code, followed by the pound sign."
						],
						"lang": lang
					}
				}else if( ccnum === 2 ){
					delete decoded.zipa;
					delete decoded.zipcode;
					//Chooses to reenter the expiry date
					var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
					resObj = {
						"digits":5,
						"action":"/dev/zipenter?ecitnum="+authToken,
						"message":"Please enter the 5 digit zip code for the credit card billing address.",
						"options":[],
						"lang": lang
					}
				}else{
					//Invalid option selected.. so repeat the step one
					decoded.expa = REPEAT;
					var authToken = jwt.sign(decoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671');
					resObj = {
						"digits":1,
						"action":"/dev/zipenter?ecitnum="+authToken,
						"message":"I didn't get your entry.",
						"options":[
							"Please try again"
						],
						"lang": lang
					}
				}
			}
			context.succeed([resObj]);
		}
	});
};
function parseQuery(qstr) {
	var query = {};
	if( qstr ){
		var a = qstr.substr(0).split('&');
		for (var i = 0; i < a.length; i++) {
			var b = a[i].split('=');
			query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
		}
	}
	return query;
}

function zipProunce(cc) {
	var ccn = "";
	for (var i = 0; i < cc.length; i++) {
		ccn = ccn + cc.charAt(i) + " ";
	}
	return ccn;
}