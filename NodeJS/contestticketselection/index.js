console.log('Loading contest tic selection check');

exports.handler = function(event, context) {
	
	qryObject = parseQuery(event.reqbody);
	var ticselection = qryObject['Digits'];
	var jwt = require('jsonwebtoken');

	var langObj = event.lang;
    var lang = "";
    var inputInvalid = "";
	if(langObj.indexOf("invalidCount") !== -1) {
        var langArr = langObj.split('00');
        lang = langArr[0];
        inputInvalidTemp = langArr[1];
        var inputInvalidArr = inputInvalidTemp.split('=');
        inputInvalid = inputInvalidArr[1];
    }else {
        lang = event.lang;
    }
	
	var authToken = jwt.sign({ selectedlang: lang, selectedOption: ticselection }, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });	
	var validOptions = ["1", "2"];
	
if(lang == "en-US"){
	if(validOptions.indexOf(ticselection) == -1) {
		inputInvalid = inputInvalid + "1"; 	
		if(inputInvalid == "111") {
			choices = [{
				"digits":0,
				"lang": lang,
				"action":"/dev/customerservicescall?lang="+lang,
				"message": "",
				"options":[],
				"redirectAction":"YES",
				"timeOut":"0"
			}];
		}else {
			choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/contestticketselection?lang="+lang+"00invalidCount="+inputInvalid,
					"message": "We are sorry, your entry was invalid. Please try again",
					"options": [
						"If you are calling about a transit violation…",
						"If you are calling about a parking violation…"
					]
			}];
		}
		context.succeed(choices[0]);
	}
	
	choices = [
		{
			"digits":1,
			"lang":lang,
			"action":"/dev/contestticselectionrepeat?ecitnum="+authToken,
			"message": "The request for an initial review must be received within twenty-one calendar days of the violation being issued.  If you do not request an initial review within this time frame, you lose your right to contest.  You may request the initial review in writing, in person, or by phone.",
			"submessage1":"Please send all correspondence to: 	With full payment of all fines and penalties to: 	Metro Transit Court, P-O Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. Be sure to include your violation number on the check or money order.",
			"submessage2":"Please state the reason for your initial review request in your letter and submit any supporting documents.  Metro Transit Court will determine whether or not the violation was properly issued.  The results of the review will be mailed to you.",
			"repeatMenu": "To repeat this message, press *",
			"mainMenu": "To return to the main menu, press 9",
			"options":[
			"To repeat the address,"
			]
		  },
		  {
			"digits":1,
			"lang":lang,
			"action":"/dev/contestticselectionrepeat?ecitnum="+authToken,
			"message": "The request for an initial review must be received within twenty-one days of the violation being issued, or within fourteen calendar days after the mailing of â€˜the notice of delinquent parking violationâ€™.  If you do not request an initial review within this time frame, you lose your right to contest.  You may request the initial review in writing, in person, or by phone.",
			"submessage1":"Please send all correspondence to: 	With full payment of all fines and penalties to:   Metro Transit Court, PO Box 8 6 6 0 1 5, Los Angeles, California, 9 0 0 8 6. Be sure to include your violation and license plate number on the check or money order.",
			"submessage2":"Please state the reason for your initial review request in your letter and submit any supporting documents.  Metro Transit Court will determine whether or not the violation was properly issued.  The results of the review will be mailed to you.",
          "repeatMenu": "To repeat this message, press *",
          "mainMenu": "To return to the main menu, press 9",
          "options":[
              "To repeat the address,"
              ]
      }];
 context.succeed(choices[qryObject['Digits']-1]);
}


	if(lang =="es-MX"){
		
		if(validOptions.indexOf(ticselection) == -1) {
			inputInvalid = inputInvalid + "1"; 
			if(inputInvalid == "111") {
				choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/customerservicescall?lang="+lang,
					"message": "",
					"options":[],
					"redirectAction":"YES",
					"timeOut":"0"
				}];
			}else {
				choices = [{
					"digits":1,
					"lang": lang,
					"action":"/dev/contestticketselection?lang="+lang+"00invalidCount="+inputInvalid,
					"message": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo",
					"optionSpanish": [
						"Si llama acerca de una multa de transito…presione uno",
						"Si llama acerca de una multa de estacionamiento…presione dos"
					]
				}];
			}
			context.succeed(choices[0]);
		}
	
	choices = [
      {
          "digits":1,
          "lang":lang,
          "action":"/dev/contestticselectionrepeat?ecitnum="+authToken,
			"message": "La solicitud de una revisiÃ³n inicial debe ser recibida dentro de los veintiÃºn dÃ­as calendario de la multa en que fue emitida. Si no solicita una revisiÃ³n inicial dentro de este marco de tiempo, pierde su derecho a participar. Usted puede solicitar la revisiÃ³n inicial por escrito, en persona o por telÃ©fono.",
			"submessage1":"Por favor envÃ­e toda la correspondencia a: 	Con el pago completo de todas las multas y multas a: Metro Transit Court, P-O Box 8 6 6 0 1 5, Los Ã�ngeles, California, 9 0 0 8 6.AsegÃºrese de incluir su nÃºmero de multa en el cheque o giro postal.",
			"submessage2":"Por favor, en la carta, indique el motivo por el que solicita la revisiÃ³n inicial y envÃ­e toda la documentaciÃ³n pertinente.  La corte de transito de Metro determinarÃ¡ si fue multado correctamente o no.   Los resultados de la revisiÃ³n se le enviarÃ¡n por correo.",
          "repeatMenu": "Para repetir este mensaje, presione estrella",
          "mainMenu": "Para regresar al menÃº principal, presione nueve",
          "optionSpanish":[
              "Para repetir la direcciÃ³n, pulse uno"
              ]
      },
      {
          "digits":1,
          "lang":lang,
          "action":"/dev/contestticselectionrepeat?ecitnum="+authToken,
			"message": "La solicitud de una revisiÃ³n inicial debe ser recibida dentro de los veintiÃºn dÃ­as de la multa en que fue emitida, o dentro de catorce dÃ­as calendario despuÃ©s del envÃ­o de 'la notificaciÃ³n de violaciÃ³n de estacionamiento delincuente'. Si no solicita una revisiÃ³n inicial dentro de este marco de tiempo, pierde su derecho a disputar. Usted puede solicitar la revisiÃ³n inicial por escrito, en persona o por telÃ©fono.",
			"submessage1":"Por favor envÃ­e toda la correspondencia a: With full payment of all fines and penalties to:   Metro Transit Court, P-O Box 8 6 6 0 1 5, Los Ã�ngeles, California, 9 0 0 8 6.AsegÃºrese de incluir su nÃºmero de multa en el cheque o giro postal.",
			"submessage2":"Por favor, en la carta, indique el motivo por el que solicita la revisiÃ³n inicial y envÃ­e toda la documentaciÃ³n pertinente.  La corte de transito de Metro determinarÃ¡ si fue multado correctamente o no.   Los resultados de la revisiÃ³n se le enviarÃ¡n por correo.",
          "repeatMenu": "Para repetir este mensaje, presione estrella",
          "mainMenu": "Para regresar al menÃº principal, presione nueve",
          "optionSpanish":[
              "Para repetir la direcciÃ³n, pulse uno"
              ]
      }]
  context.succeed(choices[qryObject['Digits']-1]);
 }
};

function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}