exports.handler = function(event, context) {
	qryObject = parseQuery(event.reqbody);
	var ccnum = qryObject['Digits'];
	var token = event.ecitnum;
	var jwt = require('jsonwebtoken');
	jwt.verify(token, 'e3b65b3a-2b52-11e7-93ae-92361f002671', function(err, decoded) {      
		if (err) {
			//Need to send user to first screen
		}else{
			
			var authToken = jwt.sign({ ecitnum: decoded.ecitnum, amount: decoded.amount, cc: ccnum}, 
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
			var response = [{
				"digits":5,
				"action":"/dev/expenter?ecitnum="+authToken,
				"message":"Enter your card expiry date in M M Y Y format followed by #.",
				"options":[]
			}];
			context.succeed(response);
		}
	});
};
function parseQuery(qstr) {
	var query = {};
	var a = qstr.substr(0).split('&');
	for (var i = 0; i < a.length; i++) {
		var b = a[i].split('=');
		query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
	}
	return query;
}