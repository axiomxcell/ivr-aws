console.log('Loading confirmenterednum selection');

exports.handler = function(event, context) {
    
    qryObject = parseQuery(event.reqbody);
    var enteredNum = qryObject['Digits'];
    var ecitnumEncoded = event.ecitnum;
    var choices = [];
    var jwt = require('jsonwebtoken');
    var request = require('request');
	
	jwt.verify(ecitnumEncoded, 'e3b65b3a-2b52-11e7-93ae-92361f002671', function(err, decoded) {    
		if (err) {
			//Need to send user to first screen
		}else{
			var lang = decoded.selectedlang;
			var validOptions = ["1", "2"];
			
		if(lang =="en-US"){
		
			if(validOptions.indexOf(enteredNum) == -1) {
				var inputInvalid = "";
				if(decoded.invalidCount != undefined) {
					inputInvalid = decoded.invalidCount;
				}
				inputInvalid = inputInvalid + "1"; 
				
				var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption , userCitNum : decoded.userCitNum, invalidCount: inputInvalid }, 
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
				var enteredNumArray = decoded.userCitNum.split('');
				var convertedNum = '';
				for(var i = 0;i<enteredNumArray.length;i++) {
					convertedNum = convertedNum + enteredNumArray[i]+',';
				}
				
				if(inputInvalid == "111") {
					choices = [{
						"digits":0,
						"lang": lang,
						"action":"",
						"message": "",
						"options":[],
						"redirectAction":"YES",
						"timeOut":"0"
					}];
				}else {
					choices = [{
						"digits":1,
						"lang": decoded.selectedlang,
						"action":"/dev/confirmenterednum?ecitnum="+authTokenTryAgain,
						"message": "We are sorry, your entry was invalid. Please try again. You entered "+convertedNum,
						"repeatMenu": "If this is correct, press 1",
						"mainMenu": "To reenter the number, press 2"
					}];
				}
			context.succeed(choices[0]);
			}
			
			
			switch(enteredNum) {
			case "1":
				if(decoded.selectedOption == "1") {
			        request("http://54.183.19.66:8080/ecitation/getCitationStatus/T"+decoded.userCitNum, function(error, res, body) {
						console.log(error+' '+body);
			            if(!error){
			                var citStatus = JSON.parse(body);
			                console.log(citStatus);
							var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum , citationStatus: citStatus}, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
							
							if(citStatus.status == "NOTFOUND") {
								
								var date = new Date();
								console.log('In select Language:: '+date);
								var checkHoliday = require('custom-metro-holidaylist')(date);
								console.log("checkHoliday:: "+checkHoliday);
								
								var businessHours = require('business-hours')(7, 16);
								var isInBusinessHours = businessHours.isBusinessHours();
								console.log(isInBusinessHours);

								if(checkHoliday || !isInBusinessHours) {
	
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"",
								"message": "We are unable to retrieve the status of your violation. If your violation was issued today, please call back in 5 business days for the ticket to update in the system.",
								"submessage1" : "If you need to speak with a Metro Transit Court representative, please stay on the line.",
								"submessage2": "Your call must be handled by a Metro Transit Court representative. Our hours are 8:00 am-4pm Monday through Friday, except holidays. Please call back during our regular business hours.",
								"options":[],
								"timeOut":"0"
								}]
								
								}else {
									
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"dialcustomer",
								"message": "We are unable to retrieve the status of your violation. If your violation was issued today, please call back in 5 business days for the ticket to update in the system.",
								"submessage1" : "If you need to speak with a Metro Transit Court representative, please stay on the line.",
								"submessage2": "Please hold, a Transit Court representative will be with you shortly. This call may be monitored for quality assurance purposes.",
								"options":[]
								}]
										
									
								}
								
								
							}else if(citStatus.status == "PREPROCESSED") {
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"/dev/citationstatusrepeat?ecitnum="+authToken,
								"message": "We are unable to retrieve the status of your violation at this time. Please call back in 5 days",
								"repeatMenu": "To repeat this information,  press *",
								"mainMenu": "To select another menu option, press 9",
								"options":[]
								}]
							}else if(citStatus.status == "CLOSED" || citStatus.status == "PAID") {
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"/dev/citationstatusrepeat?ecitnum="+authToken,
								"message": "Your violation is closed.  There is no payment due and no further action is required.",
								"repeatMenu": "To repeat this information,  press *",
								"mainMenu": "To select another menu option, press 9",
								"options":[]
								}]
							}else {
								
								if(citStatus.paymentStatus == "PARTIALPAYMENTDUE") {
											choices = [{
												"digits":1,
												"lang": decoded.selectedlang,
												"action":"/dev/paymentstatusrepeat?ecitnum="+authToken,
												"message": "A partial payment of </Say><Pause length='1'></Pause><Say language='en-US'>$"+citStatus.amountPaid +" was received. The unpaid balance is </Say><Pause length='1'></Pause><Say language='en-US'>$"+ citStatus.totalDue,
												"submessage1" : "To close this ticket, this amount must be paid in full.",
												"repeatMenu": "To repeat this information,  press *",
												"payOptn" : "For payment options, press 1",
												"mainMenu": "To return to the main menu,  press 9",
												"options":[]
											}]
								}else if(citStatus.paymentStatus == "NOPAYMENTMADE") {
											choices = [{
												"digits":1,
												"lang": decoded.selectedlang,
												"action":"/dev/paymentstatusrepeat?ecitnum="+authToken,
												"message": "No payments have been received as of today. The current amount due is  </Say><Pause length='1'></Pause><Say language='en-US'>$"+ citStatus.totalDue,
												"submessage1" : "To close this ticket, this amount must be paid in full.",
												"repeatMenu": "To repeat this information,  press *",
												"payOptn" : "For payment options, press 1",
												"mainMenu": "To return to the main menu,  press 9",
												"options":[]
											}]
								}else {
									//error
								}
							}
						context.succeed(choices[0]);
			            }else {
							var citStatus = {"code":401,"status":"PREPROCESSED","paymentStatus":"NOPAYMENTMADE","amountPaid":0.0,"totalDue":0.0,"licencePlateNumber":""};
							
							var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum , citationStatus: citStatus}, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
							choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"/dev/citationstatusrepeat?ecitnum="+authToken,
								"message": "We are unable to retrieve the status of your violation at this time. Please call back in 5 days",
								"repeatMenu": "To repeat this information,  press *",
								"mainMenu": "To select another menu option, press 9",
								"options":[]
							}]
							context.succeed(choices[0]);
						}
			        });
			    }
				
				if(decoded.selectedOption == "2") {
			        request("http://54.183.19.66:8080/ecitation/getCitationStatus/P"+decoded.userCitNum, function(error, res, body) {
						console.log(error+' '+body);
			            if(!error){
			                var citStatus = JSON.parse(body);
			                console.log(citStatus);
							var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum, citationStatus: citStatus }, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
							
							if(citStatus.status == "NOTFOUND") {
								
								var date = new Date();
								console.log('In select Language:: '+date);
								var checkHoliday = require('custom-metro-holidaylist')(date);
								console.log("checkHoliday:: "+checkHoliday);
								
								var businessHours = require('business-hours')(7, 16);
								var isInBusinessHours = businessHours.isBusinessHours();
								console.log("isInBusinessHours:: "+isInBusinessHours);

								if(checkHoliday || !isInBusinessHours) {
	
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"",
								"message": "We are unable to retrieve the status of your violation. If your violation was issued today, please call back in 5 business days for the ticket to update in the system.",
								"submessage1" : "If you need to speak with a Metro Transit Court representative, please stay on the line.",
								"submessage2": "Your call must be handled by a Metro Transit Court representative. Our hours are 8:00 am-4pm Monday through Friday, except holidays. Please call back during our regular business hours.",
								"options":[],
								"timeOut":"0"
								}]
								
								}else {
									
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"dialcustomer",
								"message": "We are unable to retrieve the status of your violation. If your violation was issued today, please call back in 5 business days for the ticket to update in the system.",
								"submessage1" : "If you need to speak with a Metro Transit Court representative, please stay on the line.",
								"submessage2": "Please hold, a Transit Court representative will be with you shortly. This call may be monitored for quality assurance purposes.",
								"options":[]
								}]
									
								}
								
							}else if(citStatus.status == "PREPROCESSED") {
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"/dev/citationstatusrepeat?ecitnum="+authToken,
								"message": "We are unable to retrieve the status of your violation at this time. Please call back in 5 days",
								"repeatMenu": "To repeat this information,  press *",
								"mainMenu": "To select another menu option, press 9",
								"options":[]
								}]
							}else if(citStatus.status == "CLOSED" || citStatus.status == "PAID") {
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"/dev/citationstatusrepeat?ecitnum="+authToken,
								"message": "Your violation is closed.  There is no payment due and no further action is required.",
								"repeatMenu": "To repeat this information,  press *",
								"mainMenu": "To select another menu option, press 9",
								"options":[]
								}]
							}else {
								
								if(citStatus.paymentStatus == "PARTIALPAYMENTDUE") {
									if(citStatus.licencePlateNumber != "" && citStatus.allPenalties > citStatus.totalDue) {
										
										var licPlateNumArray = citStatus.licencePlateNumber.split('');
										var licPlateNumToSay = '';

										for(var i = 0;i<licPlateNumArray.length;i++) {
											licPlateNumToSay = licPlateNumToSay + licPlateNumArray[i]+',';
										}
										
										choices = [{
												"digits":1,
												"lang": decoded.selectedlang,
												"action":"/dev/paymentstatusrepeat?ecitnum="+authToken,
												"message": "A partial payment of </Say><Pause length='1'></Pause><Say language='en-US'>$"+citStatus.amountPaid +" was received. The unpaid balance is </Say><Pause length='1'></Pause><Say language='en-US'>$"+ citStatus.totalDue,
												"submessage1" : "To close this ticket, this amount must be paid in full.",
												"submessage2" : "For license plate </Say><Pause length='1'></Pause><Say language='en-US'>"+licPlateNumToSay+" the total amount due with all penalties is </Say><Pause length='1'></Pause><Say language='en-US'>$"+ citStatus.allPenalties,
												"repeatMenu": "To repeat this information,  press *",
												"payOptn" : "For payment options, press 1",
												"mainMenu": "To return to the main menu,  press 9",
												"options":[]
										}]
									}else {
										choices = [{
												"digits":1,
												"lang": decoded.selectedlang,
												"action":"/dev/paymentstatusrepeat?ecitnum="+authToken,
												"message": "A partial payment of </Say><Pause length='1'></Pause><Say language='en-US'>$"+citStatus.amountPaid +" was received. The unpaid balance is </Say><Pause length='1'></Pause><Say language='en-US'>$"+ citStatus.totalDue,
												"submessage1" : "To close this ticket, this amount must be paid in full.",
												"submessage2" : "",
												"repeatMenu": "To repeat this information,  press *",
												"payOptn" : "For payment options, press 1",
												"mainMenu": "To return to the main menu,  press 9",
												"options":[]
										}]
												
									}
											
								}else if(citStatus.paymentStatus == "NOPAYMENTMADE") {
									
									if(citStatus.licencePlateNumber != "" && citStatus.allPenalties > citStatus.totalDue) {
										
										var licPlateNumArray = citStatus.licencePlateNumber.split('');
										var licPlateNumToSay = '';

										for(var i = 0;i<licPlateNumArray.length;i++) {
											licPlateNumToSay = licPlateNumToSay + licPlateNumArray[i]+',';
										}
										
										choices = [{
												"digits":1,
												"lang": decoded.selectedlang,
												"action":"/dev/paymentstatusrepeat?ecitnum="+authToken,
												"message": "No payments have been received as of today. The current amount due is  </Say><Pause length='1'></Pause><Say language='en-US'>$"+ citStatus.totalDue,
												"submessage1" : "To close this ticket, this amount must be paid in full.",
												"submessage2" : "For license plate </Say><Pause length='1'></Pause><Say language='en-US'>"+licPlateNumToSay+" , the total amount due with all penalties is </Say><Pause length='1'></Pause><Say language='en-US'>$"+ citStatus.allPenalties,
												"repeatMenu": "To repeat this information,  press *",
												"payOptn" : "For payment options, press 1",
												"mainMenu": "To return to the main menu,  press 9",
												"options":[]
										}]	
									}else {
										choices = [{
												"digits":1,
												"lang": decoded.selectedlang,
												"action":"/dev/paymentstatusrepeat?ecitnum="+authToken,
												"message": "No payments have been received as of today. The current amount due is  </Say><Pause length='1'></Pause><Say language='en-US'>$"+ citStatus.totalDue,
												"submessage1" : "To close this ticket, this amount must be paid in full.",
												"submessage2" : "",
												"repeatMenu": "To repeat this information,  press *",
												"payOptn" : "For payment options, press 1",
												"mainMenu": "To return to the main menu,  press 9",
												"options":[]
										}]
									}
								}else {
									//error
								}
							}
						context.succeed(choices[0]);
			            }else {
							var citStatus = {"code":401,"status":"PREPROCESSED","paymentStatus":"NOPAYMENTMADE","amountPaid":0.0,"totalDue":0.0,"licencePlateNumber":""};
							
							var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum , citationStatus: citStatus}, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
							choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"/dev/citationstatusrepeat?ecitnum="+authToken,
								"message": "We are unable to retrieve the status of your violation at this time. Please call back in 5 days",
								"repeatMenu": "To repeat this information,  press *",
								"mainMenu": "To select another menu option, press 9",
								"options":[]
							}]
							context.succeed(choices[0]);
						}
			        });
			    }
				break;
			case "2":
			var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption }, 
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
			if(decoded.selectedOption == "1") {
				choices = [{
					"digits":18,
					"lang": decoded.selectedlang,
					"action":"/dev/repeatenterednum?ecitnum="+authToken,
					"message": "Please enter the numbers listed after the letter T, followed by the pound sign.",
					"options":[]
				}];	
				context.succeed(choices[0]);
			}
			
			if(decoded.selectedOption == "2") {
				choices = [{
					"digits":18,
					"lang": decoded.selectedlang,
					"action":"/dev/repeatenterednum?ecitnum="+authToken,
					"message": "Please enter the numbers listed after the letter P, followed by the pound sign.",
					"options":[] 
				}];	
				context.succeed(choices[0]);
			}
			
			break;	
			}
		}
		
		
		if(lang =="es-MX"){
			
			
			if(validOptions.indexOf(enteredNum) == -1) {
				var inputInvalid = "";
				if(decoded.invalidCount != undefined) {
					inputInvalid = decoded.invalidCount;
				}
				inputInvalid = inputInvalid + "1"; 
				
				var authTokenTryAgain = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption , userCitNum : decoded.userCitNum, invalidCount: inputInvalid }, 
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
				
				var enteredNumArray = decoded.userCitNum.split('');
				var convertedNumSpanish = '';
				
				for(var i = 0;i<enteredNumArray.length;i++) {
					convertedNumSpanish = convertedNumSpanish + enteredNumArray[i]+"</Say><Pause length='1'></Pause><Say language='es-MX'>";
				}
				
				if(inputInvalid == "111") {
					choices = [{
						"digits":0,
						"lang": lang,
						"action":"",
						"message": "",
						"options":[],
						"redirectAction":"YES",
						"timeOut":"0"
					}];
				}else {
					choices = [{
						"digits":1,
						"lang": decoded.selectedlang,
						"action":"/dev/confirmenterednum?ecitnum="+authTokenTryAgain,
						"message": "Lo sentimos, su entrada no es válida. Por favor, inténtelo de nuevo. oprimio "+convertedNumSpanish,
						"repeatMenu": "Si esto es correcto, presione uno, ",
						"mainMenu": "Para volver a ingresar el número, presione dos, "
					}];
				}
			context.succeed(choices[0]);
			}
			
			
			switch(enteredNum) {
			case "1":
				if(decoded.selectedOption == "1") {
			        request("http://54.183.19.66:8080/ecitation/getCitationStatus/T"+decoded.userCitNum, function(error, res, body) {
						console.log(error+' '+body);
			            if(!error){
			                var citStatus = JSON.parse(body);
			                console.log(citStatus);
							var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum , citationStatus: citStatus}, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
							console.log('authtoken:: '+authToken);
							if(citStatus.status == "NOTFOUND") {
								
								var date = new Date();
								console.log('In select Language:: '+date);
								var checkHoliday = require('custom-metro-holidaylist')(date);
								console.log("checkHoliday:: "+checkHoliday);
								
								var businessHours = require('business-hours')(7, 16);
								var isInBusinessHours = businessHours.isBusinessHours();
								console.log(isInBusinessHours);

								if(checkHoliday || !isInBusinessHours) {
	
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"",
								"message": "No hemos podido recuperar el estado de la información de su multa.  Si su multa ha sido emitida hoy, permita que transcurran 5 días para que sea introducida en el sistema.",
								"submessage1" : "Si necesita hablar con un representante de Metro Transit Court, por favor, permanezca en la línea.",
								"submessage2": "Su llamada debe ser manejada por un representante de la corte de transito de metro. Nuestro horario es de 8:00 am-4pm de lunes a viernes, excepto días feriados. por favor llamar de vuelta durante nuestro horario comercial regular.",
								"options":[],
								"timeOut":"0"
								}]
								
								}else {
									
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"dialcustomer",
								"message": "No hemos podido recuperar el estado de la información de su multa.  Si su multa ha sido emitida hoy, permita que transcurran 5 días para que sea introducida en el sistema.",
								"submessage1" : "Si necesita hablar con un representante de Metro Transit Court, por favor, permanezca en la línea.",
								"submessage2": "Por favor, mantenga, un representante de la corte de transito de metro estará con usted en breve. Esta llamada puede ser monitoreada para fines de garantía de calidad.",
								"options":[]
								}]
										
									
								}
								
								
							}else if(citStatus.status == "PREPROCESSED") {
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"/dev/citationstatusrepeat?ecitnum="+authToken,
								"message": "No podemos recuperar el estado de su multa en este momento. Por favor llame en 5 días",
								"repeatMenu": "Para repetir esta información, presione estrella",
								"mainMenu": "Para seleccionar otra opción de menú, presione nueve",
								"options":[]
								}]
							}else if(citStatus.status == "CLOSED" || citStatus.status == "PAID") {
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"/dev/citationstatusrepeat?ecitnum="+authToken,
								"message": "Su multa está cerrada.  No hay pago pendiente y no es necesario realizar ninguna otra acción.",
								"repeatMenu": "Para repetir esta información, presione estrella",
								"mainMenu": "Para seleccionar otra opción de menú, presione nueve",
								"options":[]
								}]
							}else {
								
								if(citStatus.paymentStatus == "PARTIALPAYMENTDUE") {
											choices = [{
												"digits":1,
												"lang": decoded.selectedlang,
												"action":"/dev/paymentstatusrepeat?ecitnum="+authToken,
												"message": "Un pago parcial de </Say><Pause length='1'></Pause><Say language='es-MX'>$"+citStatus.amountPaid +" fue recibido. El saldo es </Say><Pause length='1'></Pause><Say language='es-MX'>$"+ citStatus.totalDue,
												"submessage1" : "Para cerrar esta multa, la cantidad debe ser pagado en su totalidad.",
												"repeatMenu": "Para repetir esta información, presione estrella",
												"payOptn" : "Para opciones de pago, presione uno",
												"mainMenu": "Para regresar al menú principal, presione nueve",
												"options":[]
											}]
								}else if(citStatus.paymentStatus == "NOPAYMENTMADE") {
											choices = [{
												"digits":1,
												"lang": decoded.selectedlang,
												"action":"/dev/paymentstatusrepeat?ecitnum="+authToken,
												"message": "No se han recibido pagos a partir de la fecha de hoy. El monto actual debido es </Say><Pause length='1'></Pause><Say language='es-MX'>$"+ citStatus.totalDue,
												"submessage1" : "Para cerrar esta multa, la cantidad debe ser pagado en su totalidad.",
												"repeatMenu": "Para repetir esta información, presione estrella",
												"payOptn" : "Para opciones de pago, presione uno",
												"mainMenu": "Para regresar al menú principal, presione nueve",
												"options":[]
											}]
								}else {
									//error
								}
							}
						context.succeed(choices[0]);
			            }else {
							var citStatus = {"code":401,"status":"PREPROCESSED","paymentStatus":"NOPAYMENTMADE","amountPaid":0.0,"totalDue":0.0,"licencePlateNumber":""};
							
							var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum , citationStatus: citStatus}, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
							choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"/dev/citationstatusrepeat?ecitnum="+authToken,
								"message": "No podemos recuperar el estado de su multa en este momento. Por favor llame en 5 días",
								"repeatMenu": "Para repetir esta información, presione estrella",
								"mainMenu": "Para seleccionar otra opción de menú, presione nueve",
								"options":[]
							}]
							context.succeed(choices[0]);
						}
			        });
			    }
				
				if(decoded.selectedOption == "2") {
			        request("http://54.183.19.66:8080/ecitation/getCitationStatus/P"+decoded.userCitNum, function(error, res, body) {
						console.log(error+' '+body);
			            if(!error){
			                var citStatus = JSON.parse(body);
			                console.log(citStatus);
							var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum, citationStatus: citStatus }, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
							
							if(citStatus.status == "NOTFOUND") {
								
								var date = new Date();
								console.log('In select Language:: '+date);
								var checkHoliday = require('custom-metro-holidaylist')(date);
								console.log("checkHoliday:: "+checkHoliday);
								
								var businessHours = require('business-hours')(7, 16);
								var isInBusinessHours = businessHours.isBusinessHours();
								console.log("isInBusinessHours:: "+isInBusinessHours);

								if(checkHoliday || !isInBusinessHours) {
	
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"",
								"message": "No hemos podido recuperar el estado de la información de su multa.  Si su multa ha sido emitida hoy, permita que transcurran 5 días para que sea introducida en el sistema.",
								"submessage1" : "Si necesita hablar con un representante de Metro Transit Court, por favor, permanezca en la línea.",
								"submessage2": "Su llamada debe ser manejada por un representante de la corte de transito de metro. Nuestro horario es de 8:00 am-4pm de lunes a viernes, excepto días feriados. por favor llamar de vuelta durante nuestro horario comercial regular.",
								"options":[],
								"timeOut":"0"
								}]
								
								}else {
									
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"dialcustomer",
								"message": "No hemos podido recuperar el estado de la información de su multa.  Si su multa ha sido emitida hoy, permita que transcurran 5 días para que sea introducida en el sistema.",
								"submessage1" : "Si necesita hablar con un representante de Metro Transit Court, por favor, permanezca en la línea.",
								"submessage2": "Por favor, mantenga, un representante de la corte de transito de metro estará con usted en breve. Esta llamada puede ser monitoreada para fines de garantía de calidad.",
								"options":[]
								}]
									
								}
								
							}else if(citStatus.status == "PREPROCESSED") {
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"/dev/citationstatusrepeat?ecitnum="+authToken,
								"message": "No podemos recuperar el estado de su multa en este momento. Por favor llame en 5 días",
								"repeatMenu": "Para repetir esta información, presione estrella",
								"mainMenu": "Para seleccionar otra opción de menú, presione nueve",
								"options":[]
								}]
							}else if(citStatus.status == "CLOSED" || citStatus.status == "PAID") {
								choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"/dev/citationstatusrepeat?ecitnum="+authToken,
								"message": "Su multa está cerrada.  No hay pago pendiente y no es necesario realizar ninguna otra acción.",
								"repeatMenu": "Para repetir esta información, presione estrella",
								"mainMenu": "Para seleccionar otra opción de menú, presione nueve",
								"options":[]
								}]
							}else {
								
								if(citStatus.paymentStatus == "PARTIALPAYMENTDUE") {
									if(citStatus.licencePlateNumber != "" && citStatus.allPenalties > citStatus.totalDue) {
										
										var licPlateNumArray = citStatus.licencePlateNumber.split('');
										var licPlateNumToSaySpanish = '';

										for(var i = 0;i<licPlateNumArray.length;i++) {
											licPlateNumToSaySpanish = licPlateNumToSaySpanish + licPlateNumArray[i]+"</Say><Say language='es-MX'><Pause length='1'></Pause>";
										}
										
										
										choices = [{
												"digits":1,
												"lang": decoded.selectedlang,
												"action":"/dev/paymentstatusrepeat?ecitnum="+authToken,
												"message": "Un pago parcial de </Say><Pause length='1'></Pause><Say language='es-MX'>$"+citStatus.amountPaid +" fue recibido. El saldo es </Say><Pause length='1'></Pause><Say language='es-MX'>$"+ citStatus.totalDue,
												"submessage1" : "Para cerrar esta multa, la cantidad debe ser pagado en su totalidad.",
												"submessage2" : "Para placa de carro </Say><Pause length='1'></Pause><Say language='es-MX'>"+licPlateNumToSaySpanish+" La cantidad debida con todas las penalidades es</Say><Pause length='1'></Pause><Say language='es-MX'>$"+ citStatus.allPenalties,
												"repeatMenu": "Para repetir esta información, presione estrella",
												"payOptn" : "Para opciones de pago, presione uno",
												"mainMenu": "Para regresar al menú principal, presione nueve",
												"options":[]
										}]
									}else {
										choices = [{
												"digits":1,
												"lang": decoded.selectedlang,
												"action":"/dev/paymentstatusrepeat?ecitnum="+authToken,
												"message": "Un pago parcial de </Say><Pause length='1'></Pause><Say language='es-MX'>$"+citStatus.amountPaid +" fue recibido. El saldo es </Say><Pause length='1'></Pause><Say language='es-MX'>$"+ citStatus.totalDue,
												"submessage1" : "Para cerrar esta multa, la cantidad debe ser pagado en su totalidad.",
												"submessage2" : "",
												"repeatMenu": "Para repetir esta información, presione estrella",
												"payOptn" : "Para opciones de pago, presione uno",
												"mainMenu": "Para regresar al menú principal, presione nueve",
												"options":[]
										}]
												
									}
											
								}else if(citStatus.paymentStatus == "NOPAYMENTMADE") {
									
									if(citStatus.licencePlateNumber != "" && citStatus.allPenalties > citStatus.totalDue) {
										
										var licPlateNumArray = citStatus.licencePlateNumber.split('');
										var licPlateNumToSay = '';

										for(var i = 0;i<licPlateNumArray.length;i++) {
											licPlateNumToSay = licPlateNumToSay + licPlateNumArray[i]+"</Say><Say language='es-MX'><Pause length='1'></Pause>";
										}
										
										choices = [{
												"digits":1,
												"lang": decoded.selectedlang,
												"action":"/dev/paymentstatusrepeat?ecitnum="+authToken,
												"message": "No se han recibido pagos a partir de la fecha de hoy. El monto actual debido es </Say><Pause length='1'></Pause><Say language='es-MX'>$"+ citStatus.totalDue,
												"submessage1" : "Para cerrar esta multa, la cantidad debe ser pagado en su totalidad.",
												"submessage2" : "Para placa de carro </Say><Pause length='1'></Pause><Say language='es-MX'>"+licPlateNumToSaySpanish+" , La cantidad debida con todas las penalidades es </Say><Pause length='1'></Pause><Say language='es-MX'>$"+ citStatus.allPenalties,
												"repeatMenu": "Para repetir esta información, presione estrella",
												"payOptn" : "Para opciones de pago, presione uno",
												"mainMenu": "Para regresar al menú principal, presione nueve",
												"options":[]
										}]	
									}else {
										choices = [{
												"digits":1,
												"lang": decoded.selectedlang,
												"action":"/dev/paymentstatusrepeat?ecitnum="+authToken,
												"message": "No se han recibido pagos a partir de la fecha de hoy. El monto actual debido es </Say><Pause length='1'></Pause><Say language='es-MX'>$"+ citStatus.totalDue,
												"submessage1" : "Para cerrar esta multa, la cantidad debe ser pagado en su totalidad.",
												"submessage2" : "",
												"repeatMenu": "Para repetir esta información, presione estrella",
												"payOptn" : "Para opciones de pago, presione uno",
												"mainMenu": "Para regresar al menú principal, presione nueve",
												"options":[]
										}]
									}
								}else {
									//error
								}
							}
						context.succeed(choices[0]);
			            }else {
							var citStatus = {"code":401,"status":"PREPROCESSED","paymentStatus":"NOPAYMENTMADE","amountPaid":0.0,"totalDue":0.0,"licencePlateNumber":""};
							
							var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption, userCitNum: decoded.userCitNum , citationStatus: citStatus}, 'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
							choices = [{
								"digits":1,
								"lang": decoded.selectedlang,
								"action":"/dev/citationstatusrepeat?ecitnum="+authToken,
								"message": "No podemos recuperar el estado de su multa en este momento. Por favor llame en 5 días",
								"repeatMenu": "Para repetir esta información, presione estrella",
								"mainMenu": "Para seleccionar otra opción de menú, presione nueve",
								"options":[]
							}]
							context.succeed(choices[0]);
						}
			        });
			    }
				break;
			case "2":
			var authToken = jwt.sign({ selectedlang: decoded.selectedlang, selectedOption: decoded.selectedOption }, 
				'e3b65b3a-2b52-11e7-93ae-92361f002671', { expiresIn: 90000 });
			if(decoded.selectedOption == "1") {
				choices = [{
					"digits":18,
					"lang": decoded.selectedlang,
					"action":"/dev/repeatenterednum?ecitnum="+authToken,
					"message": "Por favor, oprima los que aparecen después de la letra T, seguido por el signo de libra.",
					"options":[]
				}];	
				context.succeed(choices[0]);
			}
			
			if(decoded.selectedOption == "2") {
				choices = [{
					"digits":18,
					"lang": decoded.selectedlang,
					"action":"/dev/repeatenterednum?ecitnum="+authToken,
					"message": "Por favor, oprima los que aparecen después de la letra P, seguido por el signo de libra.",
					"options":[] 
				}];	
				context.succeed(choices[0]);
			}
			
			break;	
			}
		

		}
		}
	});
};

function parseQuery(qstr) {
        var query = {};
        var a = qstr.substr(0).split('&');
        for (var i = 0; i < a.length; i++) {
            var b = a[i].split('=');
            query[decodeURIComponent(b[0])] = decodeURIComponent(b[1] || '');
        }
        return query;
}